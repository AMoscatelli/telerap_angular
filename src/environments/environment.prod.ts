export const environment = {
  production: true,
  apiEndpoint: 'http://192.168.3.167:1234',
  IVA: 22,
  DirittoFissoChiamata: 30,
  costoKMDefault: '1,00',
  rimborsoKM: '0.440',
  rimborsoOre: '0.458',
  costoInterventoPerOraCISA: '37.00',
};
