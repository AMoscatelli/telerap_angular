import { ScadutoComponent } from './scaduto';
import { ClientiComponent } from './clienti/clienti.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA, APP_INITIALIZER } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';
import { LoginComponent } from './login';
import { HomeComponent } from './home';
import { appRoutingModule } from './app.routing';
import { ErrorInterceptor } from './_helpers';
import { CookieService } from 'ngx-cookie-service';
import { ModalComponent } from './modal/modal.component';
import { UtilityComponent } from './utility/utility.component';
import { ClientiGridComponent } from './clienti/clientiGrid/clientiGrid.component';
import { ClientiAnagraficaComponent } from './clienti/clientiAnagrafica';
import { AppuntamentiComponent } from './appuntamenti/appuntamenti.component';
import { ClientiChiaviComponent } from './clienti/clientiChiavi';
import { MdbTableEditorModule } from 'mdb-table-editor';
import { ClientiManutenzioneComponent } from './clienti/clientiManutenzione';
import { ClientiPorteComponent } from './clienti/clientiPorte';
import { AppComponent } from './app.component';
import { ClientiImpiantoComponent } from './clienti/clientiImpianto';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { ClientiInterventoComponent } from './clienti/clientiIntervento';
import { ClientiAddInterventoComponent } from './clienti/clientiAddIntervento';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ClientiFattureComponent } from './clienti/clientiFatture';
import { ClientiCISAComponent } from './clienti/clientiCISA';
import { ClientiAddImpiantoComponent } from './clienti/clientiAddImpianto';
import { ClientiEComponent } from './clienti/clientiE/clientiE.component';
import { UtilityCentraleComponent } from './utility/utility_centrale/utility_centrale.component';
import { UtilityPianoComponent } from './utility/utility_piano/utility_piano.component';
import { BlankComponent } from './blank/blank.component';
import { UtilityProfessioneComponent } from './utility/utility_professione/utility_professione.component';
import { UtilityTipoClienteComponent } from './utility/utility_tipoCliente/utility_tipoCliente.component';
import { UtilityTipoInterventoComponent } from './utility/utility_tipoIntervento/utility_tipoIntervento.component';
import { UtilityDisdettaComponent } from './utility/utility_disdetta/utility_disdetta.component';
import { UtilityTipoLocaleComponent } from './utility/utility_tipoLocale/utility_tipoLocale.component';
import { UtilityIvaComponent } from './utility/utility_iva/utility_iva.component';
import { UtilityStatoCivileComponent } from './utility/utility_statoCivile/utility_statoCivile.component';
import { UtilityServizioComponent } from './utility/utility_servizio/utility_servizio.component';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { MdbCalendarModule } from 'mdb-calendar';
import { UtilityCalendarColorComponent } from './utility/utility_calendarColor/utility_calendarColor.component';
import { ClientiPorteAddComponent} from './clienti/clientiPorteAdd';


@NgModule({
  declarations: [
    AppComponent,
    ClientiGridComponent,
    LoginComponent,
    HomeComponent,
    UtilityComponent,
    ClientiComponent,
    ClientiAnagraficaComponent,
    ClientiManutenzioneComponent,
    ClientiPorteComponent,
    ClientiPorteAddComponent,
    ClientiImpiantoComponent,
    ClientiAddImpiantoComponent,
    ClientiInterventoComponent,
    ClientiAddInterventoComponent,
    ClientiChiaviComponent,
    ClientiFattureComponent,
    ClientiCISAComponent,
    ClientiChiaviComponent,
    ClientiEComponent,
    UtilityCentraleComponent,
    UtilityPianoComponent,
    UtilityProfessioneComponent,
    UtilityTipoClienteComponent,
    UtilityTipoInterventoComponent,
    UtilityDisdettaComponent,
    UtilityTipoLocaleComponent,
    UtilityIvaComponent,
    UtilityStatoCivileComponent,
    UtilityServizioComponent,
    UtilityCalendarColorComponent,
    ScadutoComponent,
    ModalComponent,
    AppuntamentiComponent,
    BlankComponent

  ],
  imports: [
    MdbCalendarModule,
    GooglePlaceModule,
    MdbTableEditorModule,
    appRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastModule.forRoot(),
    MDBBootstrapModulesPro.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    AgmCoreModule.forRoot({
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
      apiKey: 'AIzaSyAp1xDAEhqUgXjLdzQa7LO2H89VpnYDIiI',
      libraries: ['places']
    })
  ],
  entryComponents: [ ModalComponent ],
  providers: [
    AppuntamentiComponent,
    AppComponent,
    DatePipe,
    CurrencyPipe,
    ClientiComponent,
    CookieService,
    MDBSpinningPreloader,
    ClientiGridComponent,
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
  schemas:      [ NO_ERRORS_SCHEMA ]
})
export class AppModule { }

