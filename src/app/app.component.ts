import { Component, OnInit, HostListener, ChangeDetectionStrategy } from '@angular/core';
import { User } from './_models';
import { Event, Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { AuthenticationService, GoogleService, ModalService, AlertService } from './_services';
import { Observable, fromEvent, merge, of } from 'rxjs';
import { mapTo } from 'rxjs/operators';
import { UtilityService } from './_services/utility.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']

})

export class AppComponent implements OnInit {
  title = 'TeleRAP';
  loading = false;
  currentUser: User;
  mostraDiv: boolean;
  heading: string;
  content: any;
  showComponent: boolean;
  loaderMDB: boolean;
  showGoogle: boolean;
  online$: Observable<boolean>;

  constructor(
    public alertService: AlertService,
    private modalService: ModalService,
    private googleService: GoogleService,
    private router: Router,
    private authenticationService: AuthenticationService,
    private utilityService: UtilityService,
  ) {
    this.online$ = merge(
      of(navigator.onLine),
      fromEvent(window, 'online').pipe(mapTo(true)),
      fromEvent(window, 'offline').pipe(mapTo(false))
    ),
    this.router.events.subscribe((event: Event) => {
      switch (true) {
        case event instanceof NavigationStart: {
          // console.log('navigazioneAvviata');
          this.loading = true;
          break;
        }
        case event instanceof NavigationEnd: {
          // console.log('navigazioneTerminata');
          setTimeout(() => { this.loading = false; }, 300);
          break;

        }
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.loading = false;
          break;
        }
        default: {
          break;
        }
      }
    });
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
}

// call this event handler before browser refresh
@HostListener('window:beforeunload', ['$event']) unloadHandler(event: Event) {
  localStorage.clear();
  this.mostraDiv = false;
}

ngOnInit() {
  this.authenticationService.startTimer(300);
  const s = window.document.createElement('script');
  s.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAp1xDAEhqUgXjLdzQa7LO2H89VpnYDIiI&libraries=places&language=it';
  window.document.body.appendChild(s);
  this.showGoogle = false;
  this.online$.subscribe(value => {
    if (!value) {
      this.alertService.error('Stato connettività: Internet non disponibile');
    } else {
      this.alertService.success('Stato connettività: OK');
    }
    // this.name = `Angular 6 - Network Online? ${value}`;
  });
}

logout() {
  let salvo = false;
  if (localStorage.getItem('utenteTemp')) {
    salvo = confirm('Clicca "OK" se vuoi salvare le modifiche oppure "Annulla" per abbandonare le modifiche e uscire');
  }
  if (salvo) {
    this.utilityService.salvaNuovoCliente(true);
  } else {
    localStorage.clear();
    this.authenticationService.logout();
  }

    // chiamo la funzione che resetta il local storage

}

setDiv(status: boolean, status1: boolean) {
  this.mostraDiv = status;
  this.showComponent = status1;
}

mostraGoogle(status: boolean) {
  this.showGoogle = status;
}

googleSign() {
  this.googleService.googleSignIn();
  this.modalService.showModal();
}

}
