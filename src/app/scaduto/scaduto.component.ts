import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_services';
import { AppComponent } from '../app.component';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../_models';

@Component({
  selector: 'app-scaduto',
  templateUrl: './scaduto.component.html',
  styleUrls: ['./scaduto.component.scss']
})
export class ScadutoComponent implements OnInit {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  constructor(
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {
      this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
      this.currentUser = this.currentUserSubject.asObservable();
    }

  ngOnInit() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    // console.log(localStorage.getItem('currentUser'));
    setTimeout(() => this.appComponent.setDiv(false, false), 0);
    // setTimeout(() => this.appComponent.loggato = true, 1);

  }

  logout() {
      this.authenticationService.logout();
  }
}
