import { Component } from '@angular/core';
import { MDBModalRef } from 'ng-uikit-pro-standard';
import { AuthenticationService } from '../_services/authentication.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {
  heading: 'Avviso';
  content: {
    description: '';
    description1: 'Vuoi rinnovarla?'
  };

  constructor(
    public modalRef: MDBModalRef,
    private authenticationService1: AuthenticationService
    ) {}

  refresh() {
    this.authenticationService1.tokenRefresh();
    this.modalRef.hide();
  }

  closeModal() {
    this.modalRef.hide();
  }

}
