import { Router } from '@angular/router';
import { Injectable, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import {User} from '../_models/user.model';
import { AlertService } from './alert.service';
import { Risposta } from '../_models/risposta.model';
import { MDBModalRef, MDBModalService, ModalDirective } from 'ng-uikit-pro-standard';
import { ModalComponent } from '../modal/modal.component';
import { Telefono } from '../_models/telefono.model';
import { environment } from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  @ViewChild('demoBasic', { static: true }) demoBasic: ModalDirective;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  private show5: boolean;
  private show1: boolean;
  datiRisposta: User;
  risposta: Risposta;
  interval;
  modalRef: MDBModalRef;
  descrizioneModal: string;

constructor(
  // variabili per la visualizzazione una volta della modal
  private modalService: MDBModalService,
  private http: HttpClient,
  private router: Router,
  public alertService: AlertService) {
      this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
      this.currentUser = this.currentUserSubject.asObservable();
}

public get currentUserValue(): User {
  return this.currentUserSubject.value;
}

public url = environment.apiEndpoint;
httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

  login(username: string, pwd: string) {

      return this.http.post(this.url + '/user/login', {
        nome: username,
        password: pwd
    }, this.httpOptions)
    .subscribe(response => {
      this.datiRisposta = JSON.parse(JSON.stringify(response));

      if (this.datiRisposta.esito === 'OK') {
          localStorage.setItem('currentUser', JSON.stringify(this.datiRisposta._id));
          localStorage.setItem('token', JSON.stringify(this.datiRisposta.token));
          this.currentUserSubject.next(this.datiRisposta);
          this.router.navigate(['home']);
        } else {
          this.alertService.error(JSON.stringify(this.datiRisposta.esito) );
        }
      }, (Error) => {
        this.alertService.error('Verificare la connessione di rete');
      });

  }

  logout() {
    // remove user from local storage to log user out
    localStorage.clear();
    this.currentUserSubject.next(null);
    this.router.navigate(['login']);
  }

  startTimer(timeleft: number) {
    const timeleftInit = timeleft;
    this.interval = setInterval(() => {
      if (timeleft > 0) {
        timeleft--;
        // console.log(timeleft);
      } else {
        if (localStorage.getItem('currentUser') !== null) {
          timeleft = timeleftInit;
          this.tokenCheck(localStorage.getItem('token'));
        } else {
          timeleft = timeleftInit;
        }
      }
    }, 1000);

  }


  tokenCheck(tokenBE: string) {
    return this.http.post(this.url + '/user/checkToken', {
      token: tokenBE.replace('"', '').replace('"', '')
    }, this.httpOptions)
    .subscribe(response => {
      if (response !== undefined) {
        this.risposta = JSON.parse(JSON.stringify(response));
        if (parseInt(this.risposta.time, 10) > 300 && parseInt(this.risposta.time, 10) < 60) {
          if (this.show5 === false) {
            // console.log('tempo compreso tra 10 e 20');
            this.openModal('5');
            this.show5 = true;
          }
          // console.log('tempo compreso tra 10 e 20');
        } else if (parseInt(this.risposta.time, 10) < 60) {
          if (this.show1 === false) {
            console.log('tempo minore di 10');
            this.show1 = true;
            this.openModal('1');
          }
        }
      }
    });
  }

  tokenRefresh() {
    // rimetto le variabili che fanno visualizzare la modal a false
    this.show1 = false;
    this.show5 = false;
    const tokenBE = localStorage.getItem('token');

    if (tokenBE === null) {
      this.router.navigate(['login']);
    } else {
      return this.http.post(this.url + '/user/refresh', {
        token: tokenBE.replace('"', '').replace('"', '')
      }, this.httpOptions)
      .subscribe(response => {
        if (response !== undefined) {
          this.risposta = JSON.parse(JSON.stringify(response));
          localStorage.removeItem('token');
          localStorage.setItem('token', this.risposta.token);
        }
      });
    }
}

  openModal(time: string) {

    if (time === '1') {
      this.descrizioneModal = 'La sessione scadrà tra ' + time + ' minuto...';
    } else {
      this.descrizioneModal = 'La sessione scadrà tra ' + time + ' minuti...';
    }
    const modalOptions = {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'modal-side modal-bottom-right',
      containerClass: 'right',
      animated: true,
      data: {
          heading: 'Avviso',
          content: {
            description: this.descrizioneModal,
            description1: 'Vuoi rinnovarla?'
          }
      }
    };
    this.modalRef = this.modalService.show(ModalComponent, modalOptions);

    // inizializzo un timer per chiudere automaticamente la modal
    setTimeout(() => {
      this.modalRef.hide();
    }, 10000);
  }

  // funzione che genera un array per il telefono
  sendTelefono() {
    const tel = Array<Telefono>();
    const telef = new Telefono();
    telef.numero = '123456789';
    telef.tipo = 'lavoro';
    tel.push(telef);
    tel.push(telef);

    return this.http.post(this.url + '/cliente/createCliente', {
      nomeCognome: 'provaSend',
      codCliente: '123456',
      telefono: tel
    }, this.httpOptions)
    .subscribe(response => {});
  }
}

