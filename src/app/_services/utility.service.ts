import { Router } from '@angular/router';
import { Injectable, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import {User} from '../_models/user.model';
import { AlertService } from './alert.service';
import { Risposta } from '../_models/risposta.model';
import { MDBModalRef, MDBModalService, ModalDirective, turnState } from 'ng-uikit-pro-standard';
import { ModalComponent } from '../modal/modal.component';
import { Telefono } from '../_models/telefono.model';
import { environment } from '../../environments/environment';
import { AuthenticationService } from './authentication.service';
import { DatePipe } from '@angular/common';
import { Cliente } from '../_models/client.model';
import { Impianti } from '../_models/impianti.model';
import { Chiavi } from '../_models/chiavi.model';

@Injectable({providedIn: 'root'})
export class UtilityService {
  private datiRisposta: any;
  private clienteDaSalvare: Cliente;
  private logOut = false;

constructor(
  // variabili per la visualizzazione una volta della modal
  private datapipe: DatePipe,
  private http: HttpClient,
  private router: Router,
  private auth: AuthenticationService) {
  }

  public url = environment.apiEndpoint;
httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

public getTipoTelMail() {
  if (localStorage.getItem('tipoTelMail') !== null) {
       const data = JSON.parse(localStorage.getItem('tipoTelMail'));
       const valoriSelect = [];
       valoriSelect.push({ value: 'scegli', label: 'Scegli il tipo' }),
          data.forEach((el: any) => {
            valoriSelect.push({ value: el.tipo, label: el.tipo});
          }, (Error) => {
            this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
          });
       return valoriSelect;

  } else {
    const valoriSelect = [];
    this.http.get(this.url + '/tipoTelMail/getTipoTelMail', this.httpOptions)
        .subscribe(response => {
          localStorage.setItem('tipoTelMail', JSON.stringify(response));
          const data = JSON.parse(localStorage.getItem('tipoTelMail'));
          valoriSelect.push({ value: 'scegli', label: 'Scegli il tipo' }),
          data.forEach((el: any) => {
            valoriSelect.push({ value: el.tipo, label: el.tipo});
          });
          return valoriSelect;
    }, (Error) => {
      this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
    });
  }
}

public getImpianti() {
  if (localStorage.getItem('utente') !== null) {
       const data = JSON.parse(localStorage.getItem('utente'));
       return data;
  } else {
    // alert('Nessun Cliente selezinato');
  }
}

StringToDate(date) {
  if (date === '') {
    date = null;
  }
  return new Date(this.datapipe.transform(date, 'yyyy-MM-dd'));
}

fillInputDate(data: Date, giraData = false ) {
  if (data.toISOString.length !== 0 ) {
  if (data.toISOString !== undefined) {
    // if ( data.toISOString.length !== 0) {
      if (new Date(data).toISOString().substring(0, 10) !== '1970-01-01') {
        if (giraData) {
          const anno = new Date(data).toISOString().substring(0, 4);
          const mese = new Date(data).toISOString().substring(5, 7);
          const giorno = new Date(data).toISOString().substring(8, 10);
          return giorno + '-' + mese + '-' + anno;
        } else {
          return new Date(data).toISOString().substring(0, 10);
        }
      }
    // } else {
    //  return '';
    // }
  } else {
    return '';
  }
} else {
  return '';
}
}

public getStatoCivile() {
  this.http.get(this.url + '/utility/getUtilityStatoCivile', this.httpOptions)
      .subscribe(response => {
        localStorage.setItem('statoCivile', JSON.stringify(response));
        console.log(localStorage.getItem('statoCivile'));
  });
}

public getProfessione() {
  this.http.get(this.url + '/utility/getUtilityProfessione', this.httpOptions)
      .subscribe(response => {
        localStorage.setItem('professione', JSON.stringify(response));
        console.log(localStorage.getItem('professione'));
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });
}

public getServizio() {
  this.http.get(this.url + '/utility/getUtilityServizio', this.httpOptions)
      .subscribe(response => {
        localStorage.setItem('servizio', JSON.stringify(response));
        console.log(localStorage.getItem('servizio'));
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });
}

public getTipoCliente() {
  this.http.get(this.url + '/utility/getUtilityTipoCliente', this.httpOptions)
      .subscribe(response => {
        localStorage.setItem('tipoCliente', JSON.stringify(response));
        console.log(localStorage.getItem('tipoCliente'));
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });
}

public getTipoIntervento() {
  this.http.get(this.url + '/utility/getUtilityTipoIntervento', this.httpOptions)
      .subscribe(response => {
        localStorage.setItem('tipoIntervento', JSON.stringify(response));
        console.log(localStorage.getItem('tipoIntervento'));
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });
}

public getCentrale() {
    this.http.get(this.url + '/utility/getUtilityCentrale', this.httpOptions)
        .subscribe(response => {
          localStorage.setItem('centrale', JSON.stringify(response));
          console.log(localStorage.getItem('centrale'));
    }, (Error) => {
      this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
    });
}

public getIva() {
  this.http.get(this.url + '/utility/getUtilityIva', this.httpOptions)
      .subscribe(response => {
        localStorage.setItem('iva', JSON.stringify(response));
        console.log(localStorage.getItem('iva'));
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });
}

public getDisdetta() {
  this.http.get(this.url + '/utility/getUtilityDisdetta', this.httpOptions)
      .subscribe(response => {
        localStorage.setItem('disdetta', JSON.stringify(response));
        console.log(localStorage.getItem('disdetta'));
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });
}

public getTipoLocale() {
  this.http.get(this.url + '/utility/getUtilityTipoLocale', this.httpOptions)
      .subscribe(response => {
        localStorage.setItem('tipoLocale', JSON.stringify(response));
        console.log(localStorage.getItem('tipoLocale'));
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });
}

public getPiano() {
    this.http.get(this.url + '/utility/getUtilityPiano', this.httpOptions)
        .subscribe(response => {
          localStorage.setItem('piano', JSON.stringify(response));
          console.log(localStorage.getItem('piano'));
    }, (Error) => {
      this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
    });
}

public getCalendarColor() {
      this.http.get(this.url + '/calendar/getCalendar', this.httpOptions)
          .subscribe(response => {
            localStorage.setItem('calendarColors', JSON.stringify(response));
           // console.log(this.calendarColorResult);
      }, (Error) => {
        this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
      });

}

public formatValuta(valuta: number) {
  console.log(new Intl.NumberFormat('it-IT', { style: 'currency', currency: 'EUR' }).format(valuta));
  console.log(new Intl.NumberFormat().format(valuta));
  const valuta1 = new Intl.NumberFormat('it-IT', { style: 'currency', currency: 'EUR' }).format(valuta);
  return valuta1;
}

public editCliente() {
  localStorage.setItem('utenteEdit', localStorage.getItem('utente'));
  localStorage.removeItem('utente');
}

public updateClienteVisibility(field: string, id: any, newValue: any) {
  // tslint:disable-next-line: max-line-length
  this.http.put(this.url + '/cliente/' + id + '/update', {key: field, value: newValue, idCliente: id }, this.httpOptions).subscribe(responseUpdate => {
    // SALVO LE EMAIL
    this.datiRisposta = JSON.parse(JSON.stringify(responseUpdate));
    if (this.datiRisposta.esito === 'OK') {
      this.auth.alertService.error(JSON.stringify(this.datiRisposta.esito) );
    } else {
      this.auth.alertService.error(JSON.stringify(this.datiRisposta.esito) );
    }
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });
}



public salvaNuovoCliente(logOut: boolean = false) {
  this.logOut = logOut;
  this.clienteDaSalvare = JSON.parse(localStorage.getItem('utenteTemp'));
  if (this.clienteDaSalvare._id !== undefined) {
    this.updateClienteVisibility('Visibility', this.clienteDaSalvare._id, false);
  }
  // tslint:disable-next-line: max-line-length
  // const procedi = confirm('Stai per salvare il Cliente ' + this.clienteDaSalvare.nomeCognome + ' - CODICE: ' + this.clienteDaSalvare.codCliente);
  if (true) {
    this.datiRisposta = null;
    this.salvaTelefonoCliente();
  }

}

private salvaTelefonoCliente() {
  // this.clienteDaSalvare = JSON.parse(localStorage.getItem('utenteTemp'));
  this.http.post(this.url + '/telefono/create', {cliente: this.clienteDaSalvare, }, this.httpOptions).subscribe(response => {
  // SALVO I TELEOFONI
    this.datiRisposta = JSON.parse(JSON.stringify(response));
    if (this.datiRisposta.esito === 'OK') {
      let i = 0;
      try {
        this.datiRisposta.telefoni.forEach(element => {
        this.clienteDaSalvare.telefoni[i]._id = element._id;
        i++;
        });
        console.log(this.clienteDaSalvare);
      } catch (error) {
        console.log('No telefoni slavati');
      } finally {
        this.salvaEmailCliente();
      }
    } else {
      this.auth.alertService.error(JSON.stringify(this.datiRisposta.esito) );
    }
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });
}

private salvaEmailCliente() {
  this.http.post(this.url + '/email/create', {cliente: this.clienteDaSalvare, }, this.httpOptions).subscribe(responseMail => {
    // SALVO LE EMAIL
    this.datiRisposta = JSON.parse(JSON.stringify(responseMail));
    if (this.datiRisposta.esito === 'OK') {
      let i = 0;
      try {
        this.datiRisposta.email.forEach(element => {
        this.clienteDaSalvare.email[i]._id = element._id;
        i++;
        });
        console.log(this.clienteDaSalvare);
      } catch (error) {
        console.log('No Email da salvare');
      } finally {
        this.salvaPorteCliente();
      }
    } else {
      this.auth.alertService.error(JSON.stringify(this.datiRisposta.esito) );
    }
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });
}


private salvaPorteCliente(start = 0) {

  if (this.clienteDaSalvare.porte !== undefined && this.clienteDaSalvare.porte.length > start) {
    let i = 0;
    i = i + start;
    if (this.clienteDaSalvare.porte[start].defenderSerratureCilindri.length === 0) {
      this.salvaPortaCliente(start);
    } else {
      this.salvaCilDefCliente(i);
    }
  } else {
    this.salvaImpiantoCliente();
  }

}

private salvaCilDefCliente(idPorta: any) {
  // tslint:disable-next-line: max-line-length
  this.http.post(this.url + '/defSer/create', {cliente: this.clienteDaSalvare.porte[idPorta], }, this.httpOptions).subscribe(responsePorte => {
    // SALVO LE PORTE
    this.datiRisposta = JSON.parse(JSON.stringify(responsePorte));
    if (this.datiRisposta.esito === 'OK') {
      let i = 0;
      try {
        this.datiRisposta.defSer.forEach(element => {
        this.clienteDaSalvare.porte[idPorta].defenderSerratureCilindri[i]._id = element._id;
        i++;
        });
        // console.log(this.clienteDaSalvare);
      } catch (error) {
        console.log('No Serrature da salvare');
      } finally {
        this.salvaPortaCliente(idPorta);
      }
    } else {
      this.auth.alertService.error(JSON.stringify(this.datiRisposta.esito) );
    }
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });
}

private salvaPortaCliente(idPorta: any) {
  // tslint:disable-next-line: max-line-length
  this.http.post(this.url + '/porta/create', {cliente: this.clienteDaSalvare.porte[idPorta], }, this.httpOptions).subscribe(responsePorte => {
    // SALVO LE PORTE
    this.datiRisposta = JSON.parse(JSON.stringify(responsePorte));
    if (this.datiRisposta.esito === 'OK') {
      try {
        this.clienteDaSalvare.porte[idPorta]._id = this.datiRisposta.porte._id;
        console.log(this.clienteDaSalvare);
      } catch (error) {
        console.log('No Porte da salvare');
      } finally {
        this.salvaPorteCliente(idPorta +1);
      }
    } else {
      this.auth.alertService.error(JSON.stringify(this.datiRisposta.esito) );
    }
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });
}


private salvaImpiantoCliente(start = 0) {

  if (this.clienteDaSalvare.impianti !== undefined) {
    let i = 0;
    i = i + start;
    this.salvaChiaviCliente(i);
  } else {
    this.salvaClienteFinale();
  }

}

private salvaChiaviCliente(idImpianto: any) {
  // tslint:disable-next-line: max-line-length
  this.http.post(this.url + '/chiave/create', {cliente: this.clienteDaSalvare.impianti[idImpianto], }, this.httpOptions).subscribe(responsePorte => {
    // SALVO LE PORTE
    this.datiRisposta = JSON.parse(JSON.stringify(responsePorte));
    if (this.datiRisposta.esito === 'OK') {
      let i = 0;
      try {
        this.datiRisposta.chiavi.forEach(element => {
        this.clienteDaSalvare.impianti[idImpianto].chiavi[i]._id = element._id;
        i++;
        });
        console.log(this.clienteDaSalvare);
      } catch (error) {
        console.log('No Chiavi da salvare');
      } finally {
        this.salvaFattureCliente(idImpianto);
      }
    } else {
      this.auth.alertService.error(JSON.stringify(this.datiRisposta.esito) );
    }
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });
}

private salvaFattureCliente(idImpianto: any) {
  // tslint:disable-next-line: max-line-length
  this.http.post(this.url + '/fattura/create', {cliente: this.clienteDaSalvare.impianti[idImpianto], }, this.httpOptions).subscribe(responseFatture => {
    // SALVO LE PORTE
    this.datiRisposta = JSON.parse(JSON.stringify(responseFatture));
    if (this.datiRisposta.esito === 'OK') {
      let i = 0;
      try {
        this.datiRisposta.fattura.forEach(element => {
        this.clienteDaSalvare.impianti[idImpianto].fatture[i]._id = element._id;
        i++;
        });
        console.log(this.clienteDaSalvare);
      } catch (error) {
        console.log('No Chiavi da salvare');
      } finally {
        this.salvaInterventiCliente(idImpianto);
      }
    } else {
      this.auth.alertService.error(JSON.stringify(this.datiRisposta.esito) );
    }
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });
}
private salvaInterventiCliente(idImpianto: any) {
  // tslint:disable-next-line: max-line-length
  this.http.post(this.url + '/intervento/create', {cliente: this.clienteDaSalvare.impianti[idImpianto], }, this.httpOptions).subscribe(responsePorte => {
    // SALVO LE PORTE
    this.datiRisposta = JSON.parse(JSON.stringify(responsePorte));
    if (this.datiRisposta.esito === 'OK') {
      let i = 0;
      try {
        this.datiRisposta.intervento.forEach(element => {
        this.clienteDaSalvare.impianti[idImpianto].intervento[i]._id = element._id;
        i++;
        });
        console.log(this.clienteDaSalvare);
      } catch (error) {
        console.log('No Interventi da salvare');
      } finally {
        this.salvaSMTImpiantoCliente(idImpianto);
      }
    } else {
      this.auth.alertService.error(JSON.stringify(this.datiRisposta.esito) );
    }
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });
}


private salvaSMTImpiantoCliente(idImpianto) {
  // tslint:disable-next-line: max-line-length
  this.http.post(this.url + '/smtAnno/create', {cliente: this.clienteDaSalvare.impianti[idImpianto], }, this.httpOptions).subscribe(responseE => {
    // SALVO L'SMT_IMPIANTO
    this.datiRisposta = JSON.parse(JSON.stringify(responseE));
    if (this.datiRisposta.esito === 'OK') {
      let i = 0;
      try {
        this.datiRisposta.annoSMT.forEach(element => {
        this.clienteDaSalvare.impianti[idImpianto].smt[i]._id = element._id;
        i++;
        });
        console.log(this.clienteDaSalvare);
      } catch (error) {
        console.log('No SMT da salvare');
      } finally {
        idImpianto++;
        if (idImpianto === this.clienteDaSalvare.impianti.length) {
          this.salvaImpianto();
        } else {
          this.salvaImpiantoCliente(idImpianto);
        }
      }
    } else {
      this.auth.alertService.error(JSON.stringify(this.datiRisposta.esito) );
    }
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });
}

private salvaImpianto() {

  // tslint:disable-next-line: max-line-length
  this.http.post(this.url + '/impianto/create', {cliente: this.clienteDaSalvare, }, this.httpOptions).subscribe(responseImpianto => {
    // SALVO L'IMPIANTO
    this.datiRisposta = JSON.parse(JSON.stringify(responseImpianto));
    if (this.datiRisposta.esito === 'OK') {
      let i = 0;
      try {
        this.datiRisposta.impianti.forEach(element => {
        this.clienteDaSalvare.impianti[i]._id = element._id;
        i++;
        });
        console.log(this.clienteDaSalvare);
      } catch (error) {
        console.log('No Impianto da salvare');
      } finally {
        this.salvaClienteFinale();
      }
    } else {
      this.auth.alertService.error(JSON.stringify(this.datiRisposta.esito) );
    }
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });
}


private salvaClienteFinale() {
  this.clienteDaSalvare.visibility = true;
  this.http.post(this.url + '/cliente/createCliente', {cliente: this.clienteDaSalvare, }, this.httpOptions).subscribe(responseCliente => {
    this.datiRisposta = JSON.parse(JSON.stringify(responseCliente));
    if (this.datiRisposta.esito === 'OK') {
      this.auth.alertService.success('Salvataggio avvenuto con successo!!!');
      localStorage.setItem('utente', localStorage.getItem('utenteTemp'));
      this.localStorageCleanTempData();
      this.router.navigate(['clienti']);
      this.datiRisposta = null;
      if (this.logOut) {
        localStorage.clear();
        this.auth.logout();
      }
    } else {
      this.auth.alertService.error(JSON.stringify(this.datiRisposta.esito) );
    }
  }, (Error) => {
    this.auth.alertService.error('Errore: BackEnd assente o non raggiungibile!');
  });

}

public localStorageCleanTempData() {
  localStorage.removeItem('utenteEdit');
  localStorage.removeItem('utenteTemp');
  localStorage.removeItem('impiantoSelected');
  localStorage.removeItem('portaSelected');
  localStorage.removeItem('interventoSelected');

}

}

