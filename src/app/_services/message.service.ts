import { ClientiComponent } from './../clienti/clienti.component';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class MessageService {
    private subject = new Subject<any>();
    sendMessage(message) {
        this.subject.next({
          nome: message.nomeCognome,
          codCliente: message.codCliente
         });
        // console.log(message);
    }
    clearMessage() {
        this.subject.next();
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}
