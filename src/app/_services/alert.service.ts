import { Injectable } from '@angular/core';
import { ToastService } from 'ng-uikit-pro-standard';

@Injectable({providedIn: 'root'})

export class AlertService {
  constructor(
    private toastrService: ToastService ) {}

    success(message: string) {
      this.toastrService.success(message);
    }

    error(message: string) {
      this.toastrService.error(message);
    }


  }
