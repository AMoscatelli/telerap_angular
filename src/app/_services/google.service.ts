import { AppuntamentiComponent } from './../appuntamenti/appuntamenti.component';
import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class GoogleService {

  constructor(
    private appuntamenti: AppuntamentiComponent) {
    }
  public googleSignIn() {
    this.appuntamenti.getAuth();
  }
}
