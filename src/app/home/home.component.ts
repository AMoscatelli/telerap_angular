import { Router } from '@angular/router';
import { AppComponent } from './../app.component';
import { AuthenticationService } from './../_services/authentication.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(
    private router: Router,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {}

    public ngOnInit(): void {
      // inizializzo la variabile utente in localStorage
      if (localStorage.getItem('utente') !== null) {
        localStorage.removeItem('utente');
      }
      // -----
      this.authenticationService.tokenRefresh();
      setTimeout(() => this.appComponent.setDiv(false, false), 0);
      setTimeout(() => this.appComponent.mostraGoogle(false), 0);
    }

    goTo(link: string) {
      this.router.navigate([link]);
    }

}
