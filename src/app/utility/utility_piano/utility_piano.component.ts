import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilityService } from 'src/app/_services/utility.service';
import { AppComponent } from '../../app.component';
import { AuthenticationService } from '../../_services/authentication.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Form } from '@angular/forms';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-utilitypiano',
  templateUrl: './utility_piano.component.html',
  styleUrls: ['./utility_piano.component.scss']
})
export class UtilityPianoComponent implements AfterViewInit {
  @ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
  headElements = ['ID', 'Piano'];
  save: boolean; edit: boolean;
  elementTemp: Array<{id: number, piano: string}> = [];
  elementPiano = [];
  visibleItems = 7;
  searchText = '';
  highlightedRow: any = null;
  pianoForm: FormGroup;
  pianoBE: Array<any>;
  public url = environment.apiEndpoint;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };


  constructor(
    private router: Router,
    private http: HttpClient,
    private utilityService: UtilityService,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {}

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
      this.save = true;
      this.edit = false;
      this.authenticationService.tokenRefresh();
      // inizializzo la variabile utente in localStorage
      if (localStorage.getItem('utente') !== null) {
        localStorage.removeItem('utente');
      }
      setTimeout(() => this.appComponent.setDiv(false , false), 0);
      // inizializzo la FormGroup
      this.pianoForm = new FormGroup({
        idPiano: new FormControl(),
        piano: new FormControl(),
        model: new FormControl(),
      });

      this.pianoForm.patchValue({
        model: 'piano'
      });
      // --------
      // richiamo la funzione che mi restituisce i risultati BE di centrale
      setTimeout(() => this.mdbTableEditor.dataArray = this.elementPiano, 0);
    }

    ngAfterViewInit() {
      if (JSON.parse(localStorage.getItem('piano')) !== null) {
        this.pianoBE = JSON.parse(localStorage.getItem('piano'));
        let index;
        index = 1;
        this.pianoBE.forEach(element => {
          this.elementPiano.push({idValue: element._id, id: index, piano: element.piano});
          index++;
          console.log(this.elementPiano);
        });
        setTimeout(() => this.mdbTableEditor.dataArray = this.elementPiano, 0);
      }
    }

    setValue(item) {
      localStorage.setItem('idValue', item.idValue);
      if (item === null) {
        this.save = true;
        this.edit = false;
        this.pianoForm.patchValue({
          piano: '',
        });
      } else {
        this.save = false;
        this.edit = true;
        this.pianoForm.patchValue({
          piano: item.piano
        });
      }
    }

    salvaUtility() {
      this.http.post(this.url + '/utility/create', {
        utility: this.pianoForm.value,
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.utilityService.getPiano();
          setTimeout(() => this.refreshPage(), 500);
        });
    }

    deleteUtility() {
      this.http.post(this.url + '/utility/delete', {
        model: this.pianoForm.get('model').value,
        id: localStorage.getItem('idValue')
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.salvaUtility();
        });
    }

    goTo(link: string) {
      // this.salvaClienteTemp();
      this.router.navigate([link]);
    }

    refreshPage() {
      this.router.navigateByUrl('blank', { skipLocationChange: true }).then(() => {
        setTimeout(() => this.router.navigate(['utility_piano']), 200);
    });
    }
}

