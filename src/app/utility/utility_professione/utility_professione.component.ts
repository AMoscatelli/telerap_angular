import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilityService } from 'src/app/_services/utility.service';
import { AppComponent } from '../../app.component';
import { AuthenticationService } from '../../_services/authentication.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Form } from '@angular/forms';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-utilityprofessione',
  templateUrl: './utility_professione.component.html',
  styleUrls: ['./utility_professione.component.scss']
})
export class UtilityProfessioneComponent implements AfterViewInit {
  @ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
  headElements = ['ID', 'Professione'];
  save: boolean; edit: boolean;
  elementTemp: Array<{id: number, professione: string}> = [];
  elementProfessione = [];
  visibleItems = 7;
  searchText = '';
  highlightedRow: any = null;
  professioneForm: FormGroup;
  professioneBE: Array<any>;
  public url = environment.apiEndpoint;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };


  constructor(
    private router: Router,
    private http: HttpClient,
    private utilityService: UtilityService,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {}

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
      this.save = true;
      this.edit = false;
      this.authenticationService.tokenRefresh();
      // inizializzo la variabile utente in localStorage
      if (localStorage.getItem('utente') !== null) {
        localStorage.removeItem('utente');
      }
      setTimeout(() => this.appComponent.setDiv(false , false), 0);
      // inizializzo la FormGroup
      this.professioneForm = new FormGroup({
        idProfessione: new FormControl(),
        professione: new FormControl(),
        model: new FormControl(),
      });

      this.professioneForm.patchValue({
        model: 'professione'
      });
      // --------
      // richiamo la funzione che mi restituisce i risultati BE di centrale
      setTimeout(() => this.mdbTableEditor.dataArray = this.elementProfessione, 0);
    }

    ngAfterViewInit() {
      if (JSON.parse(localStorage.getItem('professione')) !== null) {
        this.professioneBE = JSON.parse(localStorage.getItem('professione'));
        let index;
        index = 1;
        this.professioneBE.forEach(element => {
          this.elementProfessione.push({idValue: element._id, id: index, professione: element.professione});
          index++;
          console.log(this.elementProfessione);
        });
        setTimeout(() => this.mdbTableEditor.dataArray = this.elementProfessione, 0);
      }
    }

    setValue(item) {
      localStorage.setItem('idValue', item.idValue);
      if (item === null) {
        this.save = true;
        this.edit = false;
        this.professioneForm.patchValue({
          professione: '',
        });
      } else {
        this.save = false;
        this.edit = true;
        this.professioneForm.patchValue({
          professione: item.professione
        });
      }
    }

    salvaUtility() {
      this.http.post(this.url + '/utility/create', {
        utility: this.professioneForm.value,
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.utilityService.getProfessione();
          setTimeout(() => this.refreshPage(), 500);
        });
    }

    deleteUtility() {
      this.http.post(this.url + '/utility/delete', {
        model: this.professioneForm.get('model').value,
        id: localStorage.getItem('idValue')
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.salvaUtility();
        });
    }

    goTo(link: string) {
      // this.salvaClienteTemp();
      this.router.navigate([link]);
    }

    refreshPage() {
      this.router.navigateByUrl('blank', { skipLocationChange: true }).then(() => {
        setTimeout(() => this.router.navigate(['utility_professione']), 200);
    });
    }
}

