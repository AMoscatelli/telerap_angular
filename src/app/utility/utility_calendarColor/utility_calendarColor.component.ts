import { AlertService } from './../../_services/alert.service';
import { colore } from './../../colors';
import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilityService } from 'src/app/_services/utility.service';
import { AppComponent } from 'src/app/app.component';
import { AuthenticationService } from 'src/app/_services';
import { environment } from 'src/environments/environment';
import { FormGroup, FormControl } from '@angular/forms';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { IDCalendar } from 'src/app/_models/calendarId.model';
import { CalendarColor } from 'src/app/_models/calendarColor.model';
import { ModalDirective } from 'ng-uikit-pro-standard';

@Component({
  selector: 'app-utilitycalendarscolor',
  templateUrl: './utility_calendarColor.component.html',
  styleUrls: ['./utility_calendarColor.component.scss']
})

export class UtilityCalendarColorComponent implements AfterViewInit {
  @ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
  @ViewChild('frame', { static: true }) modal: ModalDirective;
  headElements = ['Nome', 'Colore'];
  elementColor = [];
  visibleItems = 7;
  searchText = '';
  highlightedRow: any = null;
  colorForm: FormGroup;
  colorBE: Array<any>;
  calendarUtente: Array<any>;
  default: boolean; verde: boolean; rosso: boolean; giallo: boolean;
  template: true;
  public url = environment.apiEndpoint;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(
    private alertService: AlertService,
    private router: Router,
    private http: HttpClient,
    private utilityService: UtilityService,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {}

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
      this.authenticationService.tokenRefresh();
      // inizializzo la variabile utente in localStorage
      if (localStorage.getItem('utente') !== null) {
        localStorage.removeItem('utente');
      }
      setTimeout(() => this.appComponent.setDiv(false , false), 0);
      this.colorForm = new FormGroup({
        model: new FormControl()
      });
      this.colorForm.patchValue({
        model: 'calendarcolor'
      });
      // --------
      // richiamo la funzione che mi restituisce i risultati BE di centrale
      setTimeout(() => this.mdbTableEditor.dataArray = this.elementColor, 0);
    }

    ngAfterViewInit() {
      if (JSON.parse(localStorage.getItem('calendarColors')) !== null) {
        this.colorBE = JSON.parse(localStorage.getItem('calendarColors'));
        console.log(localStorage.getItem('calendarUtente'));
        if (localStorage.getItem('calendarUtente') === null) {
          this.getCalendar();
        } else {
          this.calendarUtente = JSON.parse(localStorage.getItem('calendarUtente'));
          let calendariBE = new CalendarColor();
          let calendariUtente = new IDCalendar();
          this.colorBE.forEach(element => {
            calendariBE = element;
            this.calendarUtente.forEach(cal => {
              calendariUtente = cal;
              if (calendariBE.nome === calendariUtente.id) {
                this.elementColor.push(element);
              }
            });
          });
        }
        setTimeout(() => this.mdbTableEditor.dataArray = this.elementColor, 0);
      }
    }

    goTo(link: string) {
      // this.salvaClienteTemp();
      this.router.navigate([link]);
    }

    refreshPage() {
      this.router.navigateByUrl('blank', { skipLocationChange: true }).then(() => {
        setTimeout(() => this.router.navigate(['utility_calendarscolor']), 200);
    });
  }

  public getCalendar() {
    this.http.post(this.url + '/google/getEvent', {
      }, this.httpOptions)
      .subscribe(response => {
        localStorage.setItem('calendarUtente', JSON.stringify(response));
        this.calendarUtente = JSON.parse(localStorage.getItem('calendarUtente'));
        let calendariBE = new CalendarColor();
        let calendariUtente = new IDCalendar();
        this.colorBE.forEach(element => {
          calendariBE = element;
          this.calendarUtente.forEach(cal => {
            calendariUtente = cal;
            if (calendariBE.nome === calendariUtente.id) {
              this.elementColor.push(element);
            }
          });
        });
      });
  }

  public setValue() {
    console.log('modal aperta');
    console.log(localStorage.getItem('coloreDB'));
    switch (localStorage.getItem('coloreDB')) {
      case 'default':
        this.default = true;
        break;
      case 'verde':
        this.verde = true;
        break;
      case 'rosso':
        this.rosso = true;
        break;
      case 'giallo':
        this.giallo = true;
        break;
    }
    console.log(this.default);
    console.log(this.verde);
  }

  public salvaColorSuDB() {
    this.http.put(this.url + '/calendar/update', {
      id: localStorage.getItem('idCalendar'),
      colore: localStorage.getItem('coloreDB')
    }, this.httpOptions)
    .subscribe(response => {
      console.log(response);
      this.modal.hide();
      this.alertService.success('Colore calendario aggiornato');
      localStorage.removeItem('calendarColors');
      this.utilityService.getCalendarColor();
      setTimeout(() => this.refreshPage(), 500);
      });
  }

  change(evento) {
    localStorage.setItem('coloreDB', evento);
    this.resetValue();
    this.setValue();
  }

  public resetValue() {
    this.default = false;
    this.verde = false;
    this.rosso = false;
    this.giallo = false;
  }
  // funzione che spunta l'opzione dal db
  public setRadio(item) {
    // console.log(item.colore);
    console.log(item._id);
    localStorage.setItem('idCalendar', item._id);
    localStorage.setItem('coloreDB', item.colore);
    this.modal.show();
  }
}

