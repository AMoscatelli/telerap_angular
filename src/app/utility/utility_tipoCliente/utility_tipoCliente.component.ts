import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilityService } from 'src/app/_services/utility.service';
import { AppComponent } from '../../app.component';
import { AuthenticationService } from '../../_services/authentication.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Form } from '@angular/forms';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-utilitytipocliente',
  templateUrl: './utility_tipoCliente.component.html',
  styleUrls: ['./utility_tipoCliente.component.scss']
})
export class UtilityTipoClienteComponent implements AfterViewInit {
  @ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
  headElements = ['ID', 'Tipo Cliente'];
  save: boolean; edit: boolean;
  elementTemp: Array<{id: number, tipoCliente: string}> = [];
  elementTipoCliente = [];
  visibleItems = 7;
  searchText = '';
  highlightedRow: any = null;
  tipoClienteForm: FormGroup;
  tipoClienteBE: Array<any>;
  public url = environment.apiEndpoint;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };


  constructor(
    private router: Router,
    private http: HttpClient,
    private utilityService: UtilityService,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {}

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
      this.save = true;
      this.edit = false;
      this.authenticationService.tokenRefresh();
      // inizializzo la variabile utente in localStorage
      if (localStorage.getItem('utente') !== null) {
        localStorage.removeItem('utente');
      }
      setTimeout(() => this.appComponent.setDiv(false , false), 0);
      // inizializzo la FormGroup
      this.tipoClienteForm = new FormGroup({
        idTipoCliente: new FormControl(),
        tipoCliente: new FormControl(),
        model: new FormControl(),
      });

      this.tipoClienteForm.patchValue({
        model: 'tipoCliente'
      });
      // --------
      // richiamo la funzione che mi restituisce i risultati BE di centrale
      setTimeout(() => this.mdbTableEditor.dataArray = this.elementTipoCliente, 0);
    }

    ngAfterViewInit() {
      if (JSON.parse(localStorage.getItem('tipoCliente')) !== null) {
        this.tipoClienteBE = JSON.parse(localStorage.getItem('tipoCliente'));
        let index;
        index = 1;
        this.tipoClienteBE.forEach(element => {
          this.elementTipoCliente.push({idValue: element._id, id: index, tipoCliente: element.tipoCliente});
          index++;
          console.log(this.elementTipoCliente);
        });
        setTimeout(() => this.mdbTableEditor.dataArray = this.elementTipoCliente, 0);
      }
    }

    setValue(item) {
      localStorage.setItem('idValue', item.idValue);
      if (item === null) {
        this.save = true;
        this.edit = false;
        this.tipoClienteForm.patchValue({
          tipoCliente: '',
        });
      } else {
        this.save = false;
        this.edit = true;
        this.tipoClienteForm.patchValue({
          tipoCliente: item.tipoCliente
        });
      }
    }

    salvaUtility() {
      this.http.post(this.url + '/utility/create', {
        utility: this.tipoClienteForm.value,
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.utilityService.getTipoCliente();
          setTimeout(() => this.refreshPage(), 500);
        });
    }

    deleteUtility() {
      this.http.post(this.url + '/utility/delete', {
        model: this.tipoClienteForm.get('model').value,
        id: localStorage.getItem('idValue')
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.salvaUtility();
        });
    }

    goTo(link: string) {
      // this.salvaClienteTemp();
      this.router.navigate([link]);
    }

    refreshPage() {
      this.router.navigateByUrl('blank', { skipLocationChange: true }).then(() => {
        setTimeout(() => this.router.navigate(['utility_tipoCliente']), 200);
    });
    }
}

