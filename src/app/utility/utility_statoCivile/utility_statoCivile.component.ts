import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilityService } from 'src/app/_services/utility.service';
import { AppComponent } from '../../app.component';
import { AuthenticationService } from '../../_services/authentication.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Form } from '@angular/forms';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-utilitystatocivile',
  templateUrl: './utility_statoCivile.component.html',
  styleUrls: ['./utility_statoCivile.component.scss']
})
export class UtilityStatoCivileComponent implements AfterViewInit {
  @ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
  headElements = ['ID', 'Stato Civile'];
  save: boolean; edit: boolean;
  elementTemp: Array<{id: number, statoCivile: string}> = [];
  elementStatoCivile = [];
  visibleItems = 7;
  searchText = '';
  highlightedRow: any = null;
  statoCivileForm: FormGroup;
  statoCivileBE: Array<any>;
  public url = environment.apiEndpoint;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };


  constructor(
    private router: Router,
    private http: HttpClient,
    private utilityService: UtilityService,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {}

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
      this.save = true;
      this.edit = false;
      this.authenticationService.tokenRefresh();
      // inizializzo la variabile utente in localStorage
      if (localStorage.getItem('utente') !== null) {
        localStorage.removeItem('utente');
      }
      setTimeout(() => this.appComponent.setDiv(false , false), 0);
      // inizializzo la FormGroup
      this.statoCivileForm = new FormGroup({
        idStatoCivile: new FormControl(),
        statoCivile: new FormControl(),
        model: new FormControl(),
      });

      this.statoCivileForm.patchValue({
        model: 'statoCivile'
      });
      // --------
      // richiamo la funzione che mi restituisce i risultati BE di centrale
      setTimeout(() => this.mdbTableEditor.dataArray = this.elementStatoCivile, 0);
    }

    ngAfterViewInit() {
      if (JSON.parse(localStorage.getItem('statoCivile')) !== null) {
        this.statoCivileBE = JSON.parse(localStorage.getItem('statoCivile'));
        let index;
        index = 1;
        this.statoCivileBE.forEach(element => {
          this.elementStatoCivile.push({idValue: element._id, id: index, statoCivile: element.statoCivile});
          index++;
          console.log(this.elementStatoCivile);
        });
        setTimeout(() => this.mdbTableEditor.dataArray = this.elementStatoCivile, 0);
      }
    }

    setValue(item) {
      localStorage.setItem('idValue', item.idValue);
      if (item === null) {
        this.save = true;
        this.edit = false;
        this.statoCivileForm.patchValue({
          statoCivile: '',
        });
      } else {
        this.save = false;
        this.edit = true;
        this.statoCivileForm.patchValue({
          statoCivile: item.statoCivile
        });
      }
    }

    salvaUtility() {
      this.http.post(this.url + '/utility/create', {
        utility: this.statoCivileForm.value,
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.utilityService.getStatoCivile();
          setTimeout(() => this.refreshPage(), 500);
        });
    }

    deleteUtility() {
      this.http.post(this.url + '/utility/delete', {
        model: this.statoCivileForm.get('model').value,
        id: localStorage.getItem('idValue')
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.salvaUtility();
        });
    }

    goTo(link: string) {
      // this.salvaClienteTemp();
      this.router.navigate([link]);
    }

    refreshPage() {
      this.router.navigateByUrl('blank', { skipLocationChange: true }).then(() => {
        setTimeout(() => this.router.navigate(['utility_statoCivile']), 200);
    });
    }
}

