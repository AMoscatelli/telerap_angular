import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilityService } from 'src/app/_services/utility.service';
import { AppComponent } from '../../app.component';
import { AuthenticationService } from '../../_services/authentication.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Form } from '@angular/forms';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-utilityiva',
  templateUrl: './utility_iva.component.html',
  styleUrls: ['./utility_iva.component.scss']
})
export class UtilityIvaComponent implements AfterViewInit {
  @ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
  headElements = ['ID', 'Descrizione'];
  save: boolean; edit: boolean;
  elementTemp: Array<{id: number, iva: string}> = [];
  elementIva = [];
  visibleItems = 7;
  searchText = '';
  highlightedRow: any = null;
  ivaForm: FormGroup;
  ivaBE: Array<any>;
  public url = environment.apiEndpoint;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };


  constructor(
    private router: Router,
    private http: HttpClient,
    private utilityService: UtilityService,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {}

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
      this.save = true;
      this.edit = false;
      this.authenticationService.tokenRefresh();
      // inizializzo la variabile utente in localStorage
      if (localStorage.getItem('utente') !== null) {
        localStorage.removeItem('utente');
      }
      setTimeout(() => this.appComponent.setDiv(false , false), 0);
      // inizializzo la FormGroup
      this.ivaForm = new FormGroup({
        idIva: new FormControl(),
        iva: new FormControl(),
        model: new FormControl(),
      });

      this.ivaForm.patchValue({
        model: 'iva'
      });
      // --------
      // richiamo la funzione che mi restituisce i risultati BE di centrale
      setTimeout(() => this.mdbTableEditor.dataArray = this.elementIva, 0);
    }

    ngAfterViewInit() {
      if (JSON.parse(localStorage.getItem('iva')) !== null) {
        this.ivaBE = JSON.parse(localStorage.getItem('iva'));
        let index;
        index = 1;
        this.ivaBE.forEach(element => {
          this.elementIva.push({idValue: element._id, id: index, iva: element.iva});
          index++;
          console.log(this.elementIva);
        });
        setTimeout(() => this.mdbTableEditor.dataArray = this.elementIva, 0);
      }
    }

    setValue(item) {
      localStorage.setItem('idValue', item.idValue);
      if (item === null) {
        this.save = true;
        this.edit = false;
        this.ivaForm.patchValue({
          iva: '',
        });
      } else {
        this.save = false;
        this.edit = true;
        this.ivaForm.patchValue({
          iva: item.iva
        });
      }
    }

    salvaUtility() {
      this.http.post(this.url + '/utility/create', {
        utility: this.ivaForm.value,
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.utilityService.getIva();
          setTimeout(() => this.refreshPage(), 500);
        });
    }

    deleteUtility() {
      this.http.post(this.url + '/utility/delete', {
        model: this.ivaForm.get('model').value,
        id: localStorage.getItem('idValue')
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.salvaUtility();
        });
    }


    goTo(link: string) {
      // this.salvaClienteTemp();
      this.router.navigate([link]);
    }

    refreshPage() {
      this.router.navigateByUrl('blank', { skipLocationChange: true }).then(() => {
        setTimeout(() => this.router.navigate(['utility_iva']), 200);
    });
    }
}

