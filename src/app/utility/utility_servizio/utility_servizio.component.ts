import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilityService } from 'src/app/_services/utility.service';
import { AppComponent } from '../../app.component';
import { AuthenticationService } from '../../_services/authentication.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Form } from '@angular/forms';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-utilityservizio',
  templateUrl: './utility_servizio.component.html',
  styleUrls: ['./utility_servizio.component.scss']
})
export class UtilityServizioComponent implements AfterViewInit {
  @ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
  headElements = ['ID', 'Servizio'];
  save: boolean; edit: boolean;
  elementTemp: Array<{id: number, servizio: string}> = [];
  elementServizio = [];
  visibleItems = 7;
  searchText = '';
  highlightedRow: any = null;
  servizioForm: FormGroup;
  servizioBE: Array<any>;
  public url = environment.apiEndpoint;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };


  constructor(
    private router: Router,
    private http: HttpClient,
    private utilityService: UtilityService,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {}

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
      this.save = true;
      this.edit = false;
      this.authenticationService.tokenRefresh();
      // inizializzo la variabile utente in localStorage
      if (localStorage.getItem('utente') !== null) {
        localStorage.removeItem('utente');
      }
      setTimeout(() => this.appComponent.setDiv(false , false), 0);
      // inizializzo la FormGroup
      this.servizioForm = new FormGroup({
        idServizio: new FormControl(),
        servizio: new FormControl(),
        model: new FormControl(),
      });

      this.servizioForm.patchValue({
        model: 'servizio'
      });
      // --------
      // richiamo la funzione che mi restituisce i risultati BE di centrale
      setTimeout(() => this.mdbTableEditor.dataArray = this.elementServizio, 0);
    }

    ngAfterViewInit() {
      if (JSON.parse(localStorage.getItem('servizio')) !== null) {
        this.servizioBE = JSON.parse(localStorage.getItem('servizio'));
        let index;
        index = 1;
        this.servizioBE.forEach(element => {
          this.elementServizio.push({idValue: element._id, id: index, servizio: element.servizio});
          index++;
          console.log(this.elementServizio);
        });
        setTimeout(() => this.mdbTableEditor.dataArray = this.elementServizio, 0);
      }
    }

    setValue(item) {
      localStorage.setItem('idValue', item.idValue);
      if (item === null) {
        this.save = true;
        this.edit = false;
        this.servizioForm.patchValue({
          servizio: '',
        });
      } else {
        this.save = false;
        this.edit = true;
        this.servizioForm.patchValue({
          servizio: item.servizio
        });
      }
    }

    salvaUtility() {
      this.http.post(this.url + '/utility/create', {
        utility: this.servizioForm.value,
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.utilityService.getServizio();
          setTimeout(() => this.refreshPage(), 500);
        });
    }

    deleteUtility() {
      this.http.post(this.url + '/utility/delete', {
        model: this.servizioForm.get('model').value,
        id: localStorage.getItem('idValue')
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.salvaUtility();
        });
    }

    goTo(link: string) {
      // this.salvaClienteTemp();
      this.router.navigate([link]);
    }

    refreshPage() {
      this.router.navigateByUrl('blank', { skipLocationChange: true }).then(() => {
        setTimeout(() => this.router.navigate(['utility_servizio']), 200);
    });
    }
}

