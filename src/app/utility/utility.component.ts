import { UtilityService } from 'src/app/_services/utility.service';
import { Router } from '@angular/router';
import { AppComponent } from './../app.component';
import { AuthenticationService } from '../_services/authentication.service';
import { Component, OnInit} from '@angular/core';


@Component({
  selector: 'app-utility',
  templateUrl: './utility.component.html',
  styleUrls: ['./utility.component.scss']
})
export class UtilityComponent implements OnInit {
  constructor(
    private utilityService: UtilityService,
    private router: Router,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {}

    public ngOnInit(): void {
      this.authenticationService.tokenRefresh();
      // inizializzo la variabile utente in localStorage
      if (localStorage.getItem('utente') !== null) {
        localStorage.removeItem('utente');
      }
      setTimeout(() => this.appComponent.setDiv(false , false), 0);
      setTimeout(() => this.appComponent.mostraGoogle(false), 0);
      setTimeout(() => this.utilityService.getCalendarColor(), 0);
      setTimeout(() => this.utilityService.getCentrale(), 0);
      setTimeout(() => this.utilityService.getPiano(), 0);
      setTimeout(() => this.utilityService.getProfessione(), 0);
      setTimeout(() => this.utilityService.getTipoCliente(), 0);
      setTimeout(() => this.utilityService.getTipoIntervento(), 0);
      setTimeout(() => this.utilityService.getDisdetta(), 0);
      setTimeout(() => this.utilityService.getTipoLocale(), 0);
      setTimeout(() => this.utilityService.getIva(), 0);
      setTimeout(() => this.utilityService.getStatoCivile(), 0);
      setTimeout(() => this.utilityService.getServizio(), 0);
    }

    goTo(link: string) {
      // this.salvaClienteTemp();
      this.router.navigate([link]);
    }
}
