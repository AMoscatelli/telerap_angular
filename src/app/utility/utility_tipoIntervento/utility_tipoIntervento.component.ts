import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilityService } from 'src/app/_services/utility.service';
import { AppComponent } from '../../app.component';
import { AuthenticationService } from '../../_services/authentication.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Form } from '@angular/forms';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-utilitytipointervento',
  templateUrl: './utility_tipoIntervento.component.html',
  styleUrls: ['./utility_tipoIntervento.component.scss']
})
export class UtilityTipoInterventoComponent implements AfterViewInit {
  @ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
  headElements = ['ID', 'Codice Intervento', 'Descrizione Intervento'];
  save: boolean; edit: boolean;
  elementTemp: Array<{id: number, codiceIntervento: string, descrizioneIntervento: string}> = [];
  elementTipoIntervento = [];
  visibleItems = 7;
  searchText = '';
  highlightedRow: any = null;
  tipoInterventoForm: FormGroup;
  tipoInterventoBE: Array<any>;
  public url = environment.apiEndpoint;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };


  constructor(
    private router: Router,
    private http: HttpClient,
    private utilityService: UtilityService,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {}

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
      this.save = true;
      this.edit = false;
      this.authenticationService.tokenRefresh();
      // inizializzo la variabile utente in localStorage
      if (localStorage.getItem('utente') !== null) {
        localStorage.removeItem('utente');
      }
      setTimeout(() => this.appComponent.setDiv(false , false), 0);
      // inizializzo la FormGroup
      this.tipoInterventoForm = new FormGroup({
        idIntervento: new FormControl(),
        codiceIntervento: new FormControl(),
        descrizioneIntervento: new FormControl(),
        model: new FormControl(),
      });

      this.tipoInterventoForm.patchValue({
        model: 'tipoIntervento'
      });
      // --------
      // richiamo la funzione che mi restituisce i risultati BE di centrale
      setTimeout(() => this.mdbTableEditor.dataArray = this.elementTipoIntervento, 0);
    }

    ngAfterViewInit() {
      if (JSON.parse(localStorage.getItem('tipoIntervento')) !== null) {
        this.tipoInterventoBE = JSON.parse(localStorage.getItem('tipoIntervento'));
        let index;
        index = 1;
        this.tipoInterventoBE.forEach(element => {
          // tslint:disable-next-line: max-line-length
          this.elementTipoIntervento.push({idValue: element._id, id: index, codiceIntervento: element.codiceIntervento, descrizioneIntervento: element.descrizioneIntervento});
          index++;
          console.log(this.elementTipoIntervento);
        });
        setTimeout(() => this.mdbTableEditor.dataArray = this.elementTipoIntervento, 0);
      }
    }

    setValue(item) {
      localStorage.setItem('idValue', item.idValue);
      if (item === null) {
        this.save = true;
        this.edit = false;
        this.tipoInterventoForm.patchValue({
          codiceIntervento: '',
          descrizioneIntervento: '',
        });
      } else {
        this.save = false;
        this.edit = true;
        this.tipoInterventoForm.patchValue({
          codiceIntervento: item.codiceIntervento,
          descrizioneIntervento: item.descrizioneIntervento,
        });
      }
    }

    salvaUtility() {
      this.http.post(this.url + '/utility/create', {
        utility: this.tipoInterventoForm.value,
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.utilityService.getTipoIntervento();
          setTimeout(() => this.refreshPage(), 500);
        });
    }

    deleteUtility() {
      this.http.post(this.url + '/utility/delete', {
        model: this.tipoInterventoForm.get('model').value,
        id: localStorage.getItem('idValue')
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.salvaUtility();
        });
    }

    goTo(link: string) {
      // this.salvaClienteTemp();
      this.router.navigate([link]);
    }

    refreshPage() {
      this.router.navigateByUrl('blank', { skipLocationChange: true }).then(() => {
        setTimeout(() => this.router.navigate(['utility_tipoIntervento']), 200);
    });
    }
}

