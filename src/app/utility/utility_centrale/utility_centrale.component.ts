import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilityService } from 'src/app/_services/utility.service';
import { AppComponent } from '../../app.component';
import { AuthenticationService } from '../../_services/authentication.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Form } from '@angular/forms';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-utilitycentrale',
  templateUrl: './utility_centrale.component.html',
  styleUrls: ['./utility_centrale.component.scss']
})
export class UtilityCentraleComponent implements AfterViewInit {
  @ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
  headElements = ['Codice', 'Fornitore', 'Centrale'];
  save: boolean; edit: boolean;
  elementCentrale = [];
  visibleItems = 7;
  searchText = '';
  highlightedRow: any = null;
  centraleForm: FormGroup;
  centraleBE: Array<any>;
  public url = environment.apiEndpoint;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };


  constructor(
    private router: Router,
    private http: HttpClient,
    private utilityService: UtilityService,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {}

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
      this.save = true;
      this.edit = false;
      this.authenticationService.tokenRefresh();
      // inizializzo la variabile utente in localStorage
      if (localStorage.getItem('utente') !== null) {
        localStorage.removeItem('utente');
      }
      setTimeout(() => this.appComponent.setDiv(false , false), 0);
      // inizializzo la FormGroup
      this.centraleForm = new FormGroup({
        codiceCentrale: new FormControl(),
        fornitoreCentrale: new FormControl(),
        centrale: new FormControl(),
        model: new FormControl(),
      });

      this.centraleForm.patchValue({
        model: 'centrale'
      });
      // --------
      // richiamo la funzione che mi restituisce i risultati BE di centrale
      setTimeout(() => this.mdbTableEditor.dataArray = this.elementCentrale, 0);
    }

    ngAfterViewInit() {
      if (JSON.parse(localStorage.getItem('centrale')) !== null) {
        this.centraleBE = JSON.parse(localStorage.getItem('centrale'));
        console.log(this.centraleBE);
        this.centraleBE.forEach(element => {
          this.elementCentrale.push(element);
        });
        setTimeout(() => this.mdbTableEditor.dataArray = this.elementCentrale, 0);
      }
    }

    setValue(item) {
      localStorage.setItem('idValue', item._id);
      if (item === null) {
        this.save = true;
        this.edit = false;
        this.centraleForm.patchValue({
          codiceCentrale: '',
          fornitoreCentrale: '',
          centrale: ''
        });
      } else {
        this.save = false;
        this.edit = true;
        this.centraleForm.patchValue({
          codiceCentrale: item.codiceCentrale,
          fornitoreCentrale: item.fornitoreCentrale,
          centrale: item.centrale
        });
      }
    }

    salvaUtility() {
      this.http.post(this.url + '/utility/create', {
        utility: this.centraleForm.value,
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.utilityService.getCentrale();
          setTimeout(() => this.refreshPage(), 500);
        });
    }

    deleteUtility() {
      this.http.post(this.url + '/utility/delete', {
        model: this.centraleForm.get('model').value,
        id: localStorage.getItem('idValue')
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.salvaUtility();
        });
    }

    goTo(link: string) {
      // this.salvaClienteTemp();
      this.router.navigate([link]);
    }

    refreshPage() {
      this.router.navigateByUrl('blank', { skipLocationChange: true }).then(() => {
        setTimeout(() => this.router.navigate(['utility_centrale']), 200);
    });
  }
}
