import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilityService } from 'src/app/_services/utility.service';
import { AppComponent } from '../../app.component';
import { AuthenticationService } from '../../_services/authentication.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Form } from '@angular/forms';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-utilitydisdetta',
  templateUrl: './utility_disdetta.component.html',
  styleUrls: ['./utility_disdetta.component.scss']
})
export class UtilityDisdettaComponent implements AfterViewInit {
  @ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
  headElements = ['ID', 'Codice Disdetta', 'Descrizione Disdetta'];
  save: boolean; edit: boolean;
  elementTemp: Array<{id: number, codiceDisdetta: string, descrizioneDisdetta: string}> = [];
  elementDisdetta = [];
  visibleItems = 7;
  searchText = '';
  highlightedRow: any = null;
  DisdettaForm: FormGroup;
  tipoDisdettaBE: Array<any>;
  public url = environment.apiEndpoint;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };


  constructor(
    private router: Router,
    private http: HttpClient,
    private utilityService: UtilityService,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {}

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
      this.save = true;
      this.edit = false;
      this.authenticationService.tokenRefresh();
      // inizializzo la variabile utente in localStorage
      if (localStorage.getItem('utente') !== null) {
        localStorage.removeItem('utente');
      }
      setTimeout(() => this.appComponent.setDiv(false , false), 0);
      // inizializzo la FormGroup
      this.DisdettaForm = new FormGroup({
        idDisdetta: new FormControl(),
        codiceDisdetta: new FormControl(),
        descrizioneDisdetta: new FormControl(),
        model: new FormControl(),
      });

      this.DisdettaForm.patchValue({
        model: 'disdetta'
      });
      // --------
      // richiamo la funzione che mi restituisce i risultati BE di centrale
      setTimeout(() => this.mdbTableEditor.dataArray = this.elementDisdetta, 0);
    }

    ngAfterViewInit() {
      if (JSON.parse(localStorage.getItem('disdetta')) !== null) {
        this.tipoDisdettaBE = JSON.parse(localStorage.getItem('disdetta'));
        let index;
        index = 1;
        this.tipoDisdettaBE.forEach(element => {
          // tslint:disable-next-line: max-line-length
          this.elementDisdetta.push({idValue: element._id, id: index, codiceDisdetta: element.codiceDisdetta, descrizioneDisdetta: element.descrizioneDisdetta});
          index++;
          console.log(this.elementDisdetta);
        });
        setTimeout(() => this.mdbTableEditor.dataArray = this.elementDisdetta, 0);
      }
    }

    setValue(item) {
      localStorage.setItem('idValue', item.idValue);
      if (item === null) {
        this.save = true;
        this.edit = false;
        this.DisdettaForm.patchValue({
          codiceDisdetta: '',
          descrizioneDisdetta: '',
        });
      } else {
        this.save = false;
        this.edit = true;
        this.DisdettaForm.patchValue({
          codiceDisdetta: item.codiceDisdetta,
          descrizioneDisdetta: item.descrizioneDisdetta,
        });
      }
    }

    salvaUtility() {
      this.http.post(this.url + '/utility/create', {
        utility: this.DisdettaForm.value,
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.utilityService.getDisdetta();
          setTimeout(() => this.refreshPage(), 500);
        });
    }

    deleteUtility() {
      this.http.post(this.url + '/utility/delete', {
        model: this.DisdettaForm.get('model').value,
        id: localStorage.getItem('idValue')
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.salvaUtility();
        });
    }

    goTo(link: string) {
      // this.salvaClienteTemp();
      this.router.navigate([link]);
    }

    refreshPage() {
      this.router.navigateByUrl('blank', { skipLocationChange: true }).then(() => {
        setTimeout(() => this.router.navigate(['utility_disdetta']), 200);
    });
    }
}

