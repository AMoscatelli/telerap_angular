import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilityService } from 'src/app/_services/utility.service';
import { AppComponent } from '../../app.component';
import { AuthenticationService } from '../../_services/authentication.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Form } from '@angular/forms';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-utilitytipolocale',
  templateUrl: './utility_tipoLocale.component.html',
  styleUrls: ['./utility_tipoLocale.component.scss']
})
export class UtilityTipoLocaleComponent implements AfterViewInit {
  @ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
  headElements = ['ID', 'Tipo Locale'];
  save: boolean; edit: boolean;
  elementTemp: Array<{id: number, tipoLocale: string}> = [];
  elementTipoLocale = [];
  visibleItems = 7;
  searchText = '';
  highlightedRow: any = null;
  tipoLocaleForm: FormGroup;
  tipoLocaleBE: Array<any>;
  public url = environment.apiEndpoint;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };


  constructor(
    private router: Router,
    private http: HttpClient,
    private utilityService: UtilityService,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {}

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
      this.save = true;
      this.edit = false;
      this.authenticationService.tokenRefresh();
      // inizializzo la variabile utente in localStorage
      if (localStorage.getItem('utente') !== null) {
        localStorage.removeItem('utente');
      }
      setTimeout(() => this.appComponent.setDiv(false , false), 0);
      // inizializzo la FormGroup
      this.tipoLocaleForm = new FormGroup({
        idTipoLocale: new FormControl(),
        tipoLocale: new FormControl(),
        model: new FormControl(),
      });

      this.tipoLocaleForm.patchValue({
        model: 'tipoLocale'
      });
      // --------
      // richiamo la funzione che mi restituisce i risultati BE di centrale
      setTimeout(() => this.mdbTableEditor.dataArray = this.elementTipoLocale, 0);
    }

    ngAfterViewInit() {
      if (JSON.parse(localStorage.getItem('tipoLocale')) !== null) {
        this.tipoLocaleBE = JSON.parse(localStorage.getItem('tipoLocale'));
        let index;
        index = 1;
        this.tipoLocaleBE.forEach(element => {
          this.elementTipoLocale.push({idValue: element._id, id: index, tipoLocale: element.tipoLocale});
          index++;
          console.log(this.elementTipoLocale);
        });
        setTimeout(() => this.mdbTableEditor.dataArray = this.elementTipoLocale, 0);
      }
    }

    setValue(item) {
      localStorage.setItem('idValue', item.idValue);
      if (item === null) {
        this.save = true;
        this.edit = false;
        this.tipoLocaleForm.patchValue({
          tipoLocale: '',
        });
      } else {
        this.save = false;
        this.edit = true;
        this.tipoLocaleForm.patchValue({
          tipoLocale: item.tipoLocale
        });
      }
    }

    salvaUtility() {
      this.http.post(this.url + '/utility/create', {
        utility: this.tipoLocaleForm.value,
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.utilityService.getTipoLocale();
          setTimeout(() => this.refreshPage(), 500);
        });
    }

    deleteUtility() {
      this.http.post(this.url + '/utility/delete', {
        model: this.tipoLocaleForm.get('model').value,
        id: localStorage.getItem('idValue')
        }, this.httpOptions)
        .subscribe(response => {
          console.log(JSON.parse(JSON.stringify(response)));
          this.salvaUtility();
        });
    }

    goTo(link: string) {
      // this.salvaClienteTemp();
      this.router.navigate([link]);
    }

    refreshPage() {
      this.router.navigateByUrl('blank', { skipLocationChange: true }).then(() => {
        setTimeout(() => this.router.navigate(['utility_tipoLocale']), 200);
    });
    }
}

