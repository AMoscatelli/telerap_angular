import { UtilityService } from 'src/app/_services/utility.service';
import { Router } from '@angular/router';
import { AppComponent } from '../../app.component';
import { Component, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef, OnDestroy} from '@angular/core';
import { AuthenticationService } from '../../_services';
import { HttpClient } from '@angular/common/http';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { Porta } from 'src/app/_models/porta.model';
import { Cliente } from 'src/app/_models/client.model';
import { environment } from 'src/environments/environment';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl } from '@angular/forms';
import { TabHeadingDirective } from 'ng-uikit-pro-standard';

@Component({
  selector: 'app-clientiporte',
  templateUrl: './clientiPorte.component.html',
  styleUrls: ['./clientiPorte.component.scss']
})
export class ClientiPorteComponent implements AfterViewInit {

  titolo: string;
  presentatoDa: any;
  item1: any;
  private porta: Porta;
  edit; save; delete: boolean;
  @ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
  headElements = ['Data', 'Fornitore', 'Ubicazione', 'Modello'];
  elementPorte = [];
  visibleItems = 7;
  highlightedRow: any = null;
  url: string;
  searchText = '';
  fornitore: string;
  ubicazione: string;
  modelloPorta: string;
  battenti: string;
  antispiffero: string;
  antirapina: string;
  occhiello: string;
  bloccoAste: string;
  manganese: string;
  nota: string;
  data: Date;
  utente: Cliente;
  utenteTemp; utenteEdit: Cliente;
  checked = false;
  fornitoreSelect: Array<any>;
  ubicazioneSelect: Array<any>;
  modelloSelect: Array<any>;
  battentiSelect: Array<any>;
  antispifferoSelect: Array<any>;
  antirapinaSelect: Array<any>;
  occhielloSelect: Array<any>;
  manganeseSelect: Array<any>;
  clientiPorte: FormGroup;

  constructor(
    private utilityService: UtilityService,
    private router: Router,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService,
    private http: HttpClient,
    private datePipe: DatePipe) {
    }

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
      this.authenticationService.tokenRefresh();
      this.edit = false;
      this.save = true;
      this.delete = false;
      this.url = environment.apiEndpoint;
      // this.initSelect();
      this.appComponent.showComponent = true;
      this.titolo = 'PORTE BLINDATE';
         // ----------
      this.clientiPorte = new FormGroup({
        data: new FormControl(),
        fornitore: new FormControl(),
        ubicazione: new FormControl(),
        modelloPorta: new FormControl(),
        battenti: new FormControl(),
        antispiffero: new FormControl(),
        antirapina: new FormControl(),
        occhiello: new FormControl(),
        bloccoAste: new FormControl(),
        manganese: new FormControl(),
        nota: new FormControl(),
      });
      this.utente = JSON.parse(localStorage.getItem('utente'));
      this.utenteTemp = JSON.parse(localStorage.getItem('utenteTemp'));
      this.utenteEdit = JSON.parse(localStorage.getItem('utenteEdit'));

      if (localStorage.getItem('portaSelected')) {
        let portaCached = new Porta();
        portaCached = JSON.parse(localStorage.getItem('portaSelected'));
        if (JSON.stringify(portaCached).length === 2) {

        } else {
          if (this.utente !== undefined && this.utente !== null ) {
            try {
              this.utente.porte.push(portaCached);
              localStorage.removeItem('portaSelected');
              localStorage.setItem('utente', JSON.stringify(this.utente));
              this.mdbTableEditor.dataArray = this.utente.porte;
              this.edit = true;
              this.delete = false;
              this.save = false;
            } catch (e) {
              this.utente.porte = new Array<Porta>();
              this.utente.porte.push(portaCached);
              localStorage.removeItem('portaSelected');
              localStorage.setItem('utente', JSON.stringify(this.utente));
              this.mdbTableEditor.dataArray = this.utente.porte;
              this.edit = true;
              this.delete = false;
              this.save = false;
            }
          } else if (this.utenteTemp !== undefined && this.utenteTemp !== null ){
            try {
              this.utenteTemp.porte.push(portaCached);
              localStorage.removeItem('portaSelected');
              localStorage.setItem('utenteTemp', JSON.stringify(this.utenteTemp));
              this.mdbTableEditor.dataArray = this.utenteTemp.porte;
              this.edit = false;
              this.delete = false;
              this.save = true;
            } catch (e) {
              this.utenteTemp.porte = new Array<Porta>();
              this.utenteTemp.porte.push(portaCached);
              localStorage.removeItem('portaSelected');
              localStorage.setItem('utenteTemp', JSON.stringify(this.utenteTemp));
              this.mdbTableEditor.dataArray = this.utenteTemp.porte;
              this.edit = false;
              this.delete = false;
              this.save = true;
            }
          } else {
              try {
                this.utenteEdit.porte.push(portaCached);
                localStorage.removeItem('portaSelected');
                localStorage.setItem('utenteEdit', JSON.stringify(this.utenteEdit));
                this.mdbTableEditor.dataArray = this.utenteEdit.porte;
                this.edit = false;
                this.delete = false;
                this.save = true;
              } catch (e) {
                this.utenteEdit.porte = new Array<Porta>();
                this.utenteEdit.porte.push(portaCached);
                localStorage.removeItem('portaSelected');
                localStorage.setItem('utenteEdit', JSON.stringify(this.utenteEdit));
                this.mdbTableEditor.dataArray = this.utenteEdit.porte;
                this.edit = false;
                this.delete = false;
                this.save = true;
              }
            }
          }
      } else {
        if (this.utente !== undefined && this.utente !== null ) {
          this.edit = true;
          this.delete = false;
          this.save = false;
          this.mdbTableEditor.dataArray = this.utente.porte;
        } else if (this.utenteTemp.porte !== undefined && this.utenteTemp.porte !== null) {
          this.mdbTableEditor.dataArray = this.utenteTemp.porte;
        }
      }
    }

    ngAfterViewInit() {

    }

      goTo(link: string) {
        // this.salvaClienteTemp();
        this.router.navigate([link]);
      }

    sendData(item: Porta, id) {

      localStorage.setItem('portaSelected', JSON.stringify(item));
      if (this.utente !== undefined && this.utente !== null ) {
        this.utente.porte.splice(id, 1);
        localStorage.setItem('utente', JSON.stringify(this.utente));
      } else {
        this.utenteTemp.porte.splice(id, 1);
        localStorage.setItem('utenteTemp', JSON.stringify(this.utenteTemp));
      }

      this.goTo('clientiPorteAdd');

    }

    editForm() {
      this.checked = false;
      this.edit = false;
      this.save = true;
      this.delete = false;
      this.utilityService.editCliente();
    }

    salvaClienteTemp() {
      let clienteTemp = new Cliente();
      if (localStorage.getItem('utenteTemp') !== null) {
        clienteTemp = JSON.parse(localStorage.getItem('utenteTemp'));
        clienteTemp.porte = new Array<Porta>();
        const porta = new Porta();
        porta.data = this.utilityService.StringToDate(this.clientiPorte.controls.data.value);
        porta.fornitore = this.clientiPorte.controls.fornitore.value;
        porta.ubicazione = this.clientiPorte.controls.ubicazione.value;
        porta.modello = this.clientiPorte.controls.modelloPorta.value;
        porta.battente = this.clientiPorte.controls.battenti.value;
        porta.antispiffero = this.clientiPorte.controls.antispiffero.value;
        porta.antirapina = this.clientiPorte.controls.antirapina.value;
        porta.occhiello = this.clientiPorte.controls.occhiello.value;
        porta.bloccoAste = this.clientiPorte.controls.bloccoAste.value;
        porta.lastraManganese = this.clientiPorte.controls.manganese.value;
        porta.nota = this.clientiPorte.controls.nota.value;
        clienteTemp.porte.push(porta);
        localStorage.setItem('utenteTemp', JSON.stringify(clienteTemp));
      } else {
        clienteTemp = this.clientiPorte.value;
        localStorage.setItem('utenteTemp', JSON.stringify(clienteTemp));
      }
      console.log(clienteTemp);

    }

    salva() {
      // this.salvaClienteTemp();
      this.utilityService.salvaNuovoCliente();
    }
}

