import { environment } from './../../../environments/environment.prod';
import { UtilityService } from 'src/app/_services/utility.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/_services';
import { Cliente } from 'src/app/_models/client.model';
import { AppComponent } from 'src/app/app.component';
import { CurrencyPipe } from '@angular/common';
import { Impianti } from 'src/app/_models/impianti.model';

@Component({
  selector: 'app-clienticisa',
  templateUrl: './clientiCISA.component.html',
  styleUrls: ['./clientiCISA.component.scss']
})
export class ClientiCISAComponent implements AfterViewInit {
  edit; save; delete: boolean;
  rimborsoOre: string;
  rimborsoKM: string;
  envRimbKM: string;
  envRimbOre: string;
  clientiCISA: FormGroup;
  titolo: string;
  private impiantoTemp: Impianti;
  pasti: number; orelavoro: number; dirittoFisso: number; totale: number;
  prezzoKM: number;
  prezzoOre: number;
  costoInterventoPerOra: string;
  constructor(
    private utilityService: UtilityService,
    private router: Router,
    private currency: CurrencyPipe,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {
    }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    const money = 3000;
    const converted = this.currency.transform(money, 'EUR', 'symbol', );
    console.log(converted);
    this.envRimbKM = this.utilityService.formatValuta(parseFloat(environment.rimborsoKM));
    this.envRimbOre = this.utilityService.formatValuta(parseFloat(environment.rimborsoOre));
    this.costoInterventoPerOra = this.utilityService.formatValuta(parseFloat(environment.costoInterventoPerOraCISA));


    this.authenticationService.tokenRefresh();
    // inizializzo i button per il salvataggio, la modifica e la cancellazione di un cliente
    this.save = true;
    this.edit = false;
    this.delete = false;
    // ----------
    this.clientiCISA = new FormGroup({
        oreLav: new FormControl(),
        oreLavoro: new FormControl(),
        totale: new FormControl(),
        pasti: new FormControl(null, {updateOn: 'blur'}),
        dirittoFisso: new FormControl(),
        totaleIva: new FormControl(),
        totaleForfIva: new FormControl(),
        noteCISA: new FormControl(),
        totaleForf: new FormControl(null, {updateOn: 'blur'}),
        rimborsoOre: new FormControl(),
        rimborsoKM: new FormControl(),

    });
    this.titolo = 'CISA';

    // imposto il valore del diritto Fisso
    this.dirittoFisso = environment.DirittoFissoChiamata;
    this.clientiCISA.patchValue ({
      dirittoFisso: this.dirittoFisso.toFixed(2)
    });

    // disabilito le input dei totali
    this.clientiCISA.controls.totale.disable();
    this.clientiCISA.controls.totaleIva.disable();
    this.clientiCISA.controls.totaleForfIva.disable();

    // funzione che verifica i cambiamenti delle ore lavorative
    this.clientiCISA.get('oreLav').valueChanges.subscribe(selectedValue => {
      this.dirittoFisso = parseFloat(this.clientiCISA.get('dirittoFisso').value);
      this.pasti = parseFloat(this.clientiCISA.get('pasti').value);
      this.orelavoro = 37 * parseFloat(selectedValue);
      // verifico che il numero non sia null o indefinito
      // tslint:disable-next-line: use-isnan
      if (this.pasti === null || this.pasti === undefined || isNaN(this.pasti)) {
        this.pasti = 0;
      }
      // verifico che il numero non sia null o indefinito
      // tslint:disable-next-line: use-isnan
      if (this.orelavoro === null || this.orelavoro === undefined || isNaN(this.orelavoro)) {
        this.orelavoro = 0;
      }
      let OrePercorso = parseFloat(this.clientiCISA.get('rimborsoOre').value);
      if (OrePercorso === null || OrePercorso === undefined || isNaN(OrePercorso)) {
        OrePercorso = 0;
      }
       // calcolo il totale
      this.prezzoOre = OrePercorso * parseFloat(environment.rimborsoOre);
      let kmPercorsi = parseFloat(this.clientiCISA.get('rimborsoKM').value);
      if (kmPercorsi === null || kmPercorsi === undefined || isNaN(kmPercorsi)) {
        kmPercorsi = 0;
      }
      this.prezzoKM = kmPercorsi * parseFloat(environment.rimborsoKM);
      // calcolo il totale
      this.totale = (this.pasti + this.orelavoro + this.dirittoFisso + this.prezzoKM + this.prezzoOre);
      this.clientiCISA.patchValue({
        oreLavoro: this.orelavoro.toFixed(2),
        totale: this.utilityService.formatValuta(this.totale),
        totaleIva: this.utilityService.formatValuta(this.totale + (this.totale * (environment.IVA / 100)))
      });
    });

    /*this.clientiCISA.get('oreLavoro').valueChanges.subscribe(selectedValue => {
      this.dirittoFisso = parseFloat(this.clientiCISA.get('dirittoFisso').value);
      this.pasti = parseFloat(this.clientiCISA.get('pasti').value);
      this.orelavoro = parseFloat(selectedValue);
      // verifico che il numero non sia null o indefinito
      // tslint:disable-next-line: use-isnan
      if (this.pasti === null || this.pasti === undefined || isNaN(this.pasti)) {
        this.pasti = 0;
      }
      // verifico che il numero non sia null o indefinito
      // tslint:disable-next-line: use-isnan
      if (this.orelavoro === null || this.orelavoro === undefined || isNaN(this.orelavoro)) {
        this.orelavoro = 0;
      }
      let OrePercorso = parseFloat(this.clientiCISA.get('rimborsoOre').value);
      if (OrePercorso === null || OrePercorso === undefined || isNaN(OrePercorso)) {
        OrePercorso = 0;
      }
       // calcolo il totale
      this.prezzoOre = OrePercorso * parseFloat(environment.rimborsoOre);
      let kmPercorsi = parseFloat(this.clientiCISA.get('rimborsoKM').value);
      if (kmPercorsi === null || kmPercorsi === undefined || isNaN(kmPercorsi)) {
        kmPercorsi = 0;
      }
      this.prezzoKM = kmPercorsi * parseFloat(environment.rimborsoKM);
      // calcolo il totale
      this.totale = (this.pasti + this.orelavoro + this.dirittoFisso + this.prezzoKM + this.prezzoOre);
      this.clientiCISA.patchValue({
        oreLavoro: this.orelavoro.toFixed(2),
        totale: this.utilityService.formatValuta(this.totale),
        totaleIva: this.utilityService.formatValuta(this.totale + (this.totale * (environment.IVA / 100)))
      });
    });*/
    // funzione che verifica i cambiamenti delle ore lavorative
    this.clientiCISA.get('rimborsoKM').valueChanges.subscribe(selectedValue => {
      this.dirittoFisso = parseFloat(this.clientiCISA.get('dirittoFisso').value);
      this.pasti = parseFloat(this.clientiCISA.get('pasti').value);
      this.orelavoro = 37 * parseFloat(this.clientiCISA.get('oreLav').value);

      // verifico che il numero non sia null o indefinito
      // tslint:disable-next-line: use-isnan
      if (this.pasti === null || this.pasti === undefined || isNaN(this.pasti)) {
        this.pasti = 0;
      }
      // verifico che il numero non sia null o indefinito
      // tslint:disable-next-line: use-isnan
      if (this.orelavoro === null || this.orelavoro === undefined || isNaN(this.orelavoro)) {
        this.orelavoro = 0;
      }
      let kmPercorsi = parseFloat(this.clientiCISA.get('rimborsoKM').value);
      if (kmPercorsi === null || kmPercorsi === undefined || isNaN(kmPercorsi)) {
        kmPercorsi = 0;
      }
      this.prezzoKM = kmPercorsi * parseFloat(environment.rimborsoKM);
      // calcolo il totale
      const totale = (this.pasti + this.orelavoro + this.dirittoFisso + this.prezzoKM + this.prezzoOre);
      this.clientiCISA.patchValue({
        // oreLavoro: this.orelavoro.toFixed(2),
        totale: this.utilityService.formatValuta(totale),
        totaleIva: this.utilityService.formatValuta(totale + (totale * (environment.IVA / 100)))
      });
    });

    // funzione che verifica i cambiamenti delle ore lavorative
    this.clientiCISA.get('rimborsoOre').valueChanges.subscribe(selectedValue => {
      this.dirittoFisso = parseFloat(this.clientiCISA.get('dirittoFisso').value);
      this.pasti = parseFloat(this.clientiCISA.get('pasti').value);
      this.orelavoro = 37 * parseFloat(this.clientiCISA.get('oreLav').value);
      // verifico che il numero non sia null o indefinito
      // tslint:disable-next-line: use-isnan
      if (this.pasti === null || this.pasti === undefined || isNaN(this.pasti)) {
        this.pasti = 0;
      }
      // verifico che il numero non sia null o indefinito
      // tslint:disable-next-line: use-isnan
      if (this.orelavoro === null || this.orelavoro === undefined || isNaN(this.orelavoro)) {
        this.orelavoro = 0;
      }
      let OrePercorso = parseFloat(this.clientiCISA.get('rimborsoOre').value);
      if (OrePercorso === null || OrePercorso === undefined || isNaN(OrePercorso)) {
        OrePercorso = 0;
      }
       // calcolo il totale
      this.prezzoOre = OrePercorso * parseFloat(environment.rimborsoOre);
      const totale = (this.pasti + this.orelavoro + this.dirittoFisso + this.prezzoKM + this.prezzoOre);

      this.clientiCISA.patchValue({
        // oreLavoro: this.orelavoro.toFixed(2),
        totale: this.utilityService.formatValuta(totale),
        totaleIva: this.utilityService.formatValuta(totale + (totale * (environment.IVA / 100)))
      });
    });

    // funzione che verifica l'inserimento dei pasti
    this.clientiCISA.get('pasti').valueChanges.subscribe(selectedValue => {
      this.dirittoFisso = parseFloat(this.clientiCISA.get('dirittoFisso').value);
      this.pasti = parseFloat(selectedValue);
      this.orelavoro = parseFloat(this.clientiCISA.get('oreLavoro').value);
      // verifico che il numero non sia null o indefinito
      // tslint:disable-next-line: use-isnan
      if (this.pasti === null || this.pasti === undefined || isNaN(this.pasti)) {
        this.pasti = 0;
      }
      // verifico che il numero non sia null o indefinito
      // tslint:disable-next-line: use-isnan
      if (this.orelavoro === null || this.orelavoro === undefined || isNaN(this.orelavoro)) {
        this.orelavoro = 0;
      }
      let OrePercorso = parseFloat(this.clientiCISA.get('rimborsoOre').value);
      if (OrePercorso === null || OrePercorso === undefined || isNaN(OrePercorso)) {
        OrePercorso = 0;
      }
       // calcolo il totale
      this.prezzoOre = OrePercorso * parseFloat(environment.rimborsoOre);
      let kmPercorsi = parseFloat(this.clientiCISA.get('rimborsoKM').value);
      if (kmPercorsi === null || kmPercorsi === undefined || isNaN(kmPercorsi)) {
        kmPercorsi = 0;
      }
      this.prezzoKM = kmPercorsi * parseFloat(environment.rimborsoKM);
      // calcolo il totale
      this.totale = (this.pasti + this.orelavoro + this.dirittoFisso + this.prezzoKM + this.prezzoOre);
      this.clientiCISA.patchValue({
        // tslint:disable-next-line: radix
        oreLavoro: this.orelavoro.toFixed(2),
        totale: this.utilityService.formatValuta(this.totale),
        totaleIva: this.utilityService.formatValuta(this.totale + (this.totale * (environment.IVA / 100)))
      });
    });

    this.clientiCISA.get('totaleForf').valueChanges.subscribe(selectedValue => {
      this.clientiCISA.patchValue({
        totaleForfIva: this.utilityService.formatValuta(parseFloat(selectedValue) + selectedValue * (environment.IVA / 100))
      });
    });
  }

  ngAfterViewInit() {
    this.impiantoTemp = JSON.parse(localStorage.getItem('impiantoSelected'));
    if (this.impiantoTemp) {
      setTimeout(() => this.setValues(), 0);
    }
  }

  goTo(link: string, salva = false) {
    if (salva) {
    this.salvaClienteTemp();
    }
    this.router.navigate([link]);
  }

  setValues() {
    this.edit = false;
    this.save = true;
    this.delete = false;
    // console.log(this.utenteTemp.notaCISA);
    this.clientiCISA.patchValue({
      oreLav: this.impiantoTemp.oreLavCISA,
      oreLavoro: this.impiantoTemp.oreLavoroCISA,
      totale: this.impiantoTemp.totaleCISA,
      pasti: this.impiantoTemp.pastiCISA,
      // dirittoFisso: this.utenteTemp.dirittoFissoCISA,
      totaleIva: this.impiantoTemp.totaleIvaCISA,
      totaleForfIva: this.impiantoTemp.totaleForfIvaCISA,
      noteCISA: this.impiantoTemp.noteCISA,
      totaleForf: this.impiantoTemp.totaleForfCISA,
      rimborsoKM: this.impiantoTemp.KM_AR,
      rimborsoOre: this.impiantoTemp.tempoAR,
    });
    // this.clientiCISA.disable();
  }

  salvaClienteTemp() {
    // let clienteTemp = new Cliente();
    if (localStorage.getItem('impiantoSelected') !== null) {
      // this.impiantoTemp = JSON.parse(localStorage.getItem('impiantoTemp'));
      this.impiantoTemp.notaCISA = this.clientiCISA.controls.noteCISA.value;
      this.impiantoTemp.oreLavCISA = this.clientiCISA.controls.oreLav.value;
      this.impiantoTemp.oreLavoroCISA = this.clientiCISA.controls.oreLavoro.value;
      this.impiantoTemp.totaleCISA = this.clientiCISA.controls.totale.value;
      this.impiantoTemp.pastiCISA = this.clientiCISA.controls.pasti.value;
      // clienteTemp.dirittoFissoCISA = this.clientiCISA.controls.dirittoFisso.value;
      this.impiantoTemp.totaleIvaCISA = this.clientiCISA.controls.totaleIva.value;
      this.impiantoTemp.totaleForfIvaCISA = this.clientiCISA.controls.totaleForfIva.value;
      this.impiantoTemp.totaleForfCISA = this.clientiCISA.controls.totaleForf.value;
      localStorage.setItem('impiantoSelected', JSON.stringify(this.impiantoTemp));
    } else {
      const impiantoTempNew = new Impianti();
      impiantoTempNew.notaCISA = this.clientiCISA.controls.noteCISA.value;
      impiantoTempNew.oreLavCISA = this.clientiCISA.controls.oreLav.value;
      impiantoTempNew.oreLavoroCISA = this.clientiCISA.controls.oreLavoro.value;
      impiantoTempNew.totaleCISA = this.clientiCISA.controls.totale.value;
      impiantoTempNew.pastiCISA = this.clientiCISA.controls.pasti.value;
      // clienteTemp.dirittoFissoCISA = this.clientiCISA.controls.dirittoFisso.value;
      impiantoTempNew.totaleIvaCISA = this.clientiCISA.controls.totaleIva.value;
      impiantoTempNew.totaleForfIvaCISA = this.clientiCISA.controls.totaleForfIva.value;
      impiantoTempNew.totaleForfCISA = this.clientiCISA.controls.totaleForf.value;
      localStorage.setItem('impiantoSelected', JSON.stringify(this.impiantoTemp));
    }
    // console.log(clienteTemp);
  }

}
