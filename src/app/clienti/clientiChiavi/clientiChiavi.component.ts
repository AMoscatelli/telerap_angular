import { Router } from '@angular/router';
import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import {MdbTableEditorDirective} from 'mdb-table-editor';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { Chiavi } from 'src/app/_models/chiavi.model';
import { Impianti } from 'src/app/_models/impianti.model';
import { DeprecatedDatePipe, DatePipe } from '@angular/common';
import { FormGroup, FormControl } from '@angular/forms';
import { Cliente } from 'src/app/_models/client.model';

@Component({
  selector: 'app-clientichiavi',
  templateUrl: './ClientiChiavi.component.html',
  styleUrls: ['./clientiChiavi.component.scss']
})
export class ClientiChiaviComponent implements AfterViewInit {
@ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
titolo: string;
private impianto: Impianti;
headElements = ['Owner', 'Consegnato A', 'Data', 'N° Key', 'Codice Tessera'];
visibleItems = 4;
edit; save; delete; editKey; deleteKey: boolean;
searchText = '';
ownerSelect: Array <any>;
data: Array<any>;
highlightedRow: any = null;
ownerModal: string;
consegnateAModal: string;
altroCodiceModal: string;
ultimaVerificaKeyDataModal: Date;
ultimaVerificaKeyDataString: string;
statoChiaviModal: string;
numeroChiaviModal: string;
tipoCustodiaModal: string;
dataConsegnaModal: Date;
dataConsegnaString: string;
telecomandoModal: string;
tesseraKeyModal: string;
altroModal: string;
noteModal: string;
noteConsegnaModal: string;
noteChiaviModal: string;
checked = true;
owner: string;
consegnateA: string;
altroCodice: string;
ultimaVerificaKeyData: Date;
statoChiavi: string;
numeroChiavi: string;
tipoCustodia: string;
dataConsegna: Date;
telecomando: string;
tesseraKey: string;
altro: string;
note: string;
noteConsegna: string;
noteChiavi: string;
clientiKey: FormGroup;
chiaviArray: Array<Chiavi>;
utenteTemp: Cliente;
  itemCache: any;
  idCahce: any;

constructor(
  private router: Router,
  private authenticationService: AuthenticationService,
  private datePipe: DatePipe
  ) {}
// tslint:disable-next-line: use-lifecycle-interface
ngOnInit() {
  this.authenticationService.tokenRefresh();
  this.titolo = 'Dettaglio chiavi';
  this.clientiKey = new FormGroup({
    owner: new FormControl(),
    consegnateA: new FormControl(),
    altroCodice: new FormControl(),
    ultimaVerificaKeyData: new FormControl(),
    statoChiavi: new FormControl(),
    numeroChiavi: new FormControl(),
    tipoCustodia: new FormControl(),
    dataConsegna: new FormControl(),
    telecomando: new FormControl(),
    tesseraKey: new FormControl(),
    altro: new FormControl(),
    noteConsegna: new FormControl(),
    noteChiavi: new FormControl(),
    note: new FormControl(),
  });
  this.clientiKey.disable();
  // alert('ciaooooooooooooooooooooooooooooo');
}

ngAfterViewInit() {

  this.data = [
    { value: 'CGS', label: 'CGS'},
    { value: 'SecurSafe2', label: 'SecurSafe' },
    { value: 'AC', label: 'AC' }
  ];
  this.chiaviArray = new Array<Chiavi>();
  setTimeout(() =>  this.ownerSelect = this.data, 0);
  try {
    // tslint:disable-next-line: max-line-length
    if (localStorage.getItem('impiantoSelected') !== undefined  && localStorage.getItem('impiantoSelected') !== 'undefined' && localStorage.getItem('impiantoSelected') !== 'null' && localStorage.getItem('impiantoSelected') !== null) {
          this.impianto = JSON.parse(localStorage.getItem('impiantoSelected'));
          const i = 0;
          if (this.impianto.chiavi !== null && this.impianto.chiavi !== undefined) {
              this.impianto.chiavi.forEach(() => {
              // tslint:disable-next-line: max-line-length
              // this.impianto.chiavi[i].dataConsegnaString = this.datePipe.transform(this.impianto.chiavi[i].dataConsegna, 'dd-MM-yyyy', 'IT');
              // tslint:disable-next-line: max-line-length
              // this.impianto.chiavi[i].ultimaVerificaKeyDataString = this.datePipe.transform(this.impianto.chiavi[i].ultimaVerificaKeyData, 'dd-MM-yyyy', 'IT');
              // i++;
              });
          } else {
            this.impianto.chiavi = new Array<Chiavi>();
          }
          this.save = true;
          setTimeout(() => this.mdbTableEditor.dataArray = this.impianto.chiavi, 0);
    } else {
      this.utenteTemp = JSON.parse(localStorage.getItem('utenteTemp'));
      if (this.utenteTemp.impianti[0].chiavi === undefined) {
        this.utenteTemp.impianti[0].chiavi = new Array<Chiavi>();
      } else {
        setTimeout(() => this.mdbTableEditor.dataArray = this.utenteTemp.impianti[0].chiavi, 0);
      }
    }
} catch (error) {
  console.log('No impianto selected');
}

  console.log(this.impianto);
  console.log('ImpSel - ' + localStorage.getItem('impiantoSelected'));
}
goTo(link: string) {
  localStorage.setItem('impiantoSelected', JSON.stringify(this.impianto));
  if (this.utenteTemp !== undefined) {
    localStorage.setItem('utenteTemp', JSON.stringify(this.utenteTemp));
  }
  this.router.navigate([link]);
}

sendData(item, id) {
  this.editKey = true;
  this.deleteKey = true;
  this.clientiKey.patchValue({
    owner: item.owner,
    consegnateA: item.consegnateA,
    altroCodice: item.altroCodice,
    ultimaVerificaKeyData: item.ultimaVerificaKeyDataString,
    statoChiavi: item.statoChiavi,
    numeroChiavi: item.numeroChiavi,
    tipoCustodia: item.tipoCustodia,
    dataConsegna: item.dataConsegnaString,
    telecomando: item.telecomando,
    tesseraKey: item.tesseraKey,
    altro: item.altro,
    note: item.note,
    noteConsegna: item.noteConsegna,
    noteChiavi: item.noteChiavi,
  });
  this.itemCache = item;
  this.idCahce = id;

}

editaKey() {

    this.ownerModal =  this.itemCache.owner;
    this.consegnateAModal = this.itemCache.consegnateA;
    this.altroCodiceModal = this.itemCache.altroCodice;
    this.ultimaVerificaKeyDataModal = this.itemCache.ultimaVerificaKeyDataString;
    this.statoChiaviModal = this.itemCache.statoChiavi;
    this.numeroChiaviModal = this.itemCache.numeroChiavi;
    this.tipoCustodiaModal = this.itemCache.tipoCustodia;
    this.dataConsegnaModal = this.itemCache.dataConsegnaString;
    this.telecomandoModal = this.itemCache.telecomando;
    this.tesseraKeyModal = this.itemCache.tesseraKey;
    this.altroModal = this.itemCache.altro;
    this.noteModal = this.itemCache.note;
    this.noteConsegnaModal = this.itemCache.noteConsegna;
    this.noteChiaviModal = this.itemCache.noteChiavi;
    this.idCahce = null;
}

ripristinaKey() {
  if (this.itemCache != null){
    //this.impianto.chiavi.push(this.itemCache);
    this.itemCache = null;
  }
}

onOpened() {
  if (this.itemCache !== null) {
    this.ownerModal =  '';
    this.consegnateAModal = '';
    this.altroCodiceModal = '';
    this.ultimaVerificaKeyDataModal = new Date();
    this.statoChiaviModal = '';
    this.numeroChiaviModal = '';
    this.tipoCustodiaModal = '';
    this.dataConsegnaModal = new Date();
    this.telecomandoModal = '';
    this.tesseraKeyModal = '';
    this.altroModal = '';
    this.noteModal = '';
    this.noteConsegnaModal = '';
    this.noteChiaviModal = '';
  }
}
askRemoveKey() {
  const esito = confirm('Sei sicuro di voler rimuovere la Chiave selezionata?');
  if (esito) {
    this.impianto.chiavi.splice(this.idCahce, 1);
    this.clientiKey.patchValue({
      owner: '',
      consegnateA: '',
      altroCodice: '',
      ultimaVerificaKeyData: '',
      statoChiavi: '',
      numeroChiavi: '',
      tipoCustodia: '',
      dataConsegna: '',
      telecomando: '',
      tesseraKey: '',
      altro: '',
      note: '',
      noteConsegna: '',
      noteChiavi: '',
    });
  } else {

  }
  // this.impianto.chiavi.splice(this.idCahce, 1);
}

salva() {
    this.impianto.chiavi.splice(this.idCahce, 1);
    const key = new Chiavi();
    key.owner = this.ownerModal;
    key.consegnateA = this.consegnateAModal;
    key.altroCodice = this.altroCodiceModal;
    key.ultimaVerificaKeyData = this.ultimaVerificaKeyDataModal;
    key.statoChiavi = this.statoChiaviModal;
    key.numeroChiavi = this.numeroChiaviModal;
    key.tipoCustodia = this.tipoCustodiaModal;
    key.dataConsegna = this.dataConsegnaModal;
    key.telecomando = this.telecomandoModal;
    key.tesseraKey = this.tesseraKeyModal;
    key.altro = this.altroModal;
    key.note = this.noteModal;
    key.noteConsegna = this.noteConsegnaModal;
    key.noteChiavi = this.noteChiaviModal;
    console.log(this.impianto + 'è undefined?');
    if (this.impianto === undefined) {
      this.chiaviArray.push(key);
      this.utenteTemp.impianti[0].chiavi.push(key);
      setTimeout(() => this.mdbTableEditor.dataArray = this.utenteTemp.impianti[0].chiavi, 0);
    } else {
      this.impianto.chiavi.push(key);
      setTimeout(() => this.mdbTableEditor.dataArray = this.impianto.chiavi, 0);
    }
    console.log(this.impianto);
    this.clientiKey.patchValue({
      owner: key.owner,
      consegnateA: key.consegnateA,
      altroCodice: key.altroCodice,
      ultimaVerificaKeyData: key.ultimaVerificaKeyDataString,
      statoChiavi: key.statoChiavi,
      numeroChiavi: key.numeroChiavi,
      tipoCustodia: key.tipoCustodia,
      dataConsegna: key.dataConsegnaString,
      telecomando: key.telecomando,
      tesseraKey: key.tesseraKey,
      altro: key.altro,
      note: key.note,
      noteConsegna: key.noteConsegna,
      noteChiavi: key.noteChiavi,
    });
    this.itemCache = null;

}
}
