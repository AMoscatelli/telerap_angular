import { AppComponent } from './../../app.component';
import { Component, ViewChild, ElementRef, HostListener, AfterViewInit } from '@angular/core';
import { AuthenticationService } from '../../_services';
import { HttpClient } from '@angular/common/http';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { Cliente } from 'src/app/_models/client.model';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { UtilityService } from 'src/app/_services/utility.service';


@Component({
  selector: 'app-clientigrid',
  templateUrl: './clientiGrid.component.html',
  styleUrls: ['./clientiGrid.component.scss']
})
export class ClientiGridComponent implements AfterViewInit{
  @ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
  headElements = ['CodCliente', 'Nome Cognome'];
  visibleItems = 7;
  searchText = '';
  highlightedRow: any = null;
  public titoloCliente: string;
  loading: boolean;
  utente: Cliente;
  indirizzo: string; cap: string; citta: string; provincia: string; tavolaQuartiere: string;
  data: any;
  clienteSelected: Cliente;
  interval;


  constructor(
    private router: Router,
    private utilityService: UtilityService,
    private appComponent: AppComponent,
    private http: HttpClient,
    private authenticationService: AuthenticationService,) {}

  @HostListener('iconaListaClienti') click() {
    console.log('click fatto');
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    this.loading = true;
    this.appComponent.showComponent = true;
    this.authenticationService.tokenRefresh();
  }

  ngAfterViewInit(): void {
    this.getClient();
  }

  getClient() {
    return this.http.get(environment.apiEndpoint + '/cliente/getClienti', {})
      .subscribe(response => {
      this.data = JSON.parse(JSON.stringify(response));
      this.data.forEach((el: any, index: number) => {
        el.id = index + 1;
        localStorage.setItem(el.nomeCognome, el.id);
      });
      this.loading = false;
      this.mdbTableEditor.dataArray = this.data;
      });
    }

    sendData(item): void {
        this.clienteSelected = item;
        this.appComponent.showComponent = false;
        this.appComponent.loaderMDB = true;
        localStorage.removeItem('utente');
        setTimeout(() => this.sendUtente(item), 0);
    }

    deleteClient() {
      if (confirm('Vuoi eliminare il Cliente ' + this.clienteSelected.nomeCognome + ' Codice: ' + this.clienteSelected.codCliente + '?')) {
        this.utilityService.updateClienteVisibility('Visibility', this.clienteSelected._id, false);
      }
    }

    sendUtente(item): void {
      if ( item === null ) {
        this.mdbTableEditor.nextPage(true);
        this.mdbTableEditor.prevPage(true);
        localStorage.removeItem('utente');
        this.utilityService.localStorageCleanTempData();
        this.appComponent.loaderMDB = false;
        this.appComponent.showComponent = true;
        const cli = new Cliente();
        cli.visibility = true;
        localStorage.setItem('utenteTemp', JSON.stringify(cli));
        this.router.navigate(['clientiAnagrafica']);
      } else {
      this.titoloCliente = item.nomeCognome + ' - [' + item.codCliente + ']';
      this.http.get(environment.apiEndpoint + '/cliente/' + item._id, {})
      .subscribe(response => {
        const data = JSON.parse(JSON.stringify(response));
        this.utilityService.localStorageCleanTempData();
        localStorage.setItem('utente', JSON.stringify(data));
        this.appComponent.loaderMDB = false;
        this.appComponent.showComponent = true;
      });
    }
    }

    public sendUtente2() {
      // riempo i campi della parte retrostante
      this.utente = JSON.parse(localStorage.getItem('utente'));

      if (this.utente !== null) {
          // riempo i campi
        this.titoloCliente = this.utente.nomeCognome + ' - [' + this.utente.codCliente + ']';
        this.indirizzo = this.utente.indirizzo;
        this.cap = this.utente.CAP;
        this.citta = this.utente.citta;
        this.provincia = this.utente.provincia;
        this.tavolaQuartiere = this.utente.tavola + ' - ' + this.utente.quartiere;
      } else {
        this.utente = JSON.parse(localStorage.getItem('utenteTemp'));
        this.titoloCliente = this.utente.nomeCognome + ' - [' + this.utente.codCliente + ']';
        this.indirizzo = this.utente.indirizzo;
        this.cap = this.utente.CAP;
        this.citta = this.utente.citta;
        this.provincia = this.utente.provincia;
        this.tavolaQuartiere = this.utente.tavola + ' - ' + this.utente.quartiere;
      }
      this.startTimer();

    }

    startTimer() {
    /*  this.interval = setInterval(() => {
        this.sendUtente2();
      }, 2000 );*/
    }

    pauseTimer() {
      clearInterval(this.interval);
    }

}
