import { Router } from '@angular/router';
import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import {MdbTableEditorDirective} from 'mdb-table-editor';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { Cliente } from 'src/app/_models/client.model';
import { Interventi } from 'src/app/_models/interventi.model';
import { UtilityService } from 'src/app/_services/utility.service';
import { environment } from 'src/environments/environment';
import { Impianti } from 'src/app/_models/impianti.model';

@Component({
  selector: 'app-clientiaddintervento',
  templateUrl: './clientiAddIntervento.component.html',
  styleUrls: ['./clientiAddIntervento.component.scss']
})
export class ClientiAddInterventoComponent implements AfterViewInit {
utente: Cliente;
titolo: string;
clientiAddIntervento: FormGroup;
dropdownListTecnici = [];
dropdownSettings = {};
dropdownListCISA = [];
selectedItems = [];
tecniciCGS: { item_id: string; item_text: string; }[];
tecniciCISA: { item_id: string; item_text: string; }[];
intervento: Interventi;
edit: boolean;
save: boolean;
delete: boolean;
riepilogoAB: string;
riepilogoSMT: string;
riepilogoTLS: string;
notaIntervento: string;
riepilogoE: string;
riepilogoU: string;
riepilogoAL: string;
riepilogoF: string;
dataIntervento: Date;
oraInizioIntervento: Date;
oraFineIntervento: Date;
codCliente: string;
nomeUtente: string;
notaIntervento1: string;
tipoIntervento: string;
CisaFoglioIntervento: string;
costoIntervento: string;
pagatoSospeso: string;
costoKM: string;
siNo1: string;
tipologia1: string;
tipologia11: string;
siNo2: string;
tipologia2: string;
tipologia22: string;
siNo3: string;
tipologia3: string;
tipologia33: string;
siNo4: string;
tipologia4: string;
tipologia44: string;
siNo5: string;
tipologia5: string;
tipologia55: string;
oreAndy: Date;
oreFabio: Date;
oreMosca: Date;
oreGualty: Date;
calFabrizio: string;
oreFabrizio: Date;
$fabrizio: string;
calAndrea: string;
oreAndrea: Date;
calFabio1: string;
oreFabio1: Date;
calGualty1: string;
oreGualty1: Date;
calMosca1: string;
oreMosca1: Date;
cal: string;
ore: Date;
tipoConti1: string;
euro1: string;
tipoConti2: string;
euro2: string;
tipoConti3: string;
euro3: string;
tipoConti4: string;
euro4: string;
tipoConti5: string;
euro5: string;
selezioneTecnici = [];
selezioneCISA = [];
selectedItemsCGS: { item_id: string; item_text: string; }[];
selectedItemsCISA: { item_id: string; item_text: string; }[];




constructor(
  private router: Router,
  private authenticationService: AuthenticationService,
  private utilityService: UtilityService
  ) {}

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
  this.authenticationService.tokenRefresh();
  this.tecniciCISA = new Array<{ item_id: string; item_text: string; }>();
  this.tecniciCGS = new Array<{ item_id: string; item_text: string; }>();
  this.clientiAddIntervento = new FormGroup ({
    riepilogoAB: new FormControl(),
    riepilogoSMT: new FormControl(),
    riepilogoTLS: new FormControl(),
    notaIntervento: new FormControl(),
    riepilogoE: new FormControl(),
    riepilogoU: new FormControl(),
    riepilogoAL: new FormControl(),
    riepilogoF: new FormControl(),
    dataIntervento: new FormControl(),
    oraInizioIntervento: new FormControl(),
    oraFineIntervento: new FormControl(),
    codCliente: new FormControl(),
    nomeUtente: new FormControl(),
    notaIntervento1: new FormControl(),
    tipoIntervento: new FormControl(),
    CisaFoglioIntervento: new FormControl(),
    costoIntervento: new FormControl(),
    pagatoSospeso: new FormControl(),
    costoKM: new FormControl(),
    siNo1: new FormControl(),
    tipologia1: new FormControl(),
    tipologia11: new FormControl(),
    siNo2: new FormControl(),
    tipologia2: new FormControl(),
    tipologia22: new FormControl(),
    siNo3: new FormControl(),
    tipologia3: new FormControl(),
    tipologia33: new FormControl(),
    siNo4: new FormControl(),
    tipologia4: new FormControl(),
    tipologia44: new FormControl(),
    siNo5: new FormControl(),
    tipologia5: new FormControl(),
    tipologia55: new FormControl(),
    oreAndy: new FormControl(),
    oreFabio: new FormControl(),
    oreMosca: new FormControl(),
    oreGualty: new FormControl(),
    calFabrizio: new FormControl(),
    oreFabrizio: new FormControl(),
    $fabrizio: new FormControl(),
    calAndrea: new FormControl(),
    oreAndrea: new FormControl(),
    calFabio1: new FormControl(),
    oreFabio1: new FormControl(),
    calGualty1: new FormControl(),
    oreGualty1: new FormControl(),
    calMosca1: new FormControl(),
    oreMosca1: new FormControl(),
    cal: new FormControl(),
    ore: new FormControl(),
    tipoConti1: new FormControl(),
    euro1: new FormControl(),
    tipoConti2: new FormControl(),
    euro2: new FormControl(),
    tipoConti3: new FormControl(),
    euro3: new FormControl(),
    tipoConti4: new FormControl(),
    euro4: new FormControl(),
    tipoConti5: new FormControl(),
    euro5: new FormControl(),
    selezioneTecnici: new FormControl(),
    selezioneCISA: new FormControl()
  });

  this.dropdownListTecnici = [
    { item_id: 'Andrea', item_text: 'Andrea' },
    { item_id: 'Fabio', item_text: 'Fabio' },
    { item_id: 'Fabrizio', item_text: 'Fabrizio' },
    { item_id: 'Mosca', item_text: 'Mosca' },
    { item_id: 'Altro', item_text: 'Altro' }
  ];

  this.dropdownListCISA = [
    { item_id: 'Andrea', item_text: 'Andrea' },
    { item_id: 'Fabio', item_text: 'Fabio' },
    { item_id: 'Fabrizio', item_text: 'Fabrizio' },
    { item_id: 'Mosca', item_text: 'Mosca' },
    { item_id: 'Altro', item_text: 'Altro' }
  ];

  this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      enableCheckAll: false,
      clearSearchFilter: false,
    };

  this.titolo = 'Dettaglio intervento';

}

onItemSelectCGS(item: any) {
  this.tecniciCGS[this.tecniciCGS.length].item_id = item.item_id;
  this.tecniciCGS[this.tecniciCGS.length].item_text = item.item_text;
  console.log(this.tecniciCGS);
}

onSelectAllCGS(items: any) {
  this.tecniciCGS[this.tecniciCGS.length].item_id = items.item_id;
  this.tecniciCGS[this.tecniciCGS.length].item_text = items.item_text;
}
onItemSelectCISA(item: any) {
  this.tecniciCISA.push(item);
}

onSelectAllCISA(items: any) {
   this.tecniciCGS.push(items);
}

onItemDeselectedCGS(item: any) {
  alert('ciao');
  let i = 0;
  this.tecniciCGS.forEach(value => {
    if (value === item) {
      this.tecniciCGS.splice(i, 1);
    }
    i++;
  });
}

onItemDeselectedAllCGS(items: any) {
  this.tecniciCGS = new Array<{ item_id: string; item_text: string; }>();
}
onItemDeselectedCISA(item: any) {
  let i = 0;
  this.tecniciCISA.forEach(value => {
    if (value === item) {
      this.tecniciCISA.splice(i, 1);
    }
    i++;
  });
}

onItemDeselectedAllCISA(items: any) {
  this.tecniciCISA = new Array<{ item_id: string; item_text: string; }>();
}

goTo(link: string) {
  this.salvaClienteTemp();
  this.router.navigate([link]);
}

deleteIntervento() {

  localStorage.removeItem('interventoSelected');
  this.goTo('clientiIntervento');

}

ngAfterViewInit() {
  try {
    this.intervento = JSON.parse(localStorage.getItem('interventoSelected'));
    console.log('ImpSel - ' + localStorage.getItem('interventoSelected'));
    if (this.intervento !== null) {
      setTimeout(() => this.setValues(this.intervento, false), 0);
    }
  } catch (error) {
    console.log('No Impianto selected');
  }
}

setValues(int: Interventi, nuovo: boolean) {
  if (!nuovo) {
    this.edit = false;
    this.save = true;
    this.delete = true;
  } else {
    this.edit = true;
    this.save = false;
    this.delete = true;
  }
  // setto i valori delle input

  let utente = new Cliente();
  if (localStorage.getItem('utenteTemp')) {
    utente = JSON.parse(localStorage.getItem('utenteTemp'));
  } else {
    utente = JSON.parse(localStorage.getItem('utenteTemp'));
  }

  let impianto = new Impianti();
  impianto = JSON.parse(localStorage.getItem('impiantoSelected'));



  if (int !== undefined) {


    this.selectedItemsCGS = int.selezioneTecniciCGS[0];
    this.selectedItemsCISA = int.selezioneCISA[0];
    this.tecniciCGS = int.selezioneTecniciCGS[0];
    this.tecniciCISA = int.selezioneCISA[0];
    this.clientiAddIntervento.patchValue({

    riepilogoAB: int.riepilogoAB,
    riepilogoSMT: int.riepilogoSMT,
    riepilogoTLS: int.riepilogoTLS,
    notaIntervento : int.notaIntervento,
    riepilogoE: int.riepilogoE,
    riepilogoU: int.riepilogoU,
    riepilogoAL: int.riepilogoAL,
    riepilogoF: int.riepilogoF,
    dataIntervento: this.utilityService.fillInputDate(new Date(int.dataIntervento)),
    oraInizioIntervento: int.oraInizioIntervento,
    oraFineIntervento: int.oraFineIntervento,
    codCliente: int.codCliente,
    nomeUtente: int.nomeUtente,
    notaIntervento1: int.notaIntervento1,
    tipoIntervento: int.tipoIntervento,
    CisaFoglioIntervento: int.CisaFoglioIntervento,
    costoIntervento : int.costoIntervento,
    pagatoSospeso : int.pagatoSospeso,
    costoKM : int.costoKM,
    siNo1 : int.siNo1,
    tipologia1 : int.tipologia1,
    tipologia11 : int.tipologia11,
    siNo2 : int.siNo2,
    tipologia2 : int.tipologia2,
    tipologia22 : int.tipologia22,
    siNo3 : int.siNo3,
    tipologia3 : int.tipologia3,
    tipologia33 : int.tipologia33,
    siNo4 : int.siNo4,
    tipologia4 : int.tipologia4,
    tipologia44 : int.tipologia44,
    siNo5 : int.siNo5,
    tipologia5 : int.tipologia5,
    tipologia55 : int.tipologia55,
    oreAndy : int.oreAndy,
    oreFabio : int.oreFabio,
    oreMosca : int.oreMosca,
    oreGualty : int.oreGualty,
    calFabrizio : int.calFabrizio,
    oreFabrizio : int.oreFabrizio,
    $fabrizio : int.$fabrizio,
    calAndrea : int.calAndrea,
    oreAndrea : int.oreAndrea,
    calFabio1 : int.calFabio1,
    oreFabio1 : int.oreFabio1,
    calGualty1 : int.calGualty1,
    oreGualty1 : int.oreGualty1,
    calMosca1 : int.calMosca1,
    oreMosca1 : int.oreMosca1,
    cal : int.cal,
    ore : int.ore,
    tipoConti1 : int.tipoConti1,
    euro1 : int.euro1,
    tipoConti2 : int.tipoConti2,
    euro2 : int.euro2,
    tipoConti3 : int.tipoConti3,
    euro3 : int.euro3,
    tipoConti4 : int.tipoConti4,
    euro4 : int.euro4,
    tipoConti5 : int.tipoConti5,
    euro5 : int.euro5,
    });

    if (int.codCliente === undefined) {
      this.clientiAddIntervento.patchValue({
        codCliente: impianto.codiceImpianto,
        nomeUtente: utente.nomeCognome,
        costoKM: environment.costoKMDefault,
      });
    } else {
      if (int.codCliente.length === 0) {
        int.codCliente = impianto.codiceImpianto;
      }
      if (int.nomeUtente.length === 0) {
        int.nomeUtente = utente.nomeCognome;
      }
      if (int.costoKM.length === 0) {
        int.costoKM = environment.costoKMDefault;
      }
    }
  }
}

salvaClienteTemp() {
  let imp: Interventi;
  if (localStorage.getItem('interventoSelected')) {
    imp = JSON.parse(localStorage.getItem('interventoSelected'));
    imp.riepilogoAB = this.clientiAddIntervento.controls.riepilogoAB.value;
    imp.riepilogoSMT = this.clientiAddIntervento.controls.riepilogoSMT.value;
    imp.riepilogoTLS = this.clientiAddIntervento.controls.riepilogoTLS.value;
    imp.notaIntervento  = this.clientiAddIntervento.controls.notaIntervento.value;
    imp.riepilogoE = this.clientiAddIntervento.controls.riepilogoE.value;
    imp.riepilogoU = this.clientiAddIntervento.controls.riepilogoU.value;
    imp.riepilogoAL = this.clientiAddIntervento.controls.riepilogoAL.value;
    imp.riepilogoF = this.clientiAddIntervento.controls.riepilogoF.value;
    imp.dataIntervento = this.utilityService.StringToDate(this.clientiAddIntervento.controls.dataIntervento.value);
    imp.oraInizioIntervento = this.utilityService.StringToDate(this.clientiAddIntervento.controls.oraInizioIntervento.value);
    imp.oraFineIntervento = this.utilityService.StringToDate(this.clientiAddIntervento.controls.oraFineIntervento.value);
    imp.codCliente = this.clientiAddIntervento.controls.codCliente.value;
    imp.nomeUtente = this.clientiAddIntervento.controls.nomeUtente.value;
    imp.notaIntervento1 = this.clientiAddIntervento.controls.notaIntervento1.value;
    imp.tipoIntervento = this.clientiAddIntervento.controls.tipoIntervento.value;
    imp.CisaFoglioIntervento = this.clientiAddIntervento.controls.CisaFoglioIntervento.value;
    imp.costoIntervento  = this.clientiAddIntervento.controls.costoIntervento.value;
    imp.pagatoSospeso  = this.clientiAddIntervento.controls.pagatoSospeso.value;
    imp.costoKM  = this.clientiAddIntervento.controls.costoKM.value;
    imp.siNo1  = this.clientiAddIntervento.controls.siNo1.value;
    imp.tipologia1  = this.clientiAddIntervento.controls.tipologia1.value;
    imp.tipologia11  = this.clientiAddIntervento.controls.tipologia11.value;
    imp.siNo2  = this.clientiAddIntervento.controls.siNo2.value;
    imp.tipologia2  = this.clientiAddIntervento.controls.tipologia2.value;
    imp.tipologia22  = this.clientiAddIntervento.controls.tipologia22.value;
    imp.siNo3  = this.clientiAddIntervento.controls.siNo3.value;
    imp.tipologia3  = this.clientiAddIntervento.controls.tipologia3.value;
    imp.tipologia33  = this.clientiAddIntervento.controls.tipologia33.value;
    imp.siNo4  = this.clientiAddIntervento.controls.siNo4.value;
    imp.tipologia4  = this.clientiAddIntervento.controls.tipologia4.value;
    imp.tipologia44  = this.clientiAddIntervento.controls.tipologia44.value;
    imp.siNo5  = this.clientiAddIntervento.controls.siNo5.value;
    imp.tipologia5  = this.clientiAddIntervento.controls.tipologia5.value;
    imp.tipologia55  = this.clientiAddIntervento.controls.tipologia55.value;
    imp.oreAndy  = this.utilityService.StringToDate(this.clientiAddIntervento.controls.oreAndy.value);
    imp.oreFabio  = this.utilityService.StringToDate(this.clientiAddIntervento.controls.oreFabio.value);
    imp.oreMosca  = this.utilityService.StringToDate(this.clientiAddIntervento.controls.oreMosca.value);
    imp.oreGualty  = this.utilityService.StringToDate(this.clientiAddIntervento.controls.oreGualty.value);
    imp.calFabrizio  = this.clientiAddIntervento.controls.calFabrizio.value;
    imp.oreFabrizio  = this.utilityService.StringToDate(this.clientiAddIntervento.controls.oreFabrizio.value);
    imp.$fabrizio  = this.clientiAddIntervento.controls.$fabrizio.value;
    imp.calAndrea  = this.clientiAddIntervento.controls.calAndrea.value;
    imp.oreAndrea  = this.utilityService.StringToDate(this.clientiAddIntervento.controls.oreAndrea.value);
    imp.calFabio1  = this.clientiAddIntervento.controls.calFabio1.value;
    imp.oreFabio1  = this.utilityService.StringToDate(this.clientiAddIntervento.controls.oreFabio1.value);
    imp.calGualty1  = this.clientiAddIntervento.controls.calGualty1.value;
    imp.oreGualty1  = this.utilityService.StringToDate(this.clientiAddIntervento.controls.oreGualty1.value);
    imp.calMosca1  = this.clientiAddIntervento.controls.calMosca1.value;
    imp.oreMosca1  = this.utilityService.StringToDate(this.clientiAddIntervento.controls.oreMosca1.value);
    imp.cal  = this.clientiAddIntervento.controls.cal.value;
    imp.ore  = this.utilityService.StringToDate(this.clientiAddIntervento.controls.ore.value);
    imp.tipoConti1  = this.clientiAddIntervento.controls.tipoConti1.value;
    imp.euro1  = this.clientiAddIntervento.controls.euro1.value;
    imp.tipoConti2  = this.clientiAddIntervento.controls.tipoConti2.value;
    imp.euro2  = this.clientiAddIntervento.controls.euro2.value;
    imp.tipoConti3  = this.clientiAddIntervento.controls.tipoConti3.value;
    imp.euro3  = this.clientiAddIntervento.controls.euro3.value;
    imp.tipoConti4  = this.clientiAddIntervento.controls.tipoConti4.value;
    imp.euro4  = this.clientiAddIntervento.controls.euro4.value;
    imp.tipoConti5  = this.clientiAddIntervento.controls.tipoConti5.value;
    imp.euro5  = this.clientiAddIntervento.controls.euro5.value;
    imp.selezioneTecniciCGS.push(this.tecniciCGS);
    imp.selezioneCISA.push(this.tecniciCISA);
    // imp.selezioneCISA  = this.clientiAddIntervento.controls.selezioneCISA.value;

    localStorage.setItem('interventoSelected', JSON.stringify(imp));
  }
  }

}
