import { Router } from '@angular/router';
import { AppComponent } from './../app.component';
import { Component, AfterViewInit} from '@angular/core';
import { AuthenticationService, MessageService } from '../_services';
import { Cliente } from '../_models/client.model';
import { FormGroup, FormControl } from '@angular/forms';
import { UtilityService } from '../_services/utility.service';

@Component({
  selector: 'app-clienti',
  templateUrl: './clienti.component.html',
  styleUrls: ['./clienti.component.scss']
})

export class ClientiComponent implements AfterViewInit {
  clientiGenerale: FormGroup;
  titolo: string;
  presentatoDa; tipoCliente; sitoInternet; notaCiccolini; notaCliente: any;
  private utente: Cliente;
  private utenteTemp: Cliente;
  edit; save; delete: boolean;

  constructor(
    private utilityService: UtilityService,
    private router: Router,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {
    }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    // inizializzo tutte le chiamate alle varie tabelle
    this.utilityService.getTipoLocale();
    this.utilityService.getProfessione();
    this.utilityService.getStatoCivile();
    // inizializzo i button per il salvataggio, la modifica e la cancellazione di un cliente
    this.save = true;
    this.edit = false;
    this.delete = false;
     // ----------
    this.clientiGenerale = new FormGroup({
      presentatoDa: new FormControl(),
      tipoCliente: new FormControl(),
      sitoInternet: new FormControl(),
      notaCiccolini: new FormControl(),
      notaCliente: new FormControl()
    });
    this.titolo = 'ALTRE INFORMAZIONI';
    this.authenticationService.tokenRefresh();
    setTimeout(() => this.appComponent.setDiv(true, true), 0);
    setTimeout(() => this.appComponent.mostraGoogle(false), 0);
  }

  ngAfterViewInit() {
    this.utente = JSON.parse(localStorage.getItem('utente'));
    this.utenteTemp = JSON.parse(localStorage.getItem('utenteTemp'));
    if (this.utente !== null) {
      if (this.utente.codCliente !== null) {
          setTimeout(() => this.setValues(this.utente, false), 0);
      } else {
        console.log('utente non inviato');
      }
    } else if (this.utenteTemp !== null) {
      if (this.utenteTemp.codCliente !== null) {
          setTimeout(() => this.setValues(this.utenteTemp, true), 0);
      } else {
        console.log('utente non inviato');
      }
    }
  }

  goTo(link: string) {
    this.salvaClienteTemp();
    this.router.navigate([link]);
  }

  setValues(cli: Cliente, nuovo: boolean) {
    if (!nuovo) {
      this.edit = true;
      this.save = false;
      this.delete = false;
      this.clientiGenerale.disable();
    }
    this.clientiGenerale.patchValue({
      presentatoDa: cli.presentatoDa,
      tipoCliente: cli.tipoCliente,
      sitoInternet: cli.sitoInternet,
      notaCiccolini: cli.notaCiccolini,
      notaCliente: cli.notaCliente,
    });


  }

  enableForm() {
    this.clientiGenerale.enable();
  }

  salvaClienteTemp() {

    let clienteTemp = new Cliente();
    if (localStorage.getItem('utenteTemp') !== null) {
      clienteTemp = JSON.parse(localStorage.getItem('utenteTemp'));
      clienteTemp.presentatoDa = this.clientiGenerale.controls.presentatoDa.value;
      clienteTemp.tipoCliente = this.clientiGenerale.controls.tipoCliente.value;
      clienteTemp.sitoInternet = this.clientiGenerale.controls.sitoInternet.value;
      clienteTemp.notaCiccolini = this.clientiGenerale.controls.notaCiccolini.value;
      clienteTemp.notaCliente = this.clientiGenerale.controls.notaCliente.value;
      localStorage.setItem('utenteTemp', JSON.stringify(clienteTemp));
    } else {
      clienteTemp = this.clientiGenerale.value;
      localStorage.setItem('utenteTemp', JSON.stringify(clienteTemp));
    }
    console.log(clienteTemp);

  }

  salva() {
    this.salvaClienteTemp();
    this.utilityService.salvaNuovoCliente();
  }
}
