import { UtilityService } from 'src/app/_services/utility.service';
import { Router } from '@angular/router';
import { AppComponent } from '../../app.component';
import { Component, AfterViewInit, ViewChild} from '@angular/core';
import { AuthenticationService, MessageService } from '../../_services';
import { Cliente } from '../../_models/client.model';
import { FormGroup, FormControl } from '@angular/forms';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { DatePipe } from '@angular/common';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ANNOSMT } from 'src/app/_models/annoSMT.model';
import { Impianti } from 'src/app/_models/impianti.model';

@Component({
  selector: 'app-clientie',
  templateUrl: './clientiE.component.html',
  styleUrls: ['./clientiE.component.scss']
})

export class ClientiEComponent implements AfterViewInit {
  @ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
  @ViewChild('frameSMT' , { static: false }) public showModalMailOnClick: ModalDirective;
  impianto: Impianti;
  visibleItems = 3;
  searchText = '';
  highlightedRow: any = null;
  private datiRisposta: any;
  headElements = ['Data SMT', 'Anno Inizio', 'Importo', 'Anno Fine', 'IVA Compresa'];
  elements = [];
  clientiE: FormGroup;
  titolo: string; annoInizioSMT: string; annoFineSMT: string; importoSMT: string; importoIVASMT: string;
  dataSMT: Date;
  private utente: Cliente; private utenteTemp: Cliente;
  edit: boolean; save: boolean; delete: boolean; saveE: boolean; deleteE: boolean; editE: boolean;
  SMT = new Array<ANNOSMT>();

  annoSMT = [
    { value: 'Seleziona Anno', label: 'Seleziona Anno' },
    { value: '2018', label: '2018' },
    { value: '2019', label: '2019' },
    { value: '2020', label: '2020' },
    { value: '2021', label: '2021' },
    { value: '2022', label: '2022' },
    { value: '2023', label: '2023' },
    { value: '2023', label: '2024' },
    { value: '2023', label: '2025' },
    { value: '2023', label: '2026' },
    { value: '2023', label: '2027' },
    { value: '2023', label: '2028' },
    { value: '2023', label: '2029' },
    { value: '2023', label: '2030' },
    { value: '2023', label: '2031' },
    { value: '2023', label: '2032' },
    { value: '2023', label: '2033' },
    { value: '2023', label: '2034' },
    { value: '2023', label: '2035' },
    { value: '2023', label: '2036' },
    { value: '2023', label: '2037' },
  ];
  itemSelected: any;
  constructor(
    private http: HttpClient,
    private utilityService: UtilityService,
    private datePipe: DatePipe,
    private router: Router,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {
    }

    public url = environment.apiEndpoint;
    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    this.authenticationService.tokenRefresh();
    /*if (localStorage.getItem('utente') !== null) {
      this.utente = JSON.parse(localStorage.getItem('utente'));

      this.utente.annoSMT.forEach(element => {
        this.elements.push(element);
      });
      this.mdbTableEditor.dataArray = this.elements;
    }*/
    this.impianto = JSON.parse(localStorage.getItem('impiantoSelected'));
    try {
      if (this.impianto.smt !== null && this.impianto.smt !== undefined ) {

        this.impianto.smt.sort(this.ordinaArray);

        this.mdbTableEditor.dataArray = this.impianto.smt;
      } else {
        this.impianto.smt = new Array<ANNOSMT>();
      }
    } catch (error) {
      this.impianto.smt = new Array<ANNOSMT>();
    }
    // inizializzo i button per il salvataggio, la modifica e la cancellazione di un cliente
    this.save = true;
    this.edit = false;
    this.delete = false;
    this.saveE = true;
    this.editE = false;
    this.deleteE = false;
     // ----------
    this.clientiE = new FormGroup({
      notaE: new FormControl(),
    });
    this.titolo = 'E';
    setTimeout(() => this.appComponent.setDiv(true, true), 0);
  }

  ordinaArray(a, b) {
    // for String case use .toUpperCase() to ignore character casing
    const bandA = a.dataImpianto;
    const bandB = b.dataImpianto;

    let comparison = 0;
    if (bandA > bandB) {
      comparison = 1;
    } else if (bandA < bandB) {
      comparison = -1;
    }
    return comparison;
  }

  ngAfterViewInit() {
    /*this.utente = JSON.parse(localStorage.getItem('utente'));
    this.utenteTemp = JSON.parse(localStorage.getItem('utenteTemp'));
    console.log(this.utenteTemp);
    if (this.utente !== null) {
      if (this.utente.codCliente !== null) {
          setTimeout(() => this.getAnnoSMT(), 0);
          setTimeout(() => this.setValues(this.utente, false), 0);
      } else {
        console.log('utente non inviato');
      }
    } else if (this.utenteTemp !== null) {
      if (this.utenteTemp.codCliente !== null) {
        setTimeout(() => this.setValues(this.utenteTemp, true), 0);
        if (this.utenteTemp.annoSMT !== undefined) {
          setTimeout(() => this.mdbTableEditor.dataArray = this.utenteTemp.annoSMT, 0);
        }
      } else {
        console.log('utente non inviato');
      }
    }*/
  }

  goTo(link: string) {
    this.salvaClienteTemp();
    this.router.navigate([link]);
  }

  setValues(cli: Cliente, nuovo: boolean) {
    if (!nuovo) {
      this.edit = true;
      this.save = false;
      this.delete = true;
      this.clientiE.disable();
    }
    if (cli !== null) {
        this.clientiE.patchValue({
          notaE: cli.notaE
        });
      }
  }

  enableForm() {
    this.clientiE.enable();
  }

  closeModal() {
    // this.emailModal = '';
    // this.tipoMailModal = '';
    this.showModalMailOnClick.hide();
  }

  getAnnoSMT() {
    this.utente = JSON.parse(localStorage.getItem('utente'));
    let i = 0;
    this.utente.annoSMT.forEach(() => {
      const newdata = new Date(this.utente.annoSMT[i].data);
      this.utente.annoSMT[i].dataString = newdata.toISOString().substring(0, 10);
      i++;
    });
    this.mdbTableEditor.dataArray = this.utente.annoSMT;
  }

  salvaClienteTemp() {
    localStorage.setItem('impiantoSelected', JSON.stringify(this.impianto));
  }

  salva() {
    this.salvaClienteTemp();
    this.utilityService.salvaNuovoCliente();
  }

  salvaSmt() {
    const SMTtemp = new ANNOSMT();
    SMTtemp.annoFine = this.annoFineSMT;
    SMTtemp.annoInizio = this.annoInizioSMT;
    SMTtemp.data = this.dataSMT;
    SMTtemp.importo = this.importoSMT;
    SMTtemp.fatturato = this.importoIVASMT;
    console.log(this.datePipe.transform(new Date(this.dataSMT), 'dd-MM-yyyy', 'IT'));
    console.log(this.datePipe.transform(this.dataSMT, 'dd-MM-yyyy', 'IT'));
    console.log(this.utilityService.fillInputDate(new Date(this.dataSMT), true));
    SMTtemp.dataString = this.datePipe.transform(new Date(this.dataSMT), 'dd-MM-yyyy', 'IT');
    this.SMT.push(SMTtemp);
    this.impianto.smt.push(SMTtemp);
    this.mdbTableEditor.dataArray = this.impianto.smt;
    this.itemSelected = null;
  }

  sendData(item, id) {
    this.impianto.smt.splice(id, 1);
    this.mdbTableEditor.dataArray = this.impianto.smt;
    this.itemSelected = item;
  }

  onOpened(event: any) {
    if (this.itemSelected !== null) {
      this.annoFineSMT = this.itemSelected.annoFine;
      this.annoInizioSMT = this.itemSelected.annoInizio;
      this.dataSMT = this.itemSelected.dataSMT;
      this.importoSMT = this.itemSelected.importo;
      this.importoIVASMT = this.itemSelected.fatturato;
    } else {
      this.annoFineSMT = 'Seleziona Anno';
      this.annoInizioSMT = 'Seleziona Anno';
    }

  }

  onClose() {
      this.annoFineSMT = 'Seleziona Anno';
      this.annoInizioSMT = 'Seleziona Anno';
      this.dataSMT = new Date();
      this.importoSMT = '';
      this.importoIVASMT = '';
  }
}
