import { Router } from '@angular/router';
import { AppComponent } from '../../app.component';
import { Component, AfterViewInit, ViewChild} from '@angular/core';
import { AuthenticationService } from '../../_services';
import { FormGroup, FormControl } from '@angular/forms';
import { Cliente } from 'src/app/_models/client.model';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { Impianti } from 'src/app/_models/impianti.model';
import { UtilityService } from 'src/app/_services/utility.service';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-clientiaddimpianto',
  templateUrl: './clientiAddImpianto.component.html',
  styleUrls: ['./clientiAddImpianto.component.scss']
})
export class ClientiAddImpiantoComponent implements AfterViewInit {
  // clientiImpianto: FormGroup;
  titolo: string;
  presentatoDa: any;
  utente: Cliente;
  private impianto: Impianti;
  edit; save; delete: boolean;
  ownerSelect: Array <any>;
  dataImpianto: string;
  primaRistrutturazione: string;
  secondaRistrutturazione: string;
  data3708: string;
  KMAR: string;
  TempoAR: string;
  // tslint:disable-next-line: variable-name
  data3708_2: string;
  modelloApparecchiaturaCE: string;
  modelloApparecchiaturaCESelect = [];
  CT: string;
  CTC: string;
  ponteRadio: string;
  materialeOmaggio: string;
  noteImpianto: string;
  dataConsegnaFC: string;
  dataFC: string;
  foglioSpiegazioneFC: string;
  numeroSuCTC: string;
  ilFC: string;
  configurazioneImpianto: string;
  porta: string;
  chiaviCGS: string;
  chiaviSS: string;
  chiaviAC: string;
  codiceImpianto: string;
  codiceAC: string;
  produttoreAC: string;
  nomeAC: string;
  clientiAddImpianto: FormGroup;
  impiantoSelected = true;

  infoSIMSelect = [
    { value: true, label: 'SI' },
    { value: false, label: 'NO' }
  ];
  infoTGSSelect = [
    { value: true, label: 'SI' },
    { value: false, label: 'NO' }
  ];
  privacySelect = [
    { value: true, label: 'SI' },
    { value: false, label: 'NO' }
  ];
  utenteTemp: Cliente;



  constructor(
    private utilityService: UtilityService,
    private router: Router,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService) {}

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    this.authenticationService.tokenRefresh();
    // inizializzo i button per il salvataggio, la modifica e la cancellazione di un cliente
    this.save = true;
    this.edit = false;
    this.delete = false;
    // ----------

    this.titolo = 'Dettaglio Impianto';

    this.clientiAddImpianto = new FormGroup({
      dataImpianto: new FormControl(),
      primaRistrutturazione: new FormControl(),
      secondaRistrutturazione: new FormControl(),
      data3708: new FormControl(),
      data3708_2: new FormControl(),
      modelloApparecchiaturaCE: new FormControl(),
      CT: new FormControl(),
      CTC: new FormControl(),
      ponteRadio: new FormControl(),
      materialeOmaggio: new FormControl(),
      noteImpianto: new FormControl(),
      dataConsegnaFC: new FormControl(),
      dataFC: new FormControl(),
      foglioSpiegazioneFC: new FormControl(),
      numeroSuCTC: new FormControl(),
      ilFC: new FormControl(),
      configurazioneImpianto: new FormControl(),
      porta: new FormControl(),
      infoTGSSel: new FormControl(),
      dataInfoTGS: new FormControl(),
      referente: new FormControl(),
      infoTGS: new FormControl(),
      infoSIMSel: new FormControl(),
      dataInfoSIM: new FormControl(),
      infoSIM: new FormControl(),
      dataPrivacy: new FormControl(),
      privacySel: new FormControl(),
      dataGaranzia: new FormControl(),
      chiaviCGS: new FormControl(),
      chiaviSS: new FormControl(),
      chiaviAC: new FormControl(),
      codiceImpianto: new FormControl(),
      codiceAC: new FormControl(),
      produttoreAC: new FormControl(),
      nomeAC: new FormControl(),
      KMAR: new FormControl(),
      TempoAR: new FormControl(),
    });

    this.ownerSelect = [
      { value: '1', label: 'CGS'},
      { value: '2', label: 'SecurSafe' },
      { value: '3', label: 'AC' }
    ];
  }

  ngAfterViewInit() {
    if (localStorage.getItem('centrale') !== null) {
      setTimeout(() => JSON.parse(localStorage.getItem('centrale')).forEach(element => {
        // tslint:disable-next-line: max-line-length
        this.modelloApparecchiaturaCESelect = [...this.modelloApparecchiaturaCESelect, { value: element.centrale, label: element.centrale}];
      }), 0);
    }
    try {
      this.impianto = JSON.parse(localStorage.getItem('impiantoSelected'));
      console.log('ImpSel - ' + localStorage.getItem('impiantoSelected'));
      if (this.impianto !== null) {
        setTimeout(() => this.setValues(this.impianto, true), 0);
        this.delete = true;
      }
    } catch (error) {
      console.log('No Impianto selected');
      this.impiantoSelected = false;
      this.utente = JSON.parse(localStorage.getItem('utenteTemp'));
      setTimeout(() => this.setValues(this.utente.impianti[0], true), 0);
      this.utente.impianti.splice(0, 1);
    }
  }
  deleteImp() {

    localStorage.removeItem('impiantoSelected');
    this.goTo('clientiImpianto');

  }
  goTo(link: string, chiaveAction: boolean = false) {
    this.salvaClienteTemp(chiaveAction);
    this.router.navigate([link]);
  }

  setValues(cli: Impianti, nuovo: boolean) {
    if (!nuovo) {
      this.edit = true;
      this.save = false;
      this.delete = true;
    }
    // setto i valori delle input

    if (cli !== undefined) {

        this.clientiAddImpianto.patchValue({
          dataImpianto: this.utilityService.fillInputDate(new Date(cli.dataImpianto)),
          primaRistrutturazione: this.utilityService.fillInputDate(new Date(cli.dataRistrutturazione_1)),
          secondaRistrutturazione: this.utilityService.fillInputDate(new Date(cli.dataRistrutturazione_2)),
          data3708: this.utilityService.fillInputDate(new Date(cli.data3708)),
          data3708_2: this.utilityService.fillInputDate(new Date(cli.data3708_2)),
          modelloApparecchiaturaCE: cli.nomeAC,
          CT: cli.CT,
          CTC: cli.CTC,
          ponteRadio: cli.ponteRadio,
          materialeOmaggio: cli.materialeOmaggio,
          noteImpianto: cli.noteImpianto,
          dataConsegnaFC: this.utilityService.fillInputDate(new Date(cli.dataForzeArmateConsegna)),
          dataFC: this.utilityService.fillInputDate(new Date(cli.dataForzeArmate)),
          foglioSpiegazioneFC: cli.spiegazioneForzeArmate,
          numeroSuCTC: cli.numeroSuCTC,
          ilFC: this.utilityService.fillInputDate(new Date(cli.dataNumeroSuCTC)),
          configurazioneImpianto: cli.configurazioneImpianto,
          porta: cli.porta,
          infoTGSSel: cli.infoTGS,
          dataInfoTGS: this.utilityService.fillInputDate(new Date(cli.infoTGSData)),
          infoTGS: cli.infoTGSData,
          infoSIMSel: cli.infoSIM,
          dataInfoSIM: this.utilityService.fillInputDate(new Date(cli.infoSIMData)),
          infoSIM: cli.infoSIMData,
          dataPrivacy: this.utilityService.fillInputDate(new Date(cli.infoPrivacyData)),
          privacySel: cli.infoPrivacy,
          dataGaranzia: this.utilityService.fillInputDate(new Date(cli.garanziaData)),
          chiaviCGS: cli.chiaviCGS,
          chiaviSS: cli.chiaviVigilanza,
          chiaviAC: cli.chiaviAC,
          codiceImpianto: cli.codiceImpianto,
          codiceAC: cli.codiceAC,
          produttoreAC: cli.produttoreAC,
          nomeAC: cli.nomeAC,
          TempoAR: cli.tempoAR,
          KMAR: cli.KM_AR,
      });
    }
  }



  salva() {
    this.salvaClienteTemp();
    this.utilityService.salvaNuovoCliente();
  }

  salvaClienteTemp(goToChiave: boolean = false) {

    let imp: Impianti;
    imp = JSON.parse(localStorage.getItem('impiantoSelected'));
    if (imp != null) {
    imp.dataImpianto = this.utilityService.StringToDate(this.clientiAddImpianto.controls.dataImpianto.value);
    imp.dataRistrutturazione_1 = this.utilityService.StringToDate(this.clientiAddImpianto.controls.primaRistrutturazione.value);
    imp.dataRistrutturazione_2 = this.utilityService.StringToDate(this.clientiAddImpianto.controls.secondaRistrutturazione.value);
    imp.data3708 = this.utilityService.StringToDate(this.clientiAddImpianto.controls.data3708.value);
    imp.data3708_2 = this.utilityService.StringToDate(this.clientiAddImpianto.controls.data3708_2.value);
    imp.nomeAC =  this.clientiAddImpianto.controls.modelloApparecchiaturaCE.value;
    imp.CT = this.clientiAddImpianto.controls.CT.value;
    imp.CTC = this.clientiAddImpianto.controls.CTC.value;
    imp.ponteRadio = this.clientiAddImpianto.controls.ponteRadio.value;
    imp.materialeOmaggio = this.clientiAddImpianto.controls.materialeOmaggio.value;
    imp.noteImpianto = this.clientiAddImpianto.controls.noteImpianto.value;
    imp.dataForzeArmateConsegna = this.utilityService.StringToDate(this.clientiAddImpianto.controls.dataConsegnaFC.value);
    imp.dataForzeArmate = this.utilityService.StringToDate(this.clientiAddImpianto.controls.dataFC.value);
    imp.spiegazioneForzeArmate = this.clientiAddImpianto.controls.foglioSpiegazioneFC.value;
    imp.numeroSuCTC = this.clientiAddImpianto.controls.numeroSuCTC.value;
    imp.dataNumeroSuCTC = this.utilityService.StringToDate(this.clientiAddImpianto.controls.numeroSuCTC.value);
    imp.configurazioneImpianto = this.clientiAddImpianto.controls.configurazioneImpianto.value;
    imp.porta = this.clientiAddImpianto.controls.porta.value;
    imp.infoTGS = this.clientiAddImpianto.controls.infoTGSSel.value;
    imp.infoTGSData = this.utilityService.StringToDate(this.clientiAddImpianto.controls.infoTGS.value);
    imp.infoSIMData = this.utilityService.StringToDate(this.clientiAddImpianto.controls.infoSIM.value);
    imp.infoPrivacyData = this.utilityService.StringToDate(this.clientiAddImpianto.controls.dataPrivacy.value);
    imp.infoSIM = this.clientiAddImpianto.controls.infoSIMSel.value;
    imp.infoPrivacy = this.clientiAddImpianto.controls.privacySel.value;
    imp.garanziaData = this.utilityService.StringToDate(this.clientiAddImpianto.controls.dataGaranzia.value);
    imp.chiaviAC = this.clientiAddImpianto.controls.chiaviAC.value;
    imp.chiaviCGS = this.clientiAddImpianto.controls.chiaviCGS.value;
    imp.chiaviVigilanza = this.clientiAddImpianto.controls.chiaviSS.value;
    imp.codiceImpianto = this.clientiAddImpianto.controls.codiceImpianto.value;
    imp.codiceAC = this.clientiAddImpianto.controls.codiceAC.value;
    imp.produttoreAC = this.clientiAddImpianto.controls.produttoreAC.value;
    imp.nomeAC = this.clientiAddImpianto.controls.nomeAC.value;
    imp.tempoAR = this.clientiAddImpianto.controls.TempoAR.value;
    imp.KM_AR = this.clientiAddImpianto.controls.KMAR.value;
    localStorage.setItem('impiantoSelected', JSON.stringify(imp));
    }
    }
}
