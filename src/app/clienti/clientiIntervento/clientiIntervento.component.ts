import { Cliente } from 'src/app/_models/client.model';
import { Router } from '@angular/router';
import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import {MdbTableEditorDirective} from 'mdb-table-editor';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { DatePipe } from '@angular/common';
import { Interventi } from 'src/app/_models/interventi.model';
import { Impianti } from 'src/app/_models/impianti.model';

@Component({
  selector: 'app-clientiintervento',
  templateUrl: './clientiIntervento.component.html',
  styleUrls: ['./clientiIntervento.component.scss']
})
export class ClientiInterventoComponent implements AfterViewInit {
@ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
titolo: string;
private utente: Cliente;
private utenteTemp: Cliente;
edit; save; delete: boolean;
private impianto: Impianti;
private intervento: Interventi;

headElements = ['Data', 'Riepilogo AB', 'Tipo Intervento', 'Costo'];
visibleItems = 6;
searchText = '';
ownerSelect: Array <any>;
highlightedRow: any = null;


constructor(
  private router: Router,
  private authenticationService: AuthenticationService,
  private datePipe: DatePipe
  ) {}

// tslint:disable-next-line: use-lifecycle-interface
ngOnInit() {
  // inizializzo i button per il salvataggio, la modifica e la cancellazione di un cliente
  this.save = true;
  this.edit = false;
  this.delete = false;
  this.impianto = new Impianti();
  this.titolo = 'INTERVENTO';
  this.authenticationService.tokenRefresh();
  // this.utente = JSON.parse(localStorage.getItem('utente'));
  this.impianto = JSON.parse(localStorage.getItem('impiantoSelected'));
  this.intervento = JSON.parse(localStorage.getItem('interventoSelected'));

  if (this.intervento !== null ) {
    if (this.impianto.interventi === undefined) {
      this.impianto.interventi = new Array<Interventi>();
    }
    this.impianto.interventi.push(this.intervento);
    localStorage.removeItem('interventoSelected');
  }

  let i = 0;
  if (this.impianto.interventi !== null && this.impianto.interventi !== undefined ) {
    this.impianto.interventi.forEach(() => {
      // tslint:disable-next-line: max-line-length
      this.impianto.interventi[i].dataInterventoString = this.datePipe.transform(this.impianto.interventi[i].dataIntervento, 'dd-MM-yyyy', 'IT');
      i++;
    });
    this.impianto.interventi.sort(this.ordinaArray);

    this.mdbTableEditor.dataArray = this.impianto.interventi;
  }

}

ordinaArray(a, b) {
  // for String case use .toUpperCase() to ignore character casing
  const bandA = a.dataIntervento;
  const bandB = b.dataIntervento;

  let comparison = 0;
  if (bandA > bandB) {
    comparison = 1;
  } else if (bandA < bandB) {
    comparison = -1;
  }
  return comparison;
}


ngAfterViewInit() {
  this.utente = JSON.parse(localStorage.getItem('utente'));
  if (this.utente !== null) {
    if (this.utente.codCliente !== null) {
        setTimeout(() => this.setValues(), 0);
    } else {
      console.log('utente non inviato');
    }
  }
}

goTo(link: string, item, id) {
  if (item !== undefined) {
    localStorage.setItem('interventoSelected', JSON.stringify(item));
    localStorage.setItem('impiantoSelected', JSON.stringify(this.impianto));
  } else {
    const imp = new Interventi();
    if(link !== 'clientiAddImpianto') {
      localStorage.setItem('interventoSelected', JSON.stringify(imp));
    }
    localStorage.setItem('impiantoSelected', JSON.stringify(this.impianto));
  }
  if (id !== undefined) {
    this.impianto.interventi.splice(id, 1);
    localStorage.setItem('impiantoSelected', JSON.stringify(this.impianto));
  }
  this.router.navigate([link]);


}

setValues() {
  this.edit = true;
  this.save = false;
  this.delete = true;
}

}
