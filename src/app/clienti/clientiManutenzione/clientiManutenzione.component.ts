import { Router } from '@angular/router';
import { Component, AfterViewInit} from '@angular/core';
import { AuthenticationService } from '../../_services';
import { FormGroup, FormControl } from '@angular/forms';
import { Cliente } from 'src/app/_models/client.model';
import { Impianti } from 'src/app/_models/impianti.model';
import { UtilityService } from 'src/app/_services/utility.service';

@Component({
  selector: 'app-clientimanutenzione',
  templateUrl: './clientiManutenzione.component.html',
  styleUrls: ['./clientiManutenzione.component.scss']
})
export class ClientiManutenzioneComponent implements AfterViewInit {

  clientiManutenzione: FormGroup;
  datiUtente$: any;
  titolo: string;
  tipoContratto: any;
  servizio: any;
  private utente: Cliente;
  edit: boolean; save: boolean; delete: boolean;
  impianto: Impianti;
  impiantoSelected: Impianti;

  infoSIMSelect = [
    { value: true, label: 'SI' },
    { value: false, label: 'NO' }
  ];
  infoTGSSelect = [
    { value: true, label: 'SI' },
    { value: false, label: 'NO' }
  ];
  privacySelect = [
    { value: true, label: 'SI' },
    { value: false, label: 'NO' }
  ];

  tipoStato = [
    { value: 'Attivo', label: 'Attivo' },
    { value: 'Sospeso', label: 'Sospeso' }
  ];

  constructor(
    private router: Router,
    private utilityService: UtilityService,
    private authenticationService: AuthenticationService) {
    }

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
    this.authenticationService.tokenRefresh();
    // inizializzo i button per il salvataggio, la modifica e la cancellazione di un cliente
    this.save = true;
    this.edit = false;
    this.delete = false;


    // ----------
    this.clientiManutenzione = new FormGroup({
      statoSMT: new FormControl(),
      importoSMT: new FormControl(),
      scadenzaSMT: new FormControl(),
      rateSMT: new FormControl(),
      fermoDaSMT: new FormControl(),
      fermoPerSMT: new FormControl(),
      firmatoSMT: new FormControl(),
      contrattoIVAFirmatoSMT: new FormControl(),
      dataContrattoIVAFirmatoSMT: new FormControl(),
      tipoContratto: new FormControl(),
      tipoServizio: new FormControl(),
      pagamentoIVA: new FormControl(),
      spedireFatture: new FormControl(),
      statoTLS: new FormControl(),
      importoTLS: new FormControl(),
      scadenzaTLS: new FormControl(),
      rateTLS: new FormControl(),
      fermoDaTLS: new FormControl(),
      fermoPerTLS: new FormControl(),
      firmatoTLS: new FormControl(),
      contrattoIVAFirmatoTLS: new FormControl(),
      dataContrattoIVAFirmatoTLS: new FormControl(),
      notaManutenzione: new FormControl(),
      dataDisdettaSMT: new FormControl(),
      importoDisdettaSMT: new FormControl(),
      tipologiaDisdettaSMT: new FormControl(),
      motivoDisdettaSMT: new FormControl(),
      dataDisdettaTLS: new FormControl(),
      importoDisdettaTLS: new FormControl(),
      tipologiaDisdettaTLS: new FormControl(),
      motivoDisdettaTLS: new FormControl(),
      notePercorso: new FormControl(),
      speseKM: new FormControl(),
      infoTGSSel: new FormControl(),
      dataInfoTGS: new FormControl(),
      infoSIMSel: new FormControl(),
      dataInfoSIM: new FormControl(),
      privacySel: new FormControl(),
      dataPrivacy: new FormControl(),
      dataGaranzia: new FormControl(),
    });
    this.titolo = 'MANUTENZIONE';
  }

  /*ngAfterViewInit() {
    this.utente = JSON.parse(localStorage.getItem('utente'));
    if (this.utente !== null) {
      if (this.utente.codCliente !== null) {
          setTimeout(() => this.setValues(), 0);
      } else {
        console.log('utente non inviato');
      }
    }
  }*/

  ngAfterViewInit() {
    try {
      this.impianto = JSON.parse(localStorage.getItem('impiantoSelected'));
      console.log('ImpSel - ' + localStorage.getItem('impiantoSelected'));
      if (this.impianto !== null) {
        setTimeout(() => this.setValues(this.impianto, false), 0);
      }
    } catch (error) {
      console.log('No Impianto selected');
    }
  }

  goTo(link: string) {
    this.salvaClienteTemp();
    this.router.navigate([link]);
  }

  salvaClienteTemp() {
    let imp: Impianti;
    imp = JSON.parse(localStorage.getItem('impiantoSelected'));
    imp.statoSMT = this.clientiManutenzione.controls.statoSMT.value;
    imp.importoSMT = this.clientiManutenzione.controls.importoSMT.value;
    imp.scadenzaSMT = this.utilityService.StringToDate(this.clientiManutenzione.controls.scadenzaSMT.value);
    imp.rateSMT = this.clientiManutenzione.controls.rateSMT.value;
    imp.fermoDaSMT = this.clientiManutenzione.controls.fermoDaSMT.value;
    imp.fermoPerSMT = this.clientiManutenzione.controls.fermoPerSMT.value;
    imp.firmatoSMT = this.utilityService.StringToDate(this.clientiManutenzione.controls.firmatoSMT.value);
    imp.contrattoIVAFirmatoSMT = this.clientiManutenzione.controls.contrattoIVAFirmatoSMT.value;
    imp.dataContrattoIVAFirmatoSMT = this.utilityService.StringToDate(this.clientiManutenzione.controls.dataContrattoIVAFirmatoSMT.value);
    imp.tipoContratto = this.clientiManutenzione.controls.tipoContratto.value;
    imp.tipoServizio = this.clientiManutenzione.controls.tipoServizio.value;
    imp.pagamentoIVA = this.clientiManutenzione.controls.pagamentoIVA.value;
    imp.spedireFatture = this.clientiManutenzione.controls.spedireFatture.value;
    imp.statoTLS = this.clientiManutenzione.controls.statoTLS.value;
    imp.importoTLS = this.clientiManutenzione.controls.importoTLS.value;
    imp.scadenzaTLS = this.utilityService.StringToDate(this.clientiManutenzione.controls.scadenzaTLS.value);
    imp.rateTLS = this.clientiManutenzione.controls.rateTLS.value;
    imp.fermoDaTLS = this.clientiManutenzione.controls.fermoDaTLS.value;
    imp.fermoPerTLS = this.clientiManutenzione.controls.fermoPerTLS.value;
    imp.firmatoTLS = this.clientiManutenzione.controls.firmatoTLS.value;
    imp.contrattoIVAFirmatoTLS = this.clientiManutenzione.controls.contrattoIVAFirmatoTLS.value;
    imp.dataContrattoIVAFirmatoTLS = this.utilityService.StringToDate(this.clientiManutenzione.controls.dataContrattoIVAFirmatoTLS.value);
    imp.notaManutenzione = this.clientiManutenzione.controls.notaManutenzione.value;
    imp.dataDisdettaSMT = this.utilityService.StringToDate(this.clientiManutenzione.controls.dataDisdettaSMT.value);
    imp.importoDisdettaSMT = this.clientiManutenzione.controls.importoDisdettaSMT.value;
    imp.tipologiaDisdettaSMT = this.clientiManutenzione.controls.tipologiaDisdettaSMT.value;
    imp.motivoDisdettaSMT = this.clientiManutenzione.controls.motivoDisdettaSMT.value;
    imp.dataDisdettaTLS = this.utilityService.StringToDate(this.clientiManutenzione.controls.dataDisdettaTLS.value);
    imp.importoDisdettaTLS = this.clientiManutenzione.controls.importoDisdettaTLS.value;
    imp.tipologiaDisdettaTLS = this.clientiManutenzione.controls.tipologiaDisdettaTLS.value;
    imp.motivoDisdettaTLS = this.clientiManutenzione.controls.motivoDisdettaTLS.value;
    imp.notePercorso = this.clientiManutenzione.controls.notePercorso.value;
    imp.speseKM = this.clientiManutenzione.controls.speseKM.value;
    localStorage.setItem('impiantoSelected', JSON.stringify(imp));
    }

  setValues(imp: Impianti, nuovo: boolean) {
    if (nuovo) {
      this.edit = true;
      this.save = false;
      this.delete = true;
    }
    // this.clientiManutenzione.disable();
    this.clientiManutenzione.patchValue({
      statoSMT: imp.statoSMT,
      importoSMT: imp.importoSMT,
      scadenzaSMT: this.utilityService.fillInputDate(new Date(imp.scadenzaSMT)),
      rateSMT: imp.rateSMT,
      fermoDaSMT: imp.fermoDaSMT,
      fermoPerSMT: imp.fermoPerSMT,
      firmatoSMT: this.utilityService.fillInputDate(new Date(imp.firmatoSMT)),
      contrattoIVAFirmatoSMT: imp.contrattoIVAFirmatoSMT,
      dataContrattoIVAFirmatoSMT: this.utilityService.fillInputDate(new Date(imp.dataContrattoIVAFirmatoSMT)),
      tipoContratto: imp.tipoContratto,
      tipoServizio: imp.tipoServizio,
      pagamentoIVA: imp.pagamentoIVA,
      spedireFatture: imp.spedireFatture,
      statoTLS: imp.statoTLS,
      importoTLS: imp.importoTLS,
      scadenzaTLS: this.utilityService.fillInputDate(new Date(imp.scadenzaTLS)),
      rateTLS: imp.rateTLS,
      fermoDaTLS: imp.fermoDaTLS,
      fermoPerTLS: imp.fermoPerTLS,
      firmatoTLS: this.utilityService.fillInputDate(new Date(imp.firmatoTLS)),
      contrattoIVAFirmatoTLS: imp.contrattoIVAFirmatoTLS,
      dataContrattoIVAFirmatoTLS: this.utilityService.fillInputDate(new Date(imp.dataContrattoIVAFirmatoTLS)),
      notaManutenzione: imp.notaManutenzione,
      dataDisdettaSMT: this.utilityService.fillInputDate(new Date(imp.dataDisdettaSMT)),
      importoDisdettaSMT: imp.importoDisdettaSMT,
      tipologiaDisdettaSMT: imp.tipologiaDisdettaSMT,
      motivoDisdettaSMT: imp.motivoDisdettaSMT,
      dataDisdettaTLS: this.utilityService.fillInputDate(new Date(imp.dataDisdettaTLS)),
      importoDisdettaTLS: imp.importoDisdettaTLS,
      tipologiaDisdettaTLS: imp.tipologiaDisdettaTLS,
      motivoDisdettaTLS: imp.motivoDisdettaTLS,
      notePercorso: imp.notePercorso,
      speseKM: imp.speseKM
    });
  }
}
