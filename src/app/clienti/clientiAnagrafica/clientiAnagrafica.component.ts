import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Component, AfterViewInit, ViewChild, ChangeDetectorRef, Output, EventEmitter} from '@angular/core';
import { AuthenticationService } from '../../_services';
import { FormGroup, FormControl } from '@angular/forms';
import { Cliente } from 'src/app/_models/client.model';
import { DatePipe } from '@angular/common';
import { ModalDirective, UtilService } from 'ng-uikit-pro-standard';
import { UtilityService } from 'src/app/_services/utility.service';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { Mail } from 'src/app/_models/email.model';
import { Telefono } from 'src/app/_models/telefono.model';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { AppComponent } from 'src/app/app.component';
import { ClientiGridComponent } from '../clientiGrid/clientiGrid.component';


@Component({
  selector: 'app-clientianagrafica',
  templateUrl: './clientiAnagrafica.component.html',
  styleUrls: ['./clientiAnagrafica.component.scss']
})
export class ClientiAnagraficaComponent implements AfterViewInit {
  @ViewChild('placesRef', {static: false}) placesRef: GooglePlaceDirective;
  @ViewChild('frameMail' , { static: false }) public showModalMailOnClick: ModalDirective;
  @ViewChild('frameTel' , { static: false }) public showModalMailOnClickTel: ModalDirective;
  @ViewChild('tableMail', { static: true }) mdbTableEditor: MdbTableEditorDirective;
  @ViewChild('tableTel', { static: true }) mdbTableEditorTel: MdbTableEditorDirective;

  visibleItems = 3;
  searchText = '';
  highlightedRow: any = null;
  editId: any;
  options = {
    types: [],
    componentRestrictions: { country: 'IT' }
    };

  headElements = ['Tipo', 'Mail'];
  headElements1 = ['Tipo', 'Telefono'];
  elementsMail = []; elementsTel = [];

  clientiAnagrafica: FormGroup;
  titolo: string;
  newItemTel: number; newItemMail: number;
  private utente: Cliente;
  private utenteTemp: Cliente;
  // tslint:disable-next-line: max-line-length
  edit: boolean; save: boolean; delete: boolean; saveMail: boolean; deleteMail: boolean; editMail: boolean; saveTel: boolean; deleteTel: boolean; editTel: boolean; arrowUpTel: boolean; arrowDownTel: boolean; arrowUpMail: boolean; arrowDownMail: boolean;
   // tslint:disable-next-line: max-line-length
  selectParking; tipoLocaleSel; professioneSel; statoCivileSel; privacySel; infoSIMSel; infoTGSSel; itemTelId; itemMailId; itemTel; itemMail;
  tipoLocale: Array<any>; professione: Array<any>; statoCivile: Array<any>; tipoSelectMail: Array<any>; tipoSelectTel: Array<any>;
  // tslint:disable-next-line: max-line-length
  emailModal: string; tipoMailModal: string; telModal: string; tipoTelModal: string; numeroCivico: string; strada: string; cap: string; nomecitta: string; PR: string; indirizzoCompleto: string;

  parkingSelect = [
    { value: '1', label: 'SI' },
    { value: '2', label: 'NO' }
  ];



  constructor(
    private appComponent: AppComponent,
    private utilityService: UtilityService,
    private datePipe: DatePipe,
    private router: Router,
    private authenticationService: AuthenticationService,
    public clieGrid: ClientiGridComponent,
    ) {}

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    this.authenticationService.tokenRefresh();
    if (localStorage.getItem('utenteTemp') !== null) {
      this.utente = JSON.parse(localStorage.getItem('utenteTemp'));
      if (this.utente.email !== undefined) {
        this.utente.email.forEach(element => {
          this.elementsMail.push(element);
        });
        this.mdbTableEditor.dataArray = this.elementsMail;
      }
      if (this.utente.telefoni !== undefined) {
        this.utente.telefoni.forEach(elementTel => {
          console.log(elementTel);
          this.elementsTel.push(elementTel);
        });
        this.mdbTableEditorTel.dataArray = this.elementsTel;
      }
      setTimeout(() => this.appComponent.setDiv(true, true), 0);
      setTimeout(() => this.appComponent.mostraGoogle(false), 0);
    }
    // inizializzo i button per il salvataggio, la modifica e la cancellazione di un cliente
    this.save = true;
    this.edit = false;
    this.delete = false;
    this.saveMail = true;
    this.editMail = false;
    this.deleteMail = false;
    this.arrowDownMail = false;
    this.arrowDownTel = false;
    this.arrowUpMail = false;
    this.arrowUpTel = false;
    this.saveTel = true;
    this.editTel = false;
    this.deleteTel = false;
    // ----------
    this.clientiAnagrafica = new FormGroup({
      tecnicoRiferimento: new FormControl(),
      codiceCliente: new FormControl(),
      nomeCognome: new FormControl(),
      indirizzo: new FormControl(),
      citta: new FormControl(),
      provincia: new FormControl(),
      cap: new FormControl(),
      villino: new FormControl(),
      palazzo: new FormControl(),
      scala: new FormControl(),
      piano: new FormControl(),
      interno: new FormControl(),
      quartiere: new FormControl(),
      tavola: new FormControl(),
      tipologiaLocale: new FormControl(),
      professione: new FormControl(),
      statoCivile: new FormControl(),
      citofono: new FormControl(),
      selectParking: new FormControl(),
      gestoreTelefonico: new FormControl(),
      referente: new FormControl(),
      infoTGS: new FormControl(),
      dataInfoTGS: new FormControl(),
      privacy: new FormControl(),
      dataPrivacy: new FormControl(),
      infoSIM: new FormControl(),
      dataInfoSIM: new FormControl(),
      dataGaranzia: new FormControl(),
      notaAnagrafica: new FormControl(),
      tabella: new FormControl(),
      searchTabella: new FormControl()
    });
    this.titolo = 'ANAGRAFICA';
    }

  ngAfterViewInit() {
    setTimeout(() => this.appComponent.setDiv(true, true), 0);
    setTimeout(() => this.appComponent.mostraGoogle(false), 0);
    setTimeout(() => this.tipoSelectMail = this.utilityService.getTipoTelMail(), 0);
    setTimeout(() => this.tipoSelectTel = this.tipoSelectMail, 0);
    setTimeout(() => this.tipoLocale = [], 0);
    setTimeout(() => this.professione = [], 0);
    setTimeout(() => this.statoCivile = [], 0);
    this.utilityService.getCentrale();
    if (localStorage.getItem('tipoLocale') !== null) {
      setTimeout(() => JSON.parse(localStorage.getItem('tipoLocale')).forEach(element => {
        this.tipoLocale = [...this.tipoLocale, { value: element.tipoLocale, label: element.tipoLocale}];
      }), 0);
    }
    if (localStorage.getItem('professione') !== null) {
      setTimeout(() => JSON.parse(localStorage.getItem('professione')).forEach(element => {
        this.professione = [...this.professione, { value: element.professione, label: element.professione}];
      }), 0);
    }
    if (localStorage.getItem('statoCivile') !== null) {
      setTimeout(() => JSON.parse(localStorage.getItem('statoCivile')).forEach(element => {
        this.statoCivile = [...this.statoCivile, { value: element.statoCivile, label: element.statoCivile}];
      }), 0);
    }
    this.utente = JSON.parse(localStorage.getItem('utente'));
    this.utenteTemp = JSON.parse(localStorage.getItem('utenteTemp'));
    let creaUtenteTemp = false;
    if (this.utente !== null) {
      if (this.utente.codCliente !== null) {
          creaUtenteTemp = false;
          setTimeout(() => this.setValues(this.utente, false), 0);
      } else {
        console.log('utente non inviato');
      }
    } else if (this.utenteTemp !== null) {
      if (this.utenteTemp.codCliente !== null) {
          creaUtenteTemp = false;
          setTimeout(() => this.setValues(this.utenteTemp, true), 0);
      } else {
        console.log('utente non inviato');
      }
    } else {
      creaUtenteTemp = true;
    }
    if (creaUtenteTemp) {
      this.salvaClienteTemp();
    }
  }

  goTo(link: string) {
    this.salvaClienteTemp();
    this.router.navigate([link]);
  }

  setValues(cli: Cliente, nuovo: boolean) {
    if (!nuovo) {
      this.edit = true;
      this.save = false;
      this.delete = false;
    }
    // setto i valori delle input

    if (cli !== undefined) {

        this.clientiAnagrafica.patchValue({
          tecnicoRiferimento: cli.tecnicoRiferimento,
          codiceCliente: cli.codCliente,
          nomeCognome: cli.nomeCognome,
          indirizzo: cli.indirizzo,
          citta: cli.citta,
          provincia: cli.provincia,
          cap: cli.CAP,
          villino: cli.villino,
          palazzo: cli.palazzina,
          scala: cli.scala,
          piano: cli.piano,
          interno: cli.interno,
          quartiere: cli.quartiere,
          tavola: cli.tavola,
          citofono: cli.citofono,
          gestoreTelefonico: cli.gestoreTelefonico,
          referente: cli.referente,
          notaAnagrafica: cli.notaAnagrafica,
          selectParking: cli.parcheggio,
          tipologiaLocale: cli.tipologiaLocale,
          professione: cli.professione,
          statoCivile: cli.statoCivile,
      });
        this.mdbTableEditor.dataArray = cli.email;
        this.mdbTableEditorTel.dataArray = cli.telefoni;
    }
  }

  closeMailModal() {
    this.emailModal = '';
    this.tipoMailModal = '';
    this.showModalMailOnClick.hide();
  }

  closeTelModal() {
    this.telModal = '';
    this.tipoTelModal = '';
    this.showModalMailOnClickTel.hide();
  }

  resetId() {
    setTimeout(() => this.tipoSelectMail = this.utilityService.getTipoTelMail(), 0);
    setTimeout(() => this.tipoSelectTel = this.utilityService.getTipoTelMail(), 0);
    this.itemMailId = null;
    this.itemTelId = null;
  }
  showEdit(item, id) {

    this.itemMail = item;
    this.itemMailId = id;
    this.newItemMail = id;
    this.saveMail = true;
    this.editMail = true;
    this.deleteMail = true;
    this.arrowUpMail = true;
    this.arrowDownMail = true;
  }

  showEditTel(item, id) {
    this.itemTel = item;
    this.itemTelId = id;
    this.newItemTel = id;
    console.log(item);
    // modifico i pulsanti
    this.saveTel = true;
    this.editTel = true;
    this.deleteTel = true;
    this.arrowDownTel = true;
    this.arrowUpTel = true;
  }

  Save(testo, tipo, isMail) {
    if (isMail) {
      const newArrayMail = [];
      let k = 0;
      const mail = new Mail();
      mail.mail = testo;
      mail.tipo = tipo;
      if (this.itemMailId === null) {
        this.elementsMail.push(mail);
        this.insertAndShift(this.elementsMail, this.elementsMail.length - 1, 0);
        this.mdbTableEditor.dataArray = this.elementsMail;
      } else {
        for (const val of this.elementsMail) {
          if (k === this.itemMailId) {
            newArrayMail.push(mail);
          } else {
            newArrayMail.push(val);
          }
          k++;
        }
        this.elementsMail = newArrayMail;
        this.insertAndShift(this.elementsMail, this.elementsMail.length - 1, 0);
        this.mdbTableEditor.dataArray = this.elementsMail;
      }
      this.closeMailModal();
    } else {
      const newArrayTel = [];
      let k = 0;
      const tel = new Telefono();
      tel.numero = testo;
      tel.tipo = tipo;
      if (this.itemTelId === null) {
        this.elementsTel.push(tel);
        this.insertAndShift(this.elementsTel, this.elementsMail.length - 1, 0);
        this.mdbTableEditorTel.dataArray = this.elementsTel;
      } else {
        for (const val of this.elementsTel) {
          if (k === this.itemTelId) {
            newArrayTel.push(tel);
          } else {
            newArrayTel.push(val);
          }
          k++;
        }
        this.elementsTel = newArrayTel;
        this.insertAndShift(this.elementsTel, this.elementsMail.length - 1, 0);
        this.mdbTableEditorTel.dataArray = this.elementsTel;
      }
      this.closeTelModal();
    }
}

  editaTel() {
    this.telModal = this.itemTel.numero;
    this.tipoTelModal = this.itemTel.tipo;
    this.showModalMailOnClickTel.show();
  }

  editaMail() {
    this.emailModal = this.itemMail.mail;
    this.tipoMailModal = this.itemMail.tipo;
    this.showModalMailOnClick.show();
  }

  salva() {
    this.salvaClienteTemp();
    this.utilityService.salvaNuovoCliente();
  }


  salvaClienteTemp() {
    let clienteTemp = new Cliente();
    if (localStorage.getItem('utenteTemp') !== null) {
      clienteTemp = JSON.parse(localStorage.getItem('utenteTemp'));
      clienteTemp.telefoni = new Array<Telefono>();
      this.elementsTel.forEach(element => {
        const telefono = new Telefono();
        telefono.numero = element.numero;
        telefono.tipo = element.tipo;
        clienteTemp.telefoni.push(telefono);
      });
      clienteTemp.email = new Array<Mail>();
      this.elementsMail.forEach(element => {
        const email = new Mail();
        email.mail = element.mail;
        email.tipo = element.tipo;
        clienteTemp.email.push(email);
      });
      clienteTemp.indirizzo = this.clientiAnagrafica.controls.indirizzo.value;
      clienteTemp.citta = this.clientiAnagrafica.controls.citta.value;
      clienteTemp.provincia = this.clientiAnagrafica.controls.provincia.value;
      clienteTemp.CAP = this.clientiAnagrafica.controls.cap.value;
      clienteTemp.villino = this.clientiAnagrafica.controls.villino.value;
      clienteTemp.palazzina = this.clientiAnagrafica.controls.palazzo.value;
      clienteTemp.scala = this.clientiAnagrafica.controls.scala.value;
      clienteTemp.piano = this.clientiAnagrafica.controls.piano.value;
      clienteTemp.interno = this.clientiAnagrafica.controls.interno.value;
      clienteTemp.quartiere = this.clientiAnagrafica.controls.quartiere.value;
      clienteTemp.tavola = this.clientiAnagrafica.controls.tavola.value;
      clienteTemp.citofono = this.clientiAnagrafica.controls.citofono.value;
      clienteTemp.tipologiaLocale = this.clientiAnagrafica.controls.tipologiaLocale.value;
      clienteTemp.parcheggio = this.clientiAnagrafica.controls.selectParking.value;
      clienteTemp.tecnicoRiferimento = this.clientiAnagrafica.controls.tecnicoRiferimento.value;
      clienteTemp.codCliente = this.clientiAnagrafica.controls.codiceCliente.value;
      clienteTemp.nomeCognome = this.clientiAnagrafica.controls.nomeCognome.value;
      clienteTemp.gestoreTelefonico = this.clientiAnagrafica.controls.gestoreTelefonico.value;
      clienteTemp.referente = this.clientiAnagrafica.controls.referente.value;
      clienteTemp.notaAnagrafica = this.clientiAnagrafica.controls.notaAnagrafica.value;
      clienteTemp.professione = this.clientiAnagrafica.controls.professione.value;
      clienteTemp.statoCivile = this.clientiAnagrafica.controls.statoCivile.value;

      localStorage.setItem('utenteTemp', JSON.stringify(clienteTemp));
    } else {
      clienteTemp = this.clientiAnagrafica.value;
      localStorage.setItem('utenteTemp', JSON.stringify(clienteTemp));
    }
    console.log(clienteTemp);
    this.clieGrid.sendUtente2();
  }

  // auto Complete
  public handleAddressChange(address: Address) {
    console.log(address);
    const indirizzo = address.address_components;
    this.numeroCivico = '';
    this.cap = '';
  // Do some stuff
    indirizzo.forEach(element => {
      switch (element.types[0]) {
        case 'street_number':
          this.numeroCivico = ', ' + element.short_name;
          break;
        case 'route':
          this.strada = element.short_name;
          break;
        case 'administrative_area_level_2':
          this.PR = element.short_name;
          break;
        case 'postal_code':
          this.cap = element.short_name;
          break;
        case 'administrative_area_level_3':
          this.nomecitta = element.short_name;
      }

      if (this.numeroCivico === undefined) {
        this.indirizzoCompleto = this.strada;
      } else {
        this.indirizzoCompleto = this.strada + this.numeroCivico;
      }

      this.clientiAnagrafica.patchValue({
        indirizzo: this.indirizzoCompleto,
        citta: this.nomecitta,
        provincia: this.PR,
        cap: this.cap
    });
   });
  }

  keyDownFunction(event, telModal, tipoTelModal, isMail) {
    if (event.keyCode === 13) {
      this.Save(telModal, tipoTelModal, isMail);
    }
  }

  moveTel(upDown) {
    let newindex;
    let oldindex = this.itemTelId;

    if (this.newItemTel !== oldindex && this.newItemTel !== undefined) {
      oldindex = this.newItemTel;
    }

    if (upDown === 1 && oldindex > 0) {
      newindex = oldindex - 1;
      this.newItemTel = newindex;
    } else if (upDown === 0 && oldindex < this.elementsTel.length - 1) {
      newindex = oldindex + 1;
      this.newItemTel = newindex;
    }

    if (newindex >= 0 && newindex <= this.elementsTel.length - 1) {
      this.insertAndShift(this.elementsTel, oldindex, newindex);
    }

}

moveMail(upDown) {
  let newindex;
  let oldindex = this.itemTelId;

  if (this.newItemMail !== oldindex && this.newItemMail !== undefined) {
    oldindex = this.newItemMail;
  }

  if (upDown === 1 && oldindex > 0) {
    newindex = oldindex - 1;
    this.newItemMail = newindex;
  } else if (upDown === 0 && oldindex < this.elementsMail.length - 1) {
    newindex = oldindex + 1;
    this.newItemMail = newindex;
  }

  if (newindex >= 0 && newindex <= this.elementsMail.length - 1) {
    this.insertAndShift(this.elementsMail, oldindex, newindex);
  }

}

insertAndShift(arr, from, to) {
  const cutOut = arr.splice(from, 1) [0]; // cut the element at index 'from'
  arr.splice(to, 0, cutOut);
  return arr;            // insert it at index 'to'
}


}
