import { UtilityService } from 'src/app/_services/utility.service';
import { Router } from '@angular/router';
import { AppComponent } from '../../app.component';
import { Component, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef, OnDestroy} from '@angular/core';
import { AuthenticationService } from '../../_services';
import { HttpClient } from '@angular/common/http';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { Porta } from 'src/app/_models/porta.model';
import { Cliente } from 'src/app/_models/client.model';
import { environment } from 'src/environments/environment';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl } from '@angular/forms';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { DefSerCil } from 'src/app/_models/defenderSerraturaCilindro.model';

@Component({
  selector: 'app-clientiporteadd',
  templateUrl: './clientiPorteAdd.component.html',
  styleUrls: ['./clientiPorteAdd.component.scss']
})
export class ClientiPorteAddComponent implements AfterViewInit {
  @ViewChild('frameSerDef' , { static: false }) public showModalMailOnClick: ModalDirective;
  titolo: string;
  // presentatoDa: any;
  item1: any;
  private porta: Porta;
  edit; save; del; readOnly: boolean;
  @ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
  headElements = ['Data', 'Fornitore', 'Ubicazione', 'Modello'];
  elementPorte = [];
  visibleItems = 7;
  highlightedRow: any = null;
  url: string;
  searchText = '';
  fornitoreModal: string;
  dataModal: Date;
  modelloModal: string;
  tipoModal: string;
  notaModal: string;
  ubicazione: string;
  modelloPorta: string;
  battenti: string;
  antispiffero: string;
  antirapina: string;
  occhiello: string;
  bloccoAste: string;
  manganese: string;
  nota: string;
  data: Date;
  // utente: Cliente;
  portaSelected: Porta;
  utenteTemp: Cliente;
  checked = false;
  fornitoreSelect: Array<any>;
  ubicazioneSelect: Array<any>;
  modelloSelect: Array<any>;
  battentiSelect: Array<any>;
  antispifferoSelect: Array<any>;
  antirapinaSelect: Array<any>;
  occhielloSelect: Array<any>;
  manganeseSelect: Array<any>;
  clientiSerrDef: FormGroup;
  defcilTemp: DefSerCil;

  constructor(
    private utilityService: UtilityService,
    private router: Router,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService,
    private http: HttpClient,
    private datePipe: DatePipe) {
    }

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
      this.authenticationService.tokenRefresh();
      this.edit = false;
      this.save = true;
      this.del = true;
      this.url = environment.apiEndpoint;
      this.defcilTemp = null;
      this.appComponent.showComponent = true;
      this.titolo = 'PORTE BLINDATE - Dettaglio';
         // ----------
      this.clientiSerrDef = new FormGroup({
        data: new FormControl(),
        fornitore: new FormControl(),
        ubicazione: new FormControl(),
        modelloPorta: new FormControl(),
        battenti: new FormControl(),
        antispiffero: new FormControl(),
        antirapina: new FormControl(),
        occhiello: new FormControl(),
        bloccoAste: new FormControl(),
        manganese: new FormControl(),
        nota: new FormControl(),
      });
      if (localStorage.getItem('utenteEdit') || localStorage.getItem('utenteTemp')) {
        this.readOnly = false;
        this.save = true;
        this.del = true;
      } else {
        this.readOnly = true;
        this.save = false;
        this.del = false;
      }

      if (localStorage.getItem('portaSelected')) {
        this.portaSelected = JSON.parse(localStorage.getItem('portaSelected'));
        this.mdbTableEditor.dataArray = this.portaSelected.defenderSerratureCilindri;
        setTimeout(() => this.setValues(this.portaSelected, true), 0);
      } else {
        this.portaSelected = new Porta();
        this.portaSelected.defenderSerratureCilindri = new Array<DefSerCil>();
        setTimeout(() => this.setValues(this.portaSelected, true), 0);
      }

    }

    ngAfterViewInit() {
      setTimeout(() => this.initSelect(), 0);
      this.clientiSerrDef.patchValue({
        data: this.portaSelected.data,
        fornitore: this.portaSelected.fornitore,
        ubicazione: this.portaSelected.ubicazione,
        modelloPorta: this.portaSelected.modello,
        battenti: this.portaSelected.battente,
        antispiffero: this.portaSelected.antispiffero,
        antirapina: this.portaSelected.antirapina,
        occhiello: this.portaSelected.occhiello,
        bloccoAste: this.portaSelected.bloccoAste,
        manganese: this.portaSelected.lastraManganese,
        nota: this.portaSelected.nota
      });

    }

      goTo(link: string) {
        // this.salvaClienteTemp();
        this.router.navigate([link]);
      }

      deletePorta() {
        const conferma = confirm('Vuoi eliminare la porta e tutti dati in essa contwenuti?');
        if (conferma) {
          localStorage.removeItem('portaSelected');
          this.goTo('clientiPB');
        }
      }

      setValues(porte: Porta, nuovo: boolean) {
        if (!nuovo) {
          this.clientiSerrDef.disable();
        }

        this.clientiSerrDef.patchValue({
          data: porte.data,
          dataString: this.utilityService.fillInputDate(new Date(porte.data), true),
          fornitore: porte.fornitore,
          ubicazione: porte.ubicazione,
          modelloPorta: porte.modello,
          battenti: porte.battente,
          antispiffero: porte.antispiffero,
          antirapina: porte.antirapina,
          occhiello: porte.occhiello,
          bloccoAste: porte.bloccoAste,
          manganese: porte.lastraManganese,
          nota: porte.nota
        });
      }

    sendData(item, id) {
      this.defcilTemp = item;
      this.portaSelected.defenderSerratureCilindri.splice(id, 1);
      this.mdbTableEditor.dataArray = this.portaSelected.defenderSerratureCilindri;
    }

    onOpened(event) {
      if (this.defcilTemp !== null) {
          this.dataModal = this.defcilTemp.data;
          this.fornitoreModal = this.defcilTemp.fornitore;
          this.modelloModal = this.defcilTemp.modello;
          this.notaModal = this.defcilTemp.nota;
          this.tipoModal = this.defcilTemp.tipo;

        // this.defcilTemp = null;
      } else {
          this.dataModal = new Date();
          this.fornitoreModal = '';
          this.modelloModal = '';
          this.notaModal = '';
          this.tipoModal = '';
      }

    }

    onClose() {

      this.annullaSerDef();

    }

    salvaSerDef() {
      const temp = new DefSerCil();
      temp.data = this.dataModal;
      temp.fornitore = this.fornitoreModal;
      temp.modello = this.modelloModal;
      temp.tipo = this.tipoModal;
      temp.nota = this.notaModal;
      temp.dataString = this.utilityService.fillInputDate(new Date(this.dataModal), true);
      if (this.portaSelected.defenderSerratureCilindri == undefined) {
        this.portaSelected.defenderSerratureCilindri = new Array<DefSerCil>();
      }
      this.defcilTemp = null;
      this.portaSelected.defenderSerratureCilindri.push(temp);
      this.mdbTableEditor.dataArray = this.portaSelected.defenderSerratureCilindri;

    }
    annullaSerDef() {
      if (this.defcilTemp != null) {
        this.portaSelected.defenderSerratureCilindri.push(this.defcilTemp);
        this.mdbTableEditor.dataArray = this.portaSelected.defenderSerratureCilindri;
      } else {
        // this.portaSelected.defenderSerratureCilindri.push(this.defcilTemp);
        // this.mdbTableEditor.dataArray = this.portaSelected.defenderSerratureCilindri;
      }
      this.defcilTemp = null;
    }

    editForm() {
      this.checked = false;
      this.edit = false;
      this.save = true;
      this.del = false;
    }

    salvaPorta() {
      this.portaSelected.data = this.utilityService.StringToDate(this.clientiSerrDef.controls.data.value);
      this.portaSelected.fornitore = this.clientiSerrDef.controls.fornitore.value;
      this.portaSelected.ubicazione = this.clientiSerrDef.controls.ubicazione.value;
      this.portaSelected.modello = this.clientiSerrDef.controls.modelloPorta.value;
      this.portaSelected.battente = this.clientiSerrDef.controls.battenti.value;
      this.portaSelected.antispiffero = this.clientiSerrDef.controls.antispiffero.value;
      this.portaSelected.antirapina = this.clientiSerrDef.controls.antirapina.value;
      this.portaSelected.occhiello = this.clientiSerrDef.controls.occhiello.value;
      this.portaSelected.bloccoAste = this.clientiSerrDef.controls.bloccoAste.value;
      this.portaSelected.lastraManganese = this.clientiSerrDef.controls.manganese.value;
      this.portaSelected.nota = this.clientiSerrDef.controls.nota.value;
      localStorage.setItem('portaSelected', JSON.stringify(this.portaSelected));
      this.goTo('clientiPB');
    }

    initSelect() {
      this.http.get(this.url + '/montatore/getMontatore', {})
        .subscribe(response => {
          const data = JSON.parse(JSON.stringify(response));
          const valoriSelect = [];
          valoriSelect.push({ value: '', label: 'Scegli il montatore' },);
          data.forEach((el: any) => {
            valoriSelect.push({ value: el.montatore, label: el.montatore});
          });
          this.fornitoreSelect = valoriSelect;
        });
      this.http.get(this.url + '/ubicazione/getUbicazione', {})
        .subscribe(response => {
          const data = JSON.parse(JSON.stringify(response));
          const valoriSelect = [];
          valoriSelect.push({ value: '', label: 'Scegli l\'ubicazione' }),
          data.forEach((el: any) => {
            valoriSelect.push({ value: el.tipo, label: el.tipo});
          });
          this.ubicazioneSelect = valoriSelect;
        });
      this.http.get(this.url + '/modelloPorta/getModello', {})
        .subscribe(response => {
          const data = JSON.parse(JSON.stringify(response));
          const valoriSelect = [];
          valoriSelect.push({ value: '', label: 'Scegli il modello' });
          data.forEach((el: any) => {
            valoriSelect.push({ value: el.modello, label: el.modello});
          });
          this.modelloSelect = valoriSelect;
        });
      this.battentiSelect = [
        { value: '', label: 'Scegli il battente' },
        { value: '1BDX', label: '1 Battente DX' },
        { value: '1BSX', label: '1 Battente SX' },
        { value: '2BDX', label: '2 Battente DX' },
        { value: '2BSX', label: '2 Battente SX' },
      ];
      this.antispifferoSelect = [
        { value: '', label: 'Scegli l\'antispiffero' },
        { value: 'SI', label: 'SI' },
        { value: 'NO', label: 'NO' },
        { value: '??', label: '??' },
        { value: '*', label: '*' },
      ];
      this.antirapinaSelect = [
        { value: '', label: 'Scegli l\'antirapina' },
        { value: 'SI', label: 'SI' },
        { value: 'NO', label: 'NO' },
        { value: '??', label: '??' },
        { value: '*', label: '*' },
      ];
      this.occhielloSelect = [
        { value: '', label: 'Scegli l\'occhiello' },
        { value: 'SI', label: 'SI' },
        { value: 'NO', label: 'NO' },
        { value: 'SI OMAGGIO', label: 'SI OMAGGIO' },
        { value: 'TVCC', label: 'TVCC' },
        { value: 'TVCC OMAGGIO', label: 'TVCC OMAGGIO' },
      ];
      this.manganeseSelect = [
        { value: '', label: 'Presenza lastra' },
        { value: 'SI', label: 'SI' },
        { value: 'NO', label: 'NO' },
        { value: 'SI OMAGGIO', label: 'SI OMAGGIO' },
        { value: 'TVCC', label: 'TVCC' },
        { value: 'TVCC OMAGGIO', label: 'TVCC OMAGGIO' },
      ];
    }

}

