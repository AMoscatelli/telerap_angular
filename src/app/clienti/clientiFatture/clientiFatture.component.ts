import { Router } from '@angular/router';
import { AppComponent } from '../../app.component';
import { Component, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef, OnDestroy} from '@angular/core';
import { AuthenticationService } from '../../_services';
import { HttpClient } from '@angular/common/http';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { Porta } from 'src/app/_models/porta.model';
import { Cliente } from 'src/app/_models/client.model';
import { environment } from 'src/environments/environment';
import { DatePipe } from '@angular/common';
import { UtilService, MDBModalRef, MDBModalService, ModalDirective } from 'ng-uikit-pro-standard';
import { UtilityComponent } from 'src/app/utility';
import { UtilityService } from 'src/app/_services/utility.service';
import { FormGroup, FormControl, EmailValidator } from '@angular/forms';
import { Telefono } from 'src/app/_models/telefono.model';
import { Impianti } from 'src/app/_models/impianti.model';
import { Fattura } from 'src/app/_models/fattura.model';
import { Mail } from 'src/app/_models/email.model';

@Component({
  selector: 'app-clientifatture',
  templateUrl: './clientiFatture.component.html',
  styleUrls: ['./clientiFatture.component.scss']
})
export class ClientiFattureComponent implements AfterViewInit {
  impianto: Impianti;
  fattura: Fattura;
  clientiFatture: FormGroup;
  titolo: string;
  tel: Array<any>;
  mail: Array<any>;
  private porta: Porta;
  edit; save; delete; editTel; editMail; saveMail; saveTel; deleteMail; deleteTel: boolean;
  @ViewChild('table' , { static: true }) mdbTableEditor: MdbTableEditorDirective;
  @ViewChild('tableTel' , { static: true }) mdbTableEditorTel: MdbTableEditorDirective;
  @ViewChild('tableMail' , { static: true }) mdbTableEditorMail: MdbTableEditorDirective;
  @ViewChild('frameTel' , { static: false }) public showModalTelOnClick: ModalDirective;
  @ViewChild('frameMail' , { static: false }) public showModalMailOnClick: ModalDirective;
  headElements = ['Ragione Sociale', 'P.Iva', 'Cod. Fiscale', 'SDI', 'PEC'];
  headElementsTel = ['Tipo', 'Numero'];
  headElementsMail = ['Tipo', 'E-mail'];
  visibleItems = 3;
  highlightedRow: any = null;
  url: string;
  searchText = '';
  utente: Cliente;
  checked = false;
  ragioneSociale: string;
  telefonoModal: string;
  emailModal: string;
  tipoMailModal: string;
  indirizzo: string;
  CAP: string;
  provincia: string;
  pIva: string;
  CF: string;
  legaleRappresentante: string;
  CF_LR: string;
  PEC: string;
  SDI: string;
  tipoModal: string;
  notaFattura: string;
  telefono = new Array<Telefono>();
  email = new Array<Mail>();
  tipoSelectTel: Array<any>;
  tipoSelectMail: Array<any>;
  frameTel: MDBModalRef;
  itemTel: string;
  itemMail: string;

      // tslint:disable-next-line: variable-name
      residenzaVia_LR: string;
      // tslint:disable-next-line: variable-name
    residenzaCittà_LR: string;
      // tslint:disable-next-line: variable-name
    residenzaProvincia_LR: string;
      // tslint:disable-next-line: variable-name
    residenzaCAP_LR: string;
      // tslint:disable-next-line: variable-name
    natoA_LR: string;
      // tslint:disable-next-line: variable-name
    dataNascita_LR: Date;
  idToEdit: any;
  showreset = true;


  constructor(
    private router: Router,
    private appComponent: AppComponent,
    private authenticationService: AuthenticationService,
    private utilityService: UtilityService,
    private datePipe: DatePipe) {

    }

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
      this.clientiFatture = new FormGroup({
        ragioneSociale: new FormControl(),
        indirizzo: new FormControl(),
        provincia: new FormControl(),
        CAP: new FormControl(),
        pIva: new FormControl(),
        CF: new FormControl(),
        PEC: new FormControl(),
        SDI: new FormControl(),
        legaleRappresentante: new FormControl(),
        residenzaVia_LR: new FormControl(),
        residenzaProvincia_LR: new FormControl(),
        residenzaCAP_LR: new FormControl(),
        residenzaCitta_LR: new FormControl(),
        natoA_LR: new FormControl(),
        dataNascita_LR: new FormControl(),
        CF_LR: new FormControl(),
        notaFattura: new FormControl()
      });
      this.authenticationService.tokenRefresh();
      this.edit = false;
      this.save = true;
      this.saveMail = true;
      this.saveTel = true;
      this.delete = false;
      this.url = environment.apiEndpoint;
      this.appComponent.showComponent = true;
      this.titolo = 'FATTURA';

      this.impianto = JSON.parse(localStorage.getItem('impiantoSelected'));

      try {
        if (this.impianto.fatture !== null && this.impianto.fatture !== undefined ) {

          this.impianto.fatture.sort(this.ordinaArray);

          this.mdbTableEditor.dataArray = this.impianto.fatture;
        } else {
          this.impianto.fatture = new Array<Fattura>();
        }
      } catch (error) {
        this.impianto.fatture = new Array<Fattura>();
      }


    }

    ordinaArray(a, b) {
      // for String case use .toUpperCase() to ignore character casing
      const bandA = a.dataImpianto;
      const bandB = b.dataImpianto;

      let comparison = 0;
      if (bandA > bandB) {
        comparison = 1;
      } else if (bandA < bandB) {
        comparison = -1;
      }
      return comparison;
    }

    getIndirizziFattura() {
        this.utente = JSON.parse(localStorage.getItem('utente'));
        let i = 0;
        this.utente.fatture.forEach(() => {
          this.utente.fatture[i].dataNascita_LR_String = this.datePipe.transform(this.utente.fatture[i].dataNascita_LR, 'dd-MM-yyyy', 'IT');
          i++;
        });
        this.impianto.fatture.sort(this.ordinaArray);
        this.mdbTableEditor.dataArray = this.utente.fatture;
      }

    ngAfterViewInit() {
      // popolo le Select della Modal

      setTimeout(() => this.tipoSelectTel = this.utilityService.getTipoTelMail(), 0);
      setTimeout(() => this.tipoSelectMail = this.utilityService.getTipoTelMail(), 0);
      /*this.utente = JSON.parse(localStorage.getItem('utente'));
      if (this.utente !== null) {
          if (this.utente.codCliente !== null) {
              setTimeout(() => this.getIndirizziFattura(), 0);
              setTimeout(() => this.setValues(), 0);
          } else {
            console.log('utente non inviato');
          }
        }*/
      }

      goTo(link: string) {
        if (this.checked) {
          alert('Attenzione ci sono dei dati che non sono stati salvati');
        } else {
          this.router.navigate([link]);
        }
      }

      setValues() {
        this.edit = true;
        this.save = false;
        this.delete = true;
      }

    sendData(item, id) {
      this.clientiFatture.patchValue({
        ragioneSociale: item.ragioneSociale,
        indirizzo: item.indirizzo,
        provincia: item.provincia,
        CAP: item.CAP,
        pIva: item.pIva,
        CF: item.CF,
        PEC: item.PEC,
        SDI: item.SDI,
        legaleRappresentante: item.legaleRappresentante,
        residenzaVia_LR: item.residenzaVia_LR,
        residenzaProvincia_LR: item.residenzaProvincia_LR,
        residenzaCAP_LR: item.residenzaCAP_LR,
        residenzaCitta_LR: item.residenzaCitta_LR,
        natoA_LR: item.natoA_LR,
        dataNascita_LR: item.dataNascita_LR,
        CF_LR: item.CF_LR,
        notaFattura: item.notaFattura
      });
      this.mdbTableEditorTel.dataArray = item.telefono;
      this.mdbTableEditorMail.dataArray = item.email;
      this.checked = true;
      this.impianto.fatture.splice(id, 1);
      this.showreset = false;
    }

    editForm() {
      this.checked = false;
      this.edit = false;
      this.save = true;
      this.delete = false;

    }

    modalMail(item , id) {
      this.idToEdit = id;
      this.itemMail = JSON.stringify(item);
      this.editMail = true;
      this.saveMail = true;
      this.deleteMail = true;
    }
    modalTel(item, id) {
      this.idToEdit = id;
      this.itemTel = JSON.stringify(item);
      this.editTel = true;
      this.saveTel = true;
      this.deleteTel = true;
    }
    editTelefono() {
      this.telefono.splice(this.idToEdit, 1);
      this.telefonoModal = JSON.parse(this.itemTel).numero;
      this.tipoModal = JSON.parse(this.itemTel).tipo;
      this.showModalTelOnClick.show();
    }
    editEmail() {
      this.email.splice(this.idToEdit, 1);
      this.emailModal = JSON.parse(this.itemMail).mail;
      this.tipoMailModal = JSON.parse(this.itemMail).tipo;
      this.showModalMailOnClick.show();
    }
    closeMailModal() {
      this.emailModal = '';
      this.tipoMailModal = '';
      this.showModalMailOnClick.hide();
    }
    closeTelModal() {
      this.telefonoModal = '';
      this.tipoModal = '';
      this.showModalTelOnClick.hide();
    }
    salvaTel() {

      const tel = new Telefono();
      tel.numero = this.telefonoModal;
      tel.tipo = this.tipoModal;
      this.telefono.push(tel);
      this.mdbTableEditorTel.dataArray = this.telefono;
      this.closeTelModal();

    }
    salvaMail() {

      const email = new Mail();
      email.mail = this.emailModal;
      email.tipo = this.tipoMailModal;
      this.email.push(email);
      this.mdbTableEditorMail.dataArray = this.email;
      this.closeMailModal();

    }
    salva() {
      let cliente = new Cliente();
      cliente = this.clientiFatture.value;
      localStorage.setItem('utenteTemp', JSON.stringify(cliente));
      console.log(cliente);
    }

    salvaFatturazione() {
      let fat = new Fattura();
      fat = this.clientiFatture.value;
      fat.telefono = new Array<Telefono>();
      fat.email = new Array<Mail>();
      fat.telefono = this.telefono;
      fat.email = this.email;
      this.impianto.fatture.push(fat);
      localStorage.setItem('impiantoSelected', JSON.stringify(this.impianto));
      this.mdbTableEditor.dataArray = this.impianto.fatture;
      this.reset();
      this.checked = false;
    }

    reset() {
      const fattEmpty = new Fattura();
      this.clientiFatture.reset(fattEmpty);
      this.mdbTableEditorTel.dataArray = new Array<Telefono>();
      this.mdbTableEditorMail.dataArray = new Array<Mail>();
    }

}

