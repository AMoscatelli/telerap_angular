import { Router } from '@angular/router';
import { AppComponent } from '../../app.component';
import { Component, AfterViewInit, ViewChild} from '@angular/core';
import { AuthenticationService, AlertService } from '../../_services';
import { UtilityService } from 'src/app/_services/utility.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Cliente } from 'src/app/_models/client.model';
import { MdbTableEditorDirective } from 'mdb-table-editor';
import { UtilityComponent } from 'src/app/utility';
import { DatePipe } from '@angular/common';
import { Impianti } from 'src/app/_models/impianti.model';

@Component({
selector: 'app-clientiimpianto',
templateUrl: './clientiImpianto.component.html',
styleUrls: ['./clientiImpianto.component.scss']
})
export class ClientiImpiantoComponent implements AfterViewInit {
@ViewChild('table', { static: true }) mdbTableEditor: MdbTableEditorDirective;
titolo: string;
private utente: Cliente;
private utenteTemp: Cliente;
edit; save; delete: boolean;

headElements = ['Codice Impianto', 'Data Impianto', 'Nome AC', 'Codice AC'];
visibleItems = 6;
searchText = '';
ownerSelect: Array <any>;
highlightedRow: any = null;
editaImpianto: boolean;





constructor(
  private router: Router,
  private authenticationService: AuthenticationService,
  private utilityService: UtilityService,
  private datePipe: DatePipe,
  private alert: AlertService
  ) {}

// tslint:disable-next-line: use-lifecycle-interface
ngOnInit() {
  this.authenticationService.tokenRefresh();
  // inizializzo i button per il salvataggio, la modifica e la cancellazione di un cliente
  this.save = true;
  this.edit = false;
  this.delete = false;
  this.titolo = 'IMPIANTO';
  this.editaImpianto = false;

  this.utente = JSON.parse(localStorage.getItem('utente'));
  this.utenteTemp = JSON.parse(localStorage.getItem('utenteTemp'));
  let i = 0;
  if (this.utente !== null ) {
  this.utente.impianti.forEach(() => {
        // tslint:disable-next-line: max-line-length
        this.utente.impianti[i].dataImpiantoString = this.utilityService.fillInputDate(new Date(this.utente.impianti[i].dataImpianto), true);
        // tslint:disable-next-line: max-line-length
        this.utente.impianti[i].dataRistrutturazione_1String = this.utilityService.fillInputDate(new Date(this.utente.impianti[i].dataRistrutturazione_1), true);
        // tslint:disable-next-line: max-line-length
        this.utente.impianti[i].dataRistrutturazione_2String = this.utilityService.fillInputDate(new Date(this.utente.impianti[i].dataRistrutturazione_2), true);
        this.utente.impianti[i].data3708String = this.utilityService.fillInputDate(new Date(this.utente.impianti[i].data3708), true);
        this.utente.impianti[i].data3708_2String = this.utilityService.fillInputDate(new Date(this.utente.impianti[i].data3708_2), true);
        // tslint:disable-next-line: max-line-length
        this.utente.impianti[i].dataForzeArmateString = this.utilityService.fillInputDate(new Date(this.utente.impianti[i].dataForzeArmate), true);
        // tslint:disable-next-line: max-line-length
        this.utente.impianti[i].dataNumeroSuCTCString = this.utilityService.fillInputDate(new Date(this.utente.impianti[i].dataNumeroSuCTC), true);
        // tslint:disable-next-line: max-line-length
        this.utente.impianti[i].dataForzeArmateConsegnaString = this.utilityService.fillInputDate(new Date(this.utente.impianti[i].dataForzeArmateConsegna), true);
        i++;
      });
  this.utente.impianti.sort(this.ordinaArray);
  this.mdbTableEditor.dataArray = this.utente.impianti;
  this.utenteTemp = null;
  } else if (this.utenteTemp !== null) {
      if (this.utenteTemp.impianti === undefined) {
        this.utenteTemp.impianti = new Array<Impianti>();
      }
      if (localStorage.getItem('impiantoSelected')) {
        let impiantoCached = new Impianti();
        impiantoCached = JSON.parse(localStorage.getItem('impiantoSelected'));
        if (JSON.stringify(impiantoCached).length === 2) {

        } else {
          this.utenteTemp.impianti.push(impiantoCached);
          localStorage.removeItem('impiantoSelected');
          localStorage.setItem('utenteTemp', JSON.stringify(this.utenteTemp));
        }
      }
      this.utenteTemp.impianti.forEach(() => {
        // tslint:disable-next-line: max-line-length
        this.utenteTemp.impianti[i].dataImpiantoString = this.utilityService.fillInputDate(new Date(this.utenteTemp.impianti[i].dataImpianto), true);
        // tslint:disable-next-line: max-line-length
        this.utenteTemp.impianti[i].dataRistrutturazione_1String = this.utilityService.fillInputDate(new Date(this.utenteTemp.impianti[i].dataRistrutturazione_1), true);
        // tslint:disable-next-line: max-line-length
        this.utenteTemp.impianti[i].dataRistrutturazione_2String = this.utilityService.fillInputDate(new Date(this.utenteTemp.impianti[i].dataRistrutturazione_2), true);
        // tslint:disable-next-line: max-line-length
        this.utenteTemp.impianti[i].data3708String = this.utilityService.fillInputDate(new Date(this.utenteTemp.impianti[i].data3708), true);
        // tslint:disable-next-line: max-line-length
        this.utenteTemp.impianti[i].data3708_2String = this.utilityService.fillInputDate(new Date(this.utenteTemp.impianti[i].data3708_2), true);
        // tslint:disable-next-line: max-line-length
        this.utenteTemp.impianti[i].dataForzeArmateString = this.utilityService.fillInputDate(new Date(this.utenteTemp.impianti[i].dataForzeArmate), true);
        // tslint:disable-next-line: max-line-length
        this.utenteTemp.impianti[i].dataNumeroSuCTCString = this.utilityService.fillInputDate(new Date(this.utenteTemp.impianti[i].dataNumeroSuCTC), true);
        // tslint:disable-next-line: max-line-length
        this.utenteTemp.impianti[i].dataForzeArmateConsegnaString = this.utilityService.fillInputDate(new Date(this.utenteTemp.impianti[i].dataForzeArmateConsegna), true);
        i++;
      });
      this.utenteTemp.impianti.sort(this.ordinaArray);
      setTimeout(() => this.mdbTableEditor.dataArray = this.utenteTemp.impianti, 0);
      this.utente = null;
  }
  // console.log('impianto: ' + this.utente.impianti);

}

ordinaArray(a, b) {
  // for String case use .toUpperCase() to ignore character casing
  const bandA = a.dataImpianto;
  const bandB = b.dataImpianto;

  let comparison = 0;
  if (bandA > bandB) {
    comparison = 1;
  } else if (bandA < bandB) {
    comparison = -1;
  }
  return comparison;
}

ngAfterViewInit() {
  this.utente = JSON.parse(localStorage.getItem('utente'));
  if (this.utente !== null) {
    if (this.utente.codCliente !== null) {
        setTimeout(() => this.setValues(), 0);
    } else {
      console.log('utente non inviato');
    }
  }
}
goTo(link: string, item, id) {
    if (item !== undefined) {
      localStorage.setItem('impiantoSelected', JSON.stringify(item));
    } else {
      const imp = new Impianti();
      localStorage.setItem('impiantoSelected', JSON.stringify(imp));
    }
    if (id !== undefined) {
      if (this.utenteTemp !== null) {
        this.utenteTemp.impianti.splice(id, 1);
      } else {
        this.utente.impianti.splice(id, 1);
      }
      localStorage.setItem('utenteTemp', JSON.stringify(this.utenteTemp));
    }
    /*if (link !== 'clientiAddImpianto') {
      localStorage.setItem('utenteTemp', JSON.stringify(this.utenteTemp));
    }*/
    this.router.navigate([link]);


}

salva() {
  this.utilityService.salvaNuovoCliente();
}

setValues() {
  this.edit = true;
  this.save = false;
  this.delete = false;
}
}

