import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(
      private router: Router) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log(request);
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401 || err.status === 400) {
                // auto logout if 401 response returned from api
                // this.authenticationService.logout();
                // tslint:disable-next-line: deprecation
                // localStorage.removeItem('currentUser');
                // this.currentUserSubject.next(null);
                // console.log(localStorage.getItem('currentUser'));
                // this.appComponent.setDiv(false);
                setTimeout(() => this.router.navigate(['sessionExpired']), 0);
            } else {
               // return throwError('');
               const error = err.error.message || err.statusText;
               return throwError(error);
            }
            // const error = err.error.message || err.statusText;
            // return throwError(error);
        }));
    }
}
