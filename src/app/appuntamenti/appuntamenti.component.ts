import { Router } from '@angular/router';
import { AlertService } from './../_services/alert.service';
import { IDCalendar } from './../_models/calendarId.model';
import { EventoGoogleCalendar } from './../_models/GoogleCalendarEvent.model';
import { RispostaServer } from './../_models/response.model';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { CalendarEvent } from 'mdb-calendar';
import { AppComponent } from '../app.component';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { FormGroup, FormControl } from '@angular/forms';
import { ModalDirective, IMyOptions } from 'ng-uikit-pro-standard';
import { ModalService } from '../_services';
import { CalendarColor } from '../_models/calendarColor.model';
import { DatePipe } from '@angular/common';
import { Risposta } from '../_models';

@Component({
  selector: 'app-appuntamenti',
  templateUrl: './appuntamenti.component.html',
  styleUrls: ['./appuntamenti.component.scss']
})


export class AppuntamentiComponent {
  @ViewChild('frame', { static: true }) public frame: ModalDirective;
  @ViewChild('frameGoogleAdd', { static: true }) public frameGoogleAdd: ModalDirective;
  @ViewChild('frameGoogleEdit', { static: true }) public frameGoogleEdit: ModalDirective;
  @ViewChild('loginRef', {static: true }) loginElement: ElementRef;
  googleForm: FormGroup;
  trovato: boolean;
  searchResult: [];
  calendarColorResult: [];
  calendarEvent: [];
  calendarID: Array<any>;
  calendarIdSelect;
  calendarIDEdit: Array<any>;
  calendarIdSelectEdit;
  titolo: string; oraFine: string; oraInizio: string; dataEvento: string; titoloAdd: string; oraInizioAdd: string; oraFineAdd: string;
  dataEventoEdit: Date; idCalendarDelete: string; prevIdCalendar: string;
  constructor(
    private router: Router,
    private allert: AlertService,
    private datePipe: DatePipe,
    private modalService: ModalService,
    private http: HttpClient,
    private appComponent: AppComponent) {
    }
    public url = environment.apiEndpoint;
    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

  events: CalendarEvent[] = [];

    // tslint:disable-next-line: max-line-length
    months = [ 'Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre' ];

    public myDatePickerOptions: IMyOptions = {
    // Strings and translations
        dayLabels: {su: 'Dom', mo: 'Lun', tu: 'Mar', we: 'Mer', th: 'Gio', fr: 'Ven', sa: 'Sab'},
        dayLabelsFull: {su: 'Domenica', mo: 'Lunedì', tu: 'Martedì', we: 'Mercoledì', th: 'Giovedì', fr: 'Venerdì', sa: 'Sabato'},
        monthLabels: { 1: 'Gen', 2: 'Feb', 3: 'Mar', 4: 'Apr', 5: 'Mag', 6: 'Giu', 7: 'Lug', 8: 'Ago', 9: 'Set', 10:
        'Ott', 11: 'Nov', 12: 'Dic' },
        monthLabelsFull: { 1: 'Gennaio', 2: 'Febbraio', 3: 'Marzo', 4: 'Aprile', 5: 'Maggio', 6: 'Giugno', 7: 'Luglio', 8:
        'Agosto', 9: 'Settembre', 10: 'Ottobre', 11: 'Novembre', 12: 'Dicembre' },

        // Buttons
        todayBtnTxt: 'Oggi',
        clearBtnTxt: 'Cancella',
        closeBtnTxt: 'Chiudi',

        // Format
        dateFormat: 'dd-mm-yyyy',

        // First day of the week
        firstDayOfWeek: 'mo',

        disableWeekends: false,

        // Enable dates (when disabled)

        // Year limits
        minYear: 2018,
        maxYear: 2100,

        // Show Today button
        showTodayBtn: true,

        // Show Clear date button
        showClearDateBtn: true,

        markCurrentDay: true,
        // markDates: [{dates: [{year: 2018, month: 10, day: 20}], color: '#303030'}],
        // markWeekends: undefined,
        // disableHeaderButtons: false,
        // showWeekNumbers: false,
        // height: '100px',
        // width: '50%',
         selectionTxtFontSize: '15px'
      };
    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
      this.googleForm = new FormGroup({
        googleCode: new FormControl(),
      });
      setTimeout(() => this.appComponent.setDiv(false, true), 0);
      setTimeout(() => this.appComponent.mostraGoogle(true), 0);
      setTimeout(() => this.calendarID = [], 0);
      setTimeout(() => this.calendarIDEdit = [], 0);
      this.modalService.setModal(this.frame);
      this.getCalendarColor();
      this.checkGoogleAuth();
    }

    // metodo che verifica se l'autenticazione con Google è già stata effettuata
    public checkGoogleAuth() {
      this.http.get(this.url + '/google/getAuth', this.httpOptions)
      .subscribe(response => {
        // tslint:disable-next-line: variable-name
        let risposta_login = new RispostaServer();
        risposta_login = JSON.parse(JSON.stringify(response));

        // console.log(risposta_login.risposta);

        if (risposta_login.risposta === 'Login OK') {
          this.appComponent.mostraGoogle(false);
          this.getCalendar();
        }
       // console.log(this.calendarColorResult);
  });
    }

    // metodo che richiama MongoDb per controllare i colori da assegnare ai vari calendari
    public getCalendarColor() {
      this.http.get(this.url + '/calendar/getCalendar', this.httpOptions)
          .subscribe(response => {
            this.calendarColorResult = JSON.parse(JSON.stringify(response));
            localStorage.setItem('calendarColorResult', JSON.stringify(response));
            console.log(this.calendarColorResult);
      });
    }

    // metodo che confronta i calendari scaricati dal db con quelli scaricati da Google Calendar
    public checkCalendarColor(id , stato: boolean) {
      this.trovato = false;
      if (id.includes('contacts')) {
        // nn fare niente
      } else {
        if (this.calendarColorResult.length !== 0) {
          // tslint:disable-next-line: prefer-for-of
          for (let i = 0; i < this.calendarColorResult.length; i++) {
            let calendarioDB = new CalendarColor();
            calendarioDB = this.calendarColorResult[i];
            if (id === calendarioDB.nome) {
              this.getCalendarEvent(id, calendarioDB.colore);
              this.trovato = true;
              console.log('calendario già presente su DB');
              break;
            }
          }
          if (this.trovato === false) {
            this.scriviCalendarColor(id, stato);
          }
        } else {
          this.scriviCalendarColor(id, stato);
        }
      }
    }

    // metodo che scrive sul db i nuovi Calendar di Google
    public scriviCalendarColor(id, stato: boolean) {
      this.http.post(this.url + '/calendar/create', {
        nome: id,
        colore: 'default',
        utente: localStorage.getItem('currentUser')
        }, this.httpOptions)
        .subscribe(response => {
            console.log('Colore calendario scritto correttamente');

            if (stato === true) {
              this.getCalendar();
            }
            // this.getCalendar();
        });
    }

    public getAuth() {
      this.http.get(this.url + '/google/getCalendar', this.httpOptions)
          .subscribe(response => {
            let rispostaserver = new RispostaServer();
            rispostaserver = JSON.parse(JSON.stringify(response));
            const x = screen.width / 2 - 700 / 2;
            const y = screen.height / 2 - 600 / 2;
            window.open(rispostaserver.risposta, 'sharegplus', 'height=485,width=700,left=' + x + ',top=' + y);
      });
    }

    public pushCode() {
      console.log(this.googleForm.get('googleCode').value);
      localStorage.setItem('googleCode', this.googleForm.get('googleCode').value);
      this.http.post(this.url + '/google/pushCode', {
        googleCode: this.googleForm.get('googleCode').value,
        }, this.httpOptions)
        .subscribe(response => {
          console.log(response);
          this.getCalendar();
        });

      this.googleForm.patchValue({
          googleCode: ''
      });
    }

    public getCalendar() {
      this.http.post(this.url + '/google/getEvent', {
        }, this.httpOptions)
        .subscribe(response => {
          this.appComponent.mostraGoogle(false);
          this.searchResult = JSON.parse(JSON.stringify(response));
          localStorage.setItem('calendarUtente', JSON.stringify(response));
          let calendariGoogleId = new IDCalendar();
          let i = 1;
          this.searchResult.forEach(idcalendar => {
          calendariGoogleId = idcalendar;

          if (i < this.searchResult.length) {

            this.checkCalendarColor(calendariGoogleId.id, false);
            // tslint:disable-next-line: new-parens
            if (calendariGoogleId.id !== null || calendariGoogleId.id !== undefined) {
              if (calendariGoogleId.id.includes('holiday') || (calendariGoogleId.id.includes('contacts'))) {

              } else {
                this.calendarID = [...this.calendarID, { value: calendariGoogleId.id, label: calendariGoogleId.id}];
                this.calendarIDEdit = [...this.calendarIDEdit, { value: calendariGoogleId.id, label: calendariGoogleId.id}];
              }
              }
            i++;
          } else {
            this.checkCalendarColor(calendariGoogleId.id, true);
          }

           });
          console.log(this.calendarID);
        });
    }

    public getCalendarEvent(calendarID, colore) {
      this.http.post(this.url + '/google/getEvent', {
        calendarId: calendarID,
        }, this.httpOptions)
        .subscribe(response => {
          this.calendarEvent = JSON.parse(JSON.stringify(response));
          let i = 1;
          if (this.calendarEvent.length !== undefined) {
            // tslint:disable-next-line: new-parens
            this.calendarEvent.forEach(element => {
              let eventoGoogle = new EventoGoogleCalendar();
              eventoGoogle = element;
              let evento;
              if (eventoGoogle.start.toString().length === 10)  {
                evento =   {
                  id: eventoGoogle.id,
                  startDate: new Date(eventoGoogle.start),
                  endDate: new Date(eventoGoogle.start),
                  name: eventoGoogle.summary,
                  color: colore,
                  };
              } else {
                evento =   {
                  id: eventoGoogle.id,
                  startDate: new Date(eventoGoogle.start),
                  endDate: new Date(eventoGoogle.end),
                  name: eventoGoogle.summary,
                  color: colore,
                  };
              }
              i++;
              // console.log(evento);
              this.events = [...this.events, evento];
              // console.log(this.events);
              });
          }

        });
    }

    public showModal(): void {
      this.frame.show();
    }

    /* necessaria modifica per creare emitter su node_modules\mdb-calendar\fesm2015\mdb-calendar.js riga 235
    Creare nuovo emitter
      this.dayClick = new EventEmitter();

    Aggiungere output al MdbCalendarComponent.propDecorators
      dayClick: [{ type: Output }],

    Sostituire il metodo della funzione onDayClick(day) con
      this.dayClick.emit(day);

    Richiamare il metodo dall'HTML
      <mdb-calendar (dayClick)="dayClickLog($event)".... */

      onEventEdit(event: CalendarEvent) {
        const colorDBEdit = JSON.parse(localStorage.getItem('calendarColorResult'));
        colorDBEdit.forEach(element => {
          // console.log(element.nome);
          if (event.color === element.colore) {
            this.calendarIdSelectEdit = element.nome;
          }
        });
        this.prevIdCalendar = this.calendarIdSelectEdit;
        this.frameGoogleEdit.show();
        this.titolo = event.name;
        this.oraInizio = this.datePipe.transform(event.startDate, 'HH:mm');
        this.oraFine = this.datePipe.transform(event.endDate, 'HH:mm');
        this.dataEventoEdit = new Date(event.startDate);
        this.idCalendarDelete = event.id;
      }

      public modificaEventoGoogle() {
        /*this.http.post(this.url + '/google/deleteEvent', {
          calendarId: this.calendarIdSelectEdit,
          eventId: this.idCalendarDelete
          }, this.httpOptions)
          .subscribe(response => {
            this.creaEventoGoogle(this.calendarIdSelectEdit, this.titolo, this.oraInizio, this.oraFine);
          });*/
        this.http.post(this.url + '/google/moveEvent', {
          calendarId: this.prevIdCalendar,
          eventId: this.idCalendarDelete,
          destCalendarId: this.calendarIdSelectEdit,
          }, this.httpOptions)
          .subscribe(response => {
            this.allert.success('Evento modificato');
            this.frameGoogleEdit.hide();
            this.refreshPage();
          });
    }

    public cancellaEventoGoogle() {
      this.http.post(this.url + '/google/deleteEvent', {
        calendarId: this.calendarIdSelectEdit,
        eventId: this.idCalendarDelete
        }, this.httpOptions)
        .subscribe(response => {
          let esitoEvento = new RispostaServer();
          esitoEvento = JSON.parse(JSON.stringify(response));
          console.log(esitoEvento.risposta);
        });
  }

  public creaEventoGoogle(cal, tit, st, en) {
    this.http.post(this.url + '/google/createEvent', {
      calendarId: cal,
      titolo: tit,
      start: st,
      end: en
      }, this.httpOptions)
      .subscribe(response => {
        // console.log(response);
        let esitoEvento = new RispostaServer();
        esitoEvento = JSON.parse(JSON.stringify(response));
        console.log(esitoEvento.risposta);
        this.allert.success(esitoEvento.risposta);
        this.frameGoogleEdit.hide();
        this.refreshPage();
      });

  }

    onEventAdd(event: CalendarEvent) {
      console.log(event);
      this.frameGoogleAdd.show();
      this.dataEvento = this.datePipe.transform(event.startDate, 'yyyy-MM-dd');
    }

    public salvaEventoGoogle() {

      if ( this.titoloAdd === '' || this.oraInizioAdd === '' || this.oraFineAdd === '' || this.calendarIdSelect === undefined) {
        this.allert.error('Inserire tutti i campi');
      } else {
        this.http.post(this.url + '/google/createEvent', {
          calendarId: this.calendarIdSelect,
          titolo: this.titoloAdd,
          start: this.dataEvento + 'T' + this.oraInizioAdd + ':00',
          end: this.dataEvento + 'T' + this.oraFineAdd + ':00'
          }, this.httpOptions)
          .subscribe(response => {
            // console.log(response);
            let esitoEvento = new RispostaServer();
            esitoEvento = JSON.parse(JSON.stringify(response));
            console.log(esitoEvento.risposta);
            this.allert.success(esitoEvento.risposta);
            this.frameGoogleAdd.hide();
            this.titoloAdd = '';
            this.oraInizioAdd = '';
            this.oraFineAdd = '';
            this.calendarIdSelect = undefined;
            this.refreshPage();
          });
      }
    }

    onEventDelete(deletedEvent: CalendarEvent) {
    const eventIndex = this.events.findIndex( event => event.id === deletedEvent.id);
    this.events.splice(eventIndex, 1);
    this.events = [...this.events];
    }

    showEvent(event: CalendarEvent) {
      console.log(event);
    }

    refreshPage() {
      this.router.navigateByUrl('blank', { skipLocationChange: true }).then(() => {
        setTimeout(() => this.router.navigate(['appuntamenti']), 200);
    });
  }

  }
