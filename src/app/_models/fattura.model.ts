import { Telefono } from './telefono.model';
import { Mail } from './email.model';
import { Data } from '@angular/router';

export class Fattura {
    // tslint:disable-next-line: variable-name
  _id: string;
  ragioneSociale: string;
  indirizzo: string;
  CAP: string;
  provincia: string;
  pIva: string;
  CF: string;
  legaleRappresentante: string;
  // tslint:disable-next-line: variable-name
  residenzaVia_LR: string;
    // tslint:disable-next-line: variable-name
  residenzaCittà_LR: string;
    // tslint:disable-next-line: variable-name
  residenzaProvincia_LR: string;
    // tslint:disable-next-line: variable-name
  residenzaCAP_LR: string;
    // tslint:disable-next-line: variable-name
  natoA_LR: string;
    // tslint:disable-next-line: variable-name
  dataNascita_LR: Date;
  // tslint:disable-next-line: variable-name
  dataNascita_LR_String: Date;
  CF_LR: string;
  PEC: string;
  SDI: string;
  notaFattura: string;
  telefono: Array<Telefono>;
  email: Array<Mail>;
}
