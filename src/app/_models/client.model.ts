import { Telefono } from './telefono.model';
import { ANNOSMT } from './annoSMT.model';
import { Porta } from './porta.model';

export class Cliente {
  // tslint:disable-next-line: variable-name
  _id: string;
  codCliente: string;
  nomeCognome: string;
  professione: string;
  statoCivile: string;
  gestoreTelefonico: string;
  referente: string;
  tipoCliente: string;
  sitoInternet: string;
  note: string;
  notaCiccolini: string;
  notaCliente: string;
  presentatoDa: string;
  tecnicoRiferimento: string;
  telefoni: Array<Telefono>;
  email: Array<any>;
  fatture: Array<any>;
  tecnico: Array<any>;
  manutenzione: Array<any>;
  impianti: Array<any>;
  porte: Array<Porta>;
  SMT: Array<any>;
  annoSMT: Array<ANNOSMT>;
  notaAnagrafica: string;
  citta: string;
  indirizzo: string;
  provincia: string;
  quartiere: string;
  tavola: string;
  villino: string;
  scala: string;
  piano: string;
  interno: string;
  citofono: string;
  parcheggio: boolean;
  tipologiaLocale: string;
  palazzina: string;
  CAP: string;
  notaE: string;
  // determina i clienti attivi e quelli eliminati
  visibility: boolean;
}
