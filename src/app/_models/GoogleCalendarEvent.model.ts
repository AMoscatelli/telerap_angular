export class EventoGoogleCalendar {
  iCalUID: string;
  id: string;
  summary: string;
  start: Date;
  end: Date;
}
