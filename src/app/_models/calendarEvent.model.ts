export class EventoCalendar {
  // tslint:disable-next-line: variable-name
  id: string;
  startDate: Date;
  endDate: Date;
  name: string;
  color: string;
}
