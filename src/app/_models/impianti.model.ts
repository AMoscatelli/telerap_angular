import { Chiavi } from './chiavi.model';
import { Interventi } from './interventi.model';
import { Fattura } from './fattura.model';
import { ANNOSMT} from './annoSMT.model';

export class Impianti {
    // tslint:disable-next-line: variable-name
    _id: string;
    codiceImpianto: string;
    dataImpianto: Date;
    dataImpiantoString: string;

    // tslint:disable-next-line: variable-name
    dataRistrutturazione_1: Date;
    // tslint:disable-next-line: variable-name
    dataRistrutturazione_1String: string;

    // tslint:disable-next-line: variable-name
    dataRistrutturazione_2: Date;
    // tslint:disable-next-line: variable-name
    dataRistrutturazione_2String: string;

    data3708: Date;
    data3708String: string;

    // tslint:disable-next-line: variable-name
    data3708_2: Date;
    // tslint:disable-next-line: variable-name
    data3708_2String: string;

    infoTGS: boolean;
    infoTGSData: Date;
    infoTGSDataString: string;

    infoSIM: boolean;
    infoSIMData: Date;
    infoSIMDataString: string;

    infoPrivacy: boolean;
    infoPrivacyData: Date;
    infoPrivacyDataString: string;

    garanziaData: Date;
    garanziaDataString: string;

    nomeAC: string;
    codiceAC: string;
    produttoreAC: string;
    CT: string;
    CTC: string;
    ponteRadio: string;

    dataForzeArmate: Date;
    dataForzeArmateString: string;

    dataForzeArmateConsegna: Date;
    dataForzeArmateConsegnaString: string;

    daForzeArmate: string;
    spiegazioneForzeArmate: string;
    numeroSuCTC: string;

    dataNumeroSuCTC: Date;
    dataNumeroSuCTCString: string;

    chiaviCGS: string;
    chiaviVigilanza: string;
    cartellaVerde: boolean;
    chiaviAC: string;
    numeroChiaviAC: string;
    noteImpianto: string;
    noteTecnico: string;
    configurazioneImpianto: string;
    porta: string;
    materialeOmaggio: string;

    statoSMT: string;
    importoSMT: string;
    scadenzaSMT: Date;
    rateSMT: string;
    fermoPerSMT: string;
    fermoDaSMT: Date;
    firmatoSMT: Date;
    contrattoIVAFirmatoSMT: string;
    dataContrattoIVAFirmatoSMT: Date;
    tipoContratto: string;
    tipoServizio: string;
    pagamentoIVA: string;
    spedireFatture: string;
    statoTLS: string;
    importoTLS: string;
    scadenzaTLS: Date;
    rateTLS: string;
    fermoDaTLS: Date;
    fermoPerTLS: string;
    firmatoTLS: Date;
    contrattoIVAFirmatoTLS: string;
    dataContrattoIVAFirmatoTLS: Date;
    notaManutenzione: string;
    dataDisdettaSMT: Date;
    importoDisdettaSMT: string;
    tipologiaDisdettaSMT: string;
    motivoDisdettaSMT: string;
    dataDisdettaTLS: Date;
    importoDisdettaTLS: string;
    tipologiaDisdettaTLS: string;
    motivoDisdettaTLS: string;
    notePercorso: string;
    speseKM: string;
    KM_AR: string;
    tempoAR: string;
    chiavi: Array<Chiavi>;
    interventi: Array<Interventi>;
    fatture: Array<Fattura>;
    smt: Array<ANNOSMT>;
     // cisa Tab
    notaCISA: string;
    oreLavCISA: string;
    oreLavoroCISA: string;
    totaleCISA: string;
    pastiCISA: string;
    dirittoFissoCISA: string;
    totaleIvaCISA: string;
    totaleForfIvaCISA: string;
    noteCISA: string;
    totaleForfCISA: string;
}
