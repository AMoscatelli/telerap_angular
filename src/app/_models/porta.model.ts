import { DefSerCil } from './defenderSerraturaCilindro.model';

export class Porta {
  // tslint:disable-next-line: variable-name
  _id: string;
  data: Date;
  dataString: string;
  timeString: string;
  fornitore: string;
  ubicazione: string;
  modello: string;
  battente: string;
  antispiffero: string;
  antirapina: string;
  occhiello: string;
  bloccoAste: string;
  lastraManganese: string;
  nota: string;
  defenderSerratureCilindri: Array<DefSerCil>;
}
