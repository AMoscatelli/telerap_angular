export class User {
  // tslint:disable-next-line: variable-name
  _id: string;
  nomeUtente: string ;
  emailUtente: string ;
  pwdUtente: string ;
  dataRegistrazioneUtente: Date ;
  dataLoginUtente: Date ;
  permessiUtente: number ;
  areaUtente: number ;
  esito: string;
  token: string;
}
