
export class Chiavi {
  // tslint:disable-next-line: variable-name
  _id: string;
  owner: string;
  consegnateA: string;
  altroCodice: string;
  ultimaVerificaKeyData: Date;
  ultimaVerificaKeyDataString: string;
  statoChiavi: string;
  numeroChiavi: string;
  tipoCustodia: string;
  dataConsegna: Date;
  dataConsegnaString: string;
  telecomando: string;
  tesseraKey: string;
  altro: string;
  note: string;
  noteConsegna: string;
  noteChiavi: string;
}
