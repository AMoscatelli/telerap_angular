import { AppComponent } from './../app.component';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import {AuthenticationService} from '../_services/authentication.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from '../_models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  myGroup: FormGroup;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  constructor(
    private appComponent: AppComponent,
    private router: Router,
    private authenticationService: AuthenticationService,
    ) {
    }

  ngOnInit() {
    setTimeout(() => this.appComponent.setDiv(false, false), 0);
    if (localStorage.getItem('token') !== null) {
      this.router.navigate(['home']);
    } else {
      this.myGroup = new FormGroup({
        name: new FormControl(),
        password: new FormControl()
      });
    }
  }

  get f() { return this.myGroup.controls; }

  onSubmit() {
    this.authenticationService.login(this.f.name.value, this.f.password.value);
    // this.authenticationService.tokenCheck();
  }
}
