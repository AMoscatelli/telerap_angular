import { UtilityCalendarColorComponent } from './utility/utility_calendarColor/utility_calendarColor.component';
import { AppuntamentiComponent } from './appuntamenti/appuntamenti.component';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { ClientiComponent } from './clienti';
import { ScadutoComponent } from './scaduto';
import { UtilityComponent } from './utility';
import { ClientiAnagraficaComponent } from './clienti/clientiAnagrafica';
import { ClientiChiaviComponent } from './clienti/clientiChiavi';
import { ClientiManutenzioneComponent } from './clienti/clientiManutenzione';
import { ClientiPorteComponent } from './clienti/clientiPorte';
import { ClientiPorteAddComponent } from './clienti/clientiPorteAdd';
import { ClientiImpiantoComponent } from './clienti/clientiImpianto';
import { ClientiInterventoComponent } from './clienti/clientiIntervento';
import { ClientiAddInterventoComponent } from './clienti/clientiAddIntervento';
import { ClientiFattureComponent } from './clienti/clientiFatture';
import { ClientiCISAComponent } from './clienti/clientiCISA/clientiCISA.component';
import { ClientiAddImpiantoComponent } from './clienti/clientiAddImpianto';
import { ClientiEComponent } from './clienti/clientiE/clientiE.component';
import { UtilityCentraleComponent } from './utility/utility_centrale/utility_centrale.component';
import { UtilityPianoComponent } from './utility/utility_piano/utility_piano.component';
import { BlankComponent } from './blank/blank.component';
import { UtilityProfessioneComponent } from './utility/utility_professione/utility_professione.component';
import { UtilityTipoClienteComponent } from './utility/utility_tipoCliente/utility_tipoCliente.component';
import { UtilityTipoInterventoComponent } from './utility/utility_tipoIntervento/utility_tipoIntervento.component';
import { UtilityDisdettaComponent } from './utility/utility_disdetta/utility_disdetta.component';
import { UtilityTipoLocaleComponent } from './utility/utility_tipoLocale/utility_tipoLocale.component';
import { UtilityIvaComponent } from './utility/utility_iva/utility_iva.component';
import { UtilityStatoCivileComponent } from './utility/utility_statoCivile/utility_statoCivile.component';
import { UtilityServizioComponent } from './utility/utility_servizio/utility_servizio.component';

const routes: Routes = [
    { path: '', component: LoginComponent},
    { path: 'login', component: LoginComponent },
    { path: 'home', component: HomeComponent },
    { path: 'clienti', component: ClientiComponent },
    { path: 'clientiAnagrafica', component: ClientiAnagraficaComponent },
    { path: 'clientiManutenzione', component: ClientiManutenzioneComponent },
    { path: 'clientiImpianto', component: ClientiImpiantoComponent },
    { path: 'clientiAddImpianto', component: ClientiAddImpiantoComponent },
    { path: 'clientiAddIntervento', component: ClientiAddInterventoComponent },
    { path: 'clientiIntervento', component: ClientiInterventoComponent },
    { path: 'clientiChiavi', component: ClientiChiaviComponent },
    { path: 'clientiCISA', component: ClientiCISAComponent },
    { path: 'clientiE', component: ClientiEComponent },
    { path: 'utility', component: UtilityComponent },
    { path: 'utility_centrale', component: UtilityCentraleComponent },
    { path: 'utility_piano', component: UtilityPianoComponent },
    { path: 'utility_professione', component: UtilityProfessioneComponent },
    { path: 'utility_tipoCliente', component: UtilityTipoClienteComponent },
    { path: 'utility_tipoIntervento', component: UtilityTipoInterventoComponent },
    { path: 'utility_disdetta', component: UtilityDisdettaComponent },
    { path: 'utility_tipoLocale', component: UtilityTipoLocaleComponent },
    { path: 'utility_iva', component: UtilityIvaComponent },
    { path: 'utility_statoCivile', component: UtilityStatoCivileComponent },
    { path: 'utility_servizio', component: UtilityServizioComponent },
    { path: 'utility_calendarscolor', component: UtilityCalendarColorComponent },
    { path: 'appuntamenti', component: AppuntamentiComponent },
    { path: 'sessionExpired', component: ScadutoComponent },
    { path: 'clientiPB', component: ClientiPorteComponent },
    { path: 'clientiFattura', component: ClientiFattureComponent},
    { path: 'clientiPorteAdd', component: ClientiPorteAddComponent},
    { path: 'blank', component: BlankComponent},


    // otherwise redirect to home
    { path: '**', redirectTo: 'home' }
];

export const appRoutingModule = RouterModule.forRoot(routes);
